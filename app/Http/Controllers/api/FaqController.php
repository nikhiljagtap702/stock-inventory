<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;




class FaqController extends Controller {

   
	public function get_faqs( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        
        /*for( $i = 0; $i < 10; $i++ ) {
            $response['data'][] = array(
                'nodeID'    => ($i+1),
                'title'     => 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
                'body'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            );
        }*/

        $response['data'][] = array(
            'nodeID'    => 1,
            'title'     => 'Why Support?',
            'body'      => 'Support is a multi-product aggregator that offers seamless management of all restaurant purchases and billing with an app.',
        );

        $response['data'][] = array(
            'nodeID'    => 2,
            'title'     => 'Why Support?',
            'body'      => '-   No minimum orders\n 
                            -   Unmatched prices\n
                            -   Free and safe delivery\n
                            -   24/7 support',
        );

        $response['data'][] = array(
            'nodeID'    => 3,
            'title'     => 'How do I register to the Support app?',
            'body'      => 'You can register with your mobile number and OTP. Once you have put the mobile number and OTP, registration process will start.',
        );


        $response['data'][] = array(
            'nodeID'    => 4,
            'title'     => 'Is there a MOQ for order?',
            'body'      => 'Order a single crate or make a bulk purchase as per your agreement.',
        );  

        $response['data'][] = array(
            'nodeID'    => 5,
            'title'     => 'What are the accepted modes of payment on Support?',
            'body'      => 'Support accepts an online UPI payment, bank transfers and cheques, as well as offers a credit period supported a pre agreed contract.',
        );  

        $response['data'][] = array(
            'nodeID'    => 6,
            'title'     => 'What type of products can I buy from Support?',
            'body'      => 'Currently Support trades in soft beverages but we are looking to expand to all restaurant supplies.',
        );  

        $response['data'][] = array(
            'nodeID'    => 7,
            'title'     => 'As a restaurant owner, can I add multiple restaurants?',
            'body'      => 'Yes, Support allows you to add multiple restaurants. You can manage the purchases of each restaurant seamlessly through the app.',
        );  

        $response['data'][] = array(
            'nodeID'    => 8,
            'title'     => 'How can I track my order?',
            'body'      => 'On your purchase history / order details tab on the app, there is a tracking button that will allow you to track the entire delivery timeline of your order.',
        );  

        $response['data'][] = array(
            'nodeID'    => 9,
            'title'     => 'Can I return the item once it is purchased?',
            'body'      => 'Any items you want to return (due to quality concerns, manufacturing defects, expiry dates or anything else) need to be returned at the time of delivery. We do not accept returns once the item has been claimed by the restaurant.',
        );  

        $response['data'][] = array(
            'nodeID'    => 10,
            'title'     => 'What brands are available on the support app?',
            'body'      => 'All the popular, favourite brands are part of Support and we continue to grow by adding all new market entrants.',
        );

        $response['status'] = 'success';

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
}
