<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class session extends Model {

	const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';

    // public $timestamps = false;

    protected $primaryKey = 'sessionID';
    
    protected $fillable = [
   		'userID',
   		'fcmToken',
   		'location',
   		'device',
   		'deviceID',
   		'account',
        'status',
   		'system',
   		'manufacturer',
   		'model',
        'version'
   	];
}