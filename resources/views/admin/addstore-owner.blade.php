@extends('layouts/admin')

@section('title','Store Owner') 

@section('content')
    <style type="text/css">
        .img-fluid {
            max-height: 10rem;
            object-fit: contain;
            object-position: center;
        }
    </style>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                Add a Store-Owner
                </h6>
            </div>
            <div class="card-body pb-0">
                <form method="post" action="{{ route('owner.store') }}" enctype="multipart/form-data" > 
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-3 mb-3 mb-sm-0">
                            <label>First Name</label>
                            <input type="text" class="form-control form-control-user" id="exampleFirstName" name="name"  required="required" >
                          </div>

                          <div class="col-sm-3">
                            <label>Full Name</label>
                            <input type="text" class="form-control form-control-user" id="exampleLastName"  name="full_name" required="required">
                          </div>
                       
                         <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>Email Address</label>
                          <input type="email" class="form-control form-control-user" id="exampleInputEmail" name="email"  required="required">
                        </div>
                        <div class="col-sm-3">
                          <label>Mobile</label>
                        	<input type="text" class="form-control form-control-user" id="exampleInputEmail" name="mobile"  required="required" maxlength="10">	
                        </div>
                        <div class="col-sm-3">
                            <label>Role </label>
                            <select name="role" class="form-control">
                                <option value="3">Store Owner</option>
                            </select>
                        </div>
                           
                         
                          <div class="col-sm-3 mb-3 mb-sm-0">
                            <label>Password</label>
                            <input type="password" class="form-control form-control-user" id="exampleInputPassword"  name="password"  required="required">
                          </div>

                          <div class="col-sm-3">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control form-control-user" id="exampleRepeatPassword">
                          </div>
                       
                        </div>
                        
                        <div class="form-group row">
                          <div class="col-sm-3 mb-3 mb-sm-0">
                            <label>Date of Birth</label>
                            <input type="date" class="form-control form-control-user" id="exampleInputPassword"  name="dob"  required="required">
                           </div>
                           <div class="col-sm-3">
                            <label>Company Name</label>
                            <input type="text" class="form-control form-control-user" id="exampleRepeatPassword"  name="company"  required="required">
                          </div>
        			
                        <div class="col-sm-3 mb-3 mb-sm-0">
                            <label>Position</label>
                            <input type="text" class="form-control form-control-user" id="exampleInputPassword"   name="position"  required="required">
                          </div>
                          <div class="col-sm-3">
                            <label>Website</label>
                            <input type="text" class="form-control form-control-user" id="exampleRepeatPassword"  name="website" required="required">
                          </div>
        				 </div>

    			   <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Address</label>
                        <textarea name="address" class="form-control" required="required"></textarea>
                      </div>
                      <div class="col-sm-2">
                        <label>State</label>
                        <input type="text" class="form-control form-control-user" id="exampleRepeatPassword"  name="state"  required="required">
                      </div>
    			     <div class="col-sm-2 mb-3 mb-sm-0">
                        <label>City</label>
                        <input type="text" class="form-control form-control-user" id="exampleInputPassword"   name="city"  required="required">
                      </div>
                      <div class="col-sm-2">
                        <label>Zip-Code</label>
                        <input type="text" class="form-control form-control-user" id="exampleRepeatPassword"  name="zipcode"  required=" required">
                      </div>
    				</div>

        			<div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label for="enthusiast" >Gender</label><br/>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="male" name="gender" class="custom-control-input" value="0"  >
                                <label class="custom-control-label" for="male" style="cursor: pointer;">Male</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="female" name="gender" class="custom-control-input" value="1" >
                                <label class="custom-control-label" for="female" style="cursor: pointer;">Female</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="other" name="gender" class="custom-control-input" value="2" >
                                <label class="custom-control-label" for="other" style="cursor: pointer;">Other</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                           <label>Featured Image</label>
                           <input type='file' name="fileToUpload" onchange="readURL(this);" alt="your image"  />
                        </div>
                       <div class="form-group col-md-3">
                            <img id="blah"  class="img-fluid" src="/" />
                            </div>
                       </div>
                     <div class="form-group mt-4">
                        <button type="submit" class="btn btn-success">
                            <i class="fas fa-save"></i>
                            <span>Save</span>
                        </button>

                        <a class="btn btn-outline-secondary ml-3" href="{{ url('/admin/stores/create') }}" role="button" title="Go back">
                            <i class="fas fa-chevron-left"></i>
                            <span>Back</span>
                        </a>
                    </div>
                </form>
            </div>
           
        </div>
  
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection		