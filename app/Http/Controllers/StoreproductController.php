<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\store_products;
use App\order_product;
use App\order;
use App\store;
use App\product;

class StoreproductController extends Controller
{
    public function show($id){
        $data = store_products::latest()->join('stores','store_products.storeID', '=','stores.storeID')->join('products','store_products.productID', '=' ,'products.productID')->leftjoin('files','products.fileID' , '=','files.fileID')->where('store_products.storeID','=',$id)->select('store_products.*','stores.title','products.title as producttitle','products.max_price','products.price as sale_price','files.uri','products.stock','products.sales')->get();
    	 return view("admin.viewproductstore", compact('data'))
          ->with('i');
    }
    public function add(){
        $data = store::all();
        $product = product::all();
          $product = product::whereNotExists( function ($query) {
            $query->select('store_products.storeID')
            ->from('store_products')
            ->whereRaw('products.productID = store_products.productID')
            ->where('store_products.storeID', '=' ,request()->segment(5));
          })->get();
        return view("admin.productstore-manage",compact('data'),compact('product'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'product' => 'required',
        ]);
        foreach ($request['product'] as  $value) {
            // echo $value;
           store_products::create( ['storeID' => $request['storeID'] , 'productID' => $value ] );
        }
        $data = store_products::latest()->join('stores','store_products.storeID', '=','stores.storeID')->join('products','store_products.productID', '=' ,'products.productID')->leftjoin('files','products.fileID' , '=','files.fileID')->where('store_products.storeID','=', $request['storeID'])->select('store_products.*','stores.title','products.title as producttitle','products.price as sale_price','files.uri','products.max_price','products.stock','products.sales')->get();
        $storeID = $request['storeID'];
        return view('admin.edit-store-products',compact('data','storeID'));
        
        // return redirect( '/admin/store/product/'.$request['storeID'])->with('success', 'Product has been  created successfully' );
    }

    public function edit_products(Request $request){
        $storeID = $request->id;
        $data = store_products::latest()->join('stores','store_products.storeID', '=','stores.storeID')->join('products','store_products.productID', '=' ,'products.productID')->leftjoin('files','products.fileID' , '=','files.fileID')->where('store_products.storeID','=', $request->id)->select('store_products.*','stores.title','products.title as producttitle','products.price as sale_price','files.uri','products.max_price','products.stock','products.sales')->get();
        return view('admin.edit-store-products',compact('data','storeID'));
        
        // return redirect( '/admin/store/product/'.$request['storeID'])->with('success', 'Product has been  created successfully' );
    }


    public function edit($id){
        $sproduct = store_products::where('store_products.id',$id)->join('products', 'products.productID', '=' , 'store_products.productID')->select('store_products.*','products.title','products.price as p_price','products.max_price','products.category')->first();
        return view("admin/productstore-manage",compact('sproduct'));
    }

    public function update(Request $request,$id){
        foreach ($request['price'] as $key => $value) {
            store_products::where('id','=',$key)->update(  array('price' => $value[0] ) );
        }

        $msg = 'Products has been  updated successfully';
        return redirect( '/admin/store/product/'.$request->id )->with('success', $msg );

    }

    public function destroy($id){
        $store_products = store_products::find($id);
        $store_products->delete();
        $store = $store_products->storeID;
        return redirect( '/admin/store/product/'.$store )->with('success','Store product Deleted successfully');
    }

    public function import(){
        $page = 1;
        if( isset( $_REQUEST['page'] ) && !empty( $_REQUEST['page'] ) ) {
            $page = intval( $_REQUEST['page'] );
        }
        $limit = 10;
        $filepath =  url(asset('/public/data/salesTrackingReport.csv'));
        $file = fopen( $filepath, 'r' );
        $importData_arr = array();
        $i = 0;
        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count( $filedata );
            // Skip first row (Remove below comment if you want to skip the first row)
            if($i == 0){
                $i++;
                continue;
            }
            for ($c=0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata [$c];
            }
            $i++;
        }
        fclose($file);

        $orders = array();
        // Insert to MySQL database
        foreach($importData_arr as $importData){

            $referenceID =  str_replace( 'SO', '', $importData[1] );
            $date = date( 'c', strtotime( $importData[0] ) );
            $statusName = strtolower( $importData[2] );
            if( $statusName == 'draft' ) {
                $status = '0';
            }else if( $statusName == 'posted' ) {
                $status = '1';
            }else {
                $status = '99';
            }

            if( ! isset( $orders[ $referenceID ] ) ) {
                $orders[ $referenceID ] = array();
                $orders[ $referenceID ]['storeID']      = $importData[3];
                $orders[ $referenceID ]['created']      = $date;
                $orders[ $referenceID ]['updated']      = $date;
                // $orders[ $referenceID ]['name']         = $importData[4];
                $orders[ $referenceID ]['status']       = $status;
                $orders[ $referenceID ]['referenceID']  = $referenceID;
                $orders[ $referenceID ]['products']     = array();
            }

            $orders[ $referenceID ]['products'][] = array(
                "title"     => $importData[6],
                "quantity"  => floatval( str_replace( ',', '', $importData[7] ) ),
                "amount"    => floatval( str_replace( ',', '', $importData[8] ) ),
                "hsn"       => $referenceID,
                'storeID'   => $importData[3],
                'price'     => ( floatval( str_replace( ',', '', $importData[8] ) ) / floatval( str_replace( ',', '', $importData[7] ) ) ),
                'created'   => $date,
                'updated'   => $date,
            );
        }
        if( isset( $orders ) && !empty( $orders ) ) {
            $o = array_values( $orders );
            $orders = array();
            foreach ( $o as $orderID => $order ) {
                // $order['orderID'] = ($orderID+1);
                $order['items'] = count( $order['products'] );
                $order['amount'] = array_sum( array_column( $order['products'], 'amount' ) );
                $orders[] = $order;

                $orderID = order::create( $order );
                if( isset( $order['products'] ) && !empty( $order['products'] ) ) {
                    foreach ( $order['products'] as $product ) {
                        // $product['orderID'] = $orderID;
                        order_product::create( $product );
                    }
                }
                // print_r( $orderID . PHP_EOL );
            }
            print_r( $orders );
            exit;
        }
    }
}
