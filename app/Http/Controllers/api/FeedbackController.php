<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\feedback;
use App\User;
use Validator;

/**
 * @group Feedbacks
 *
 * APIs for managing feedbacks
 */

class FeedbackController extends Controller {
	 /** 
     * Manage feedback data
     * 
     * @bodyParam userID required Unique user ID Example: 1
     * @bodyParam storeID required Unique store ID Example: 2
     * @bodyParam subject optional What is this feedback about Example: Unable to create an order
     * @bodyParam message required Feedback message or query  Example: This is a test message
     * @bodyParam module optional Feedback module  Example: query
     * @bodyParam company_name optional Company name  Example: xyz
     * @bodyParam referenceID optional Any reference ID  Example: abc1234
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     */
	
	public function post_feedback( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        
        $valid = Validator::make( $request->all() , [
            'userID' => ['required'],
            'storeID' => ['required'],
            'subject' => [],
            'message' => ['required'],
            'module' => [],
            'company_name' => [],
            'referenceID' => [],
        ]);
        
        if( !$valid->fails() ) {
        
        
            $data = array(
                'userID'       => $request->userID,
                'module'       => 'feedback',
                'referenceID'  => $request->storeID ?? '',
                'subject'      => $request->subject ?? '',
                'company_name' => $request->company_name ?? '',
                'message'      => $request->message,
                'status'       =>  1,
            );

            $response['data'] = feedback::create( $data );

            $response['status'] = 'success';
            $response['messages'][] = array( 'message' => 'Your feedback has been received successfully', 'type' => 'success' );

		}
        
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

     /** 
     * Manage feedback register data
     * 
     * @bodyParam store_name optional store_name Example: xyz
     * @bodyParam subject optional What is this feedback about Example: Unable to create an order
     * @bodyParam message required Feedback message or query  Example: This is a test message
     * @bodyParam referenceID optional Any reference ID  Example: abc1234
     * @bodyParam name optional  name   Example: xyz
     * @bodyParam email optional  email   Example: xyz@gmail.com
     * @bodyParam mobile optional mobile  Example: 68415324655
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     */
    
    public function post_register( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        
        $valid = Validator::make( $request->all() , [
            'store_name'  => [],
            'name'        => [],
            'email'       => [],
            'mobile'      => [],
            'subject'     => [],
            'message'     => ['required'],
            'referenceID' => [],
        ]);
        
        if( !$valid->fails() ) {
        
        
            $data = array(
                'name'        => $request->name,
                'email'       => $request->email ?? '',
                'mobile'      => $request->mobile ?? '',
                'module'      => 'register',
                'referenceID' => $request->storeID ?? '',
                'subject'     => $request->subject ?? '',
                'message'     => $request->message,
                'company_name' => $request->store_name ?? '',
                'status'      =>  1,
            );

            $response['data'] = feedback::create( $data );

            $response['status'] = 'success';
            $response['messages'][] = array( 'message' => 'Your feedback has been received successfully', 'type' => 'success' );

        }
        
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
    /*public function get_feedbacks( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }*/
}
