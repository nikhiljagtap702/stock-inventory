@extends('layouts/admin')

@section('title','Order Payment')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Order Payment</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ url('/admin/orders/updatebilling/'.Request::segment(4)) }}" >
            @csrf
            @method('PUT')

          <!--   <div class="form-row">
                <div class="form-group col-md-4 mx-auto">
                    <label>Type<span class="text-danger">*</span></label><br/>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="Credit" name="type" class="custom-control-input" value="0" required='required' >
                        <label class="custom-control-label" for="Credit" style="cursor: pointer;">Credit</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="Debit" name="type" class="custom-control-input" value="1" required='required' >
                        <label class="custom-control-label" for="Debit" style="cursor: pointer;">Debit</label>
                    </div>
                </div>
            </div> -->
            <div class="form-row">
                <div class="form-group col-md-4 mx-auto">
                    <label>TransactionID<span class="text-danger">*</span> </label>
                    <select class="form-control" name="transactionID" required="required">
                        <option value=""> --Select Transaction ID-- </option>
                        @foreach($transaction as $val)
                        <option value="">{{ $val['transactionID'] ?? '' }}/{{ $val['method'] ?? '' }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <input type="hidden" id="Credit" name="type" value="0" >
            <div class="form-group col-md-4 mx-auto">
                <button type="submit" class="btn btn-primary"><i class="bi bi-file-earmark-plus"></i><span class="ml-1">Submit</span></button>

                <a class="btn btn-outline-secondary" href="{{ url('/admin/orders/view/'.Request::segment(4))}}" role="button" title="Go back">
                    <i class="bi bi-caret-left"></i>
                    <span>Back</span>
                </a>
            </div>                    
        </form>
    </div>
</div>
@endsection