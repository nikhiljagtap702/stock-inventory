@extends('layouts/admin')

@section('title','user page')

@section('content')
		<!-- main content start-->
	       <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Edit User </h6>
          </div>
          <div class="card-body">
              <form  method="post" action="{{ route('users.update',$user->userID )}}" enctype="multipart/form-data">
              	@csrf
                @method('PUT')
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="exampleFirstName" value="{{ $user->name}}" placeholder="First Name" name="name">
                     @error('name')
                     <span class="feedback" role="alert">
                      <strong class="text-danger">{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="exampleLastName" value="{{ $user->full_name}}" placeholder="Full Name" name="full_name">
                     @error('full_name')
                    <span class="feedback" role="alert">
                      <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
                <div class="form-group row">
                	<div class="col-sm-6 mb-3 mb-sm-0">
                  <input type="email" class="form-control form-control-user" id="exampleInputEmail" value="{{ $user->email }}" placeholder="Email Address" name="email">
                  @error('email')
                  <span class="feedback" role="alert">
                    <strong  class="text-danger">{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="col-sm-6">
                	<input type="text" class="form-control form-control-user" id="exampleInputEmail" value="{{ $user->mobile }}" placeholder="mobile" name="mobile">
                	  @error('mobile')
                   <span class="feedback" role="alert">
                    <strong  class="text-danger">{{ $message }}</strong>
                   </span>
                  @enderror	
                </div>
            </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password" name="password">
                     @error('password')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                     @enderror
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Repeat Password">
                  </div>
                 </div>
                    <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="date" class="form-control form-control-user" value="{{ $user->dob }}" id="exampleInputPassword" placeholder="dob"  name="dob">
                     @error('dob')
                     <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                     </span>
                     @enderror
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" value="{{ $user->company }}" id="exampleRepeatPassword" placeholder="company" name="company">
                    @error('company')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                     @enderror
                  </div>
			         	</div>
				    <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" value="{{ $user->position }}" id="exampleInputPassword" placeholder="position"  name="position">
                     @error('position')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                @enderror
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" value="{{ $user->website }}" id="exampleRepeatPassword" placeholder="website" name="website">
                    @error('website')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
				      </div>
				     <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text"  class="form-control form-control-user" id="exampleInputPassword" placeholder="address" value="{{ $user->address }}"  name="address">
                     @error('address')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                  @enderror
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="exampleRepeatPassword" value="{{ $user->state}}" placeholder="state" name="state">
                    @error('state')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
				        </div>
				      <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" value="{{ $user->city }}" id="exampleInputPassword" placeholder="city"  name="city">
                    @error('city')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                     @enderror
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" value="{{ $user->zipcode }}" id="exampleRepeatPassword" placeholder="zipcode" name="zipcode">
                    @error('zipcode')
                    <span class="feedback" role="alert">
                        <strong  class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
				      </div>
				   <div class="form-group row">
               <div class="col-sm-6 mb-3 mb-sm-0">
                  <input type="file" name="fileToUpload" class="form-control" id="focusedinput">
                   </div>
                  <div class="col-sm-6">
                   <label class="font-weight-bold">Gender</label>
                   <input type="radio"  name="gender" value="0">
            				<label for="male">Male</label>
            				<input type="radio"  name="gender" value="1">
            				<label for="female">Female</label>
            				<input type="radio"  name="gender" value="2">
            				<label for="other">Other</label>
            			</div>
            		</div>
               <input type="submit" class="btn btn-primary btn-user btn-block" value="Edit Account" name="submit">
            </form>
             </div>
          </div>
        </div>
      </div>
    </div>
  </main>



									

		@endsection
		