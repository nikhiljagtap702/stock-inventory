@extends('layouts/admin')

@section('title','Team Member page')

@section('content')
<div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Teams </h6>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ url('/admin/store/add/'.Request::segment(4).'/') }}" class="btn btn-primary">+ADD TEAM MEMBER</a>
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Role</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($data as $value )
                        <tr> 
                            <td scope="row">#{{ $value->teamID }}</td>
                            <td>
                                <?php if($value->userID == $value->owner ){ ?>
                                    {{ $value->name}}
                                <?php }else{ ?>
                                    <a href="{{ url('/admin/store/edit/'.$value->teamID.'/') }}">{{ $value->name}}</a>
                                <?php } ?>
                            </td>
                             <td><?php if($value->userID == $value->owner && $value->role == 1){
                                echo "Owner";
                            }elseif ($value->role == 2 ) {
                                 echo "Manager";
                            }elseif ($value->role == 3 ) {
                                echo "Customer";
                            }elseif ($value->role == 1) {
                                echo "Admin";
                            }else{
                                echo "Employee";
                            } ?></td>
                           
                         </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.card-body -->
    </div><!-- /.card -->
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : []
        });
    });
</script>
@endsection
    