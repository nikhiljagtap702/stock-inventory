@extends('layouts/admin')

@section('title','Stores')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6 class="m-0 font-weight-bold text-primary">Stores </h6>
            </div>
            <div class="col-md-6 text-right">
                @if(Auth::check() && Auth::User()->role == 1)
                <a href="{{ route('stores.create') }}" class="btn btn-primary">+ADD STORE</a>
                @endif
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col" >Store Name</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Products</th>
                        <th scope="col">Members</th>
                        <th scope="col">Orders</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
				    @foreach($data as $value)
                    <tr> 
                        <td class="align-middle">#{{ $value->storeID }}</td> 
                        <td class="align-middle" >
                            <a href="{{ route('stores.show', $value->storeID ) }}">
                                <?php if($value->fileID){?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/'.$value->uri )}}" height="32" width="auto" style="height: 2rem;">
                                <?php }else{ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" height="32" width="auto" style="height: 2rem;">
                                <?php } ?>
                                {{ $value->title }}</a>
                        </td> 
                        <td class="align-middle"><a href="{{ route('users.show', $value->userID ?? 0 ) }}">  {{ $value->name ?? '' }} </a></td> 
                        <td class="text-center align-middle">{{ $value->products ?? 0 }}</td> 
                        <td class="text-center align-middle">{{ $value->members ?? 0 }}</td> 
                        <td class="text-center align-middle">{{ $value->orders ?? 0 }}</td> 
                        <td class="text-center align-middle">
                            <?php if(Auth::check() && Auth::User()->role == 2){ ?>
                                <a href="{{ url('/admin/store/cart/'.$value->storeID.'/') }}" class="btn btn-info" title="View orders">
                                    <i class="bi bi-cart4"></i><span class="d-none d-md-block">Orders</span>
                                </a>
                              <!--   <a href="{{ url('/admin/store/team/'.$value->storeID.'/') }}" class="btn btn-success" title="View members">
                                    <i class="fas fa-user"></i><span class="d-none d-md-block">Members</span>
                                </a> -->
                            <?php  }elseif (Auth::check() && Auth::User()->role == 1 ) { ?>
                                <a href="{{ url('/admin/store/cart/'.$value->storeID.'/') }}" class="btn btn-info d-flex" title="View orders">
                                    <i class="bi bi-cart4"></i><span class="d-none d-md-block ml-1">Orders</span>
                                </a>
                            <?php   } ?>
                        </td>
    				</tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div><!-- /.card-body -->
    </div><!-- /.card -->
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : [],
              "pageLength": 25

        });
    });
</script>
@endsection