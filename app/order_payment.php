<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_payment extends Model
{
    
    const CREATED_AT = 'created';
	public $timestamps = false;

	protected $primaryKey = 'paymentID';
    
    protected $fillable = [
   		'orderID',
      'fileID',
   		'transactionID',
   		'amount',
   	  'type',
      'gateway',
      'method',
      'status',
   	];
}
