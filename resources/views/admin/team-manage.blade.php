@extends('layouts/admin')
<?php if( isset( $data->teamID ) && !empty( $data->teamID ) ) :?>
    @section('title','Manage Team Member ') 
<?php else :?>
    @section('title','Add Team Member') 
<?php endif;?>

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            <?php if( isset( $data->teamID ) && !empty( $data->teamID ) ) :?>
                Edit Team Member
            <?php else :?>
                Add a Team Member
            <?php endif;?>
        </h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?php if( isset( $data->teamID ) && !empty( $data->teamID ) ) :?>
                    <form method="post" action="{{url('/admin/store/update/'.$data->teamID.'/') }}"  >
                    @csrf
                    @method('PUT') 
                <?php else :?>
                    <form method="post" action="{{ url('/admin/store/store/') }}" >
                    @csrf
                <?php endif;?>
                <input type="hidden" name="storeID"  value="{{ $storeID ?? ''}}">
                <input type="hidden" name="userID"  value="{{ $user->userID ?? ''}}">
                <input type="hidden" name="store"  value="{{ $data->storeID ?? ''}}">
                <div class="form-row">
                    <label>Role </label>
                    <select name="role" class="form-control">
                        <?php if( isset( $data->teamID ) && !empty( $data->teamID ) ) :?>
                            <option value ="1"  <?php if($data->role=='1') echo 'selected="selected"'; ?> >Admin</option>
                            <option value ="2" <?php if($data->role=='2') echo 'selected="selected"'; ?>>Manager</option>
                        <?php else :?>
                            <option value ="1" >Admin</option>
                            <option value ="2" >Manager</option>
                        <?php endif;?>
                    </select>
                    </div><br/>
                    <button type="submit" class="btn btn-success">
                        <i class="bi bi-file-earmark-plus"></i><span class="pl-1">Add</span>
                    </button>
                    <?php if( isset( $data->teamID ) && !empty( $data->teamID ) ) :?>
                        <a class="btn btn-outline-secondary" href="{{ url('/admin/store/team/'.$data->storeID) }}" role="button" title="Go back">
                            <i class="bi bi-caret-left"></i>
                            <span>Back</span>
                        </a>
                        <?php else :?>
                            <a class="btn btn-outline-secondary" href="{{ url('/admin/store/team/'.$storeID) }}" role="button" title="Go back">
                                <i class="bi bi-caret-left"></i>
                                <span>Back</span>
                            </a>
                        <?php endif;?>
                    </form>
                    <?php if( isset( $data->teamID ) && !empty( $data->teamID ) ) :?>
                        <form action="{{url('/admin/store/delete/'.$data->teamID.'/') }}" method="post"><br/>
                            <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are you sure you want to delete this Team member?');" title="Delete">
                                <i class="bi bi-trash"></i>
                                <span>Remove Access</span>
                            </button>
                            @csrf
                            @method('DELETE')
                        </form>
                        <?php else :?>
                        <?php endif;?>
                    </div>
                    <div class="col-md-6">
                        <h6>User Details</h6>
                        <table class="table table-sm table-hover table-bordered">
                            <tr>
                                <th width="120">Name</th>
                                <td>{{$user->name ?? ''}}</td>
                            </tr>
                            <tr>
                                <th width="120">Mobile</th>
                                <td >{{$user->mobile ?? ''}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
