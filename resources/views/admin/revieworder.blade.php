@extends('layouts/admin')

@section('title','Review Order page')

@section('content')
    <form class="form" name="form" action="">
      <div class="card shadow mb-4">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-4">
                        <h6 class="m-0 font-weight-bold text-primary">REVIEW ORDER</h6>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="text-nowrap" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col">IMAGE</th>
                                <th scope="col">PRODUCT</th> 
                                <th scope="col">BRAND NAME</th>                     
                                <th scope="col">PRICE</th>
                                <th scope="col">QUANTITY</th>
                                <th scope="col">SUB TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                           <tr>
                                @foreach($product_order as $pro)
                                <td> <img class="img-fluid" src="{{ $pro['image'] }}"  height="32" width="auto" style="height: 2rem;"></td>
                                <td>{{ $pro['title'] }}</td>
                                <td>{{ $pro['brand_name'] }}</td>
                                <td>{{ $pro['price'] }}</td>
                                <td>{{ $pro['quantity'] }}</td>
                                <td>{{ $pro['amount'] }}</td>
                          
                              </tr>
                             @endforeach   
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->
                 <hr/>
                <div class="row">
                <div  class="col-3 ml-auto">
                 <strong> ORDER SUBTOTAL  :  {{ $orderTotal }}</strong>
                </div>
               </div>
              <hr/>
             <div class="row pt-3">
            <div class="col-md-6">
             <a href="{{ url('/admin/store/cart/edit/'.$order->orderID.'/') }}" type="submit" class="btn btn-info">BACK</a>
            </div>
            <div class="col-md-1 ml-auto">
                 <a href="{{ url('/admin/store/cart/update/place/'.$order->orderID.'/') }}" class="btn btn btn-warning text-right">PLACE</a>
            </div>
             </div>
            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </form> 
@endsection 
