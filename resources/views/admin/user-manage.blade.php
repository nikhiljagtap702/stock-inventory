@extends('layouts/admin')

<?php if( isset( $user->userID ) && !empty( $user->userID ) ) :?>
    @section('title','Manage User') 
<?php elseif( isset( $password ) && !empty( $password ) ) :?>
    @section('title','Add Store Owner') 
<?php else :?>
    @section('title','Add User') 
<?php endif;?>





@section('content')
<?php

if( !isset( $user->userID ) || empty( $user->userID ) ) {
    $user = collect( [] );
    $user->userID = 0;
    $user->fileID = 0;
    $user->name = '';
    $user->full_name = '';
    $user->email = '';
    $user->password = '';
    $user->mobile = '';
    $user->dob = '';
    $user->company = '';
    $user->position = '';
    $user->website = '';
    $user->address = '';
    $user->state = '';
    $user->city = '';
    $user->country = '';
    $user->zipcode = '';
    $user->gender = null;
    $user->role = null;
    $user->avatar = asset( 'public/media/default-user.png' );
}
?>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php if( isset( $user->userID ) && !empty( $user->userID ) ) :?>
                Edit User 
                <?php elseif( isset( $password ) && !empty( $password ) ) :?>
                    Add Store Owner
                <?php else :?>
                Add a User
                <?php endif;?>
                </h6>
            </div>
            <div class="card-body pb-0">
                <?php if( isset( $user->userID ) && !empty( $user->userID ) ) :?>
                <form method="post" action="{{ route('users.update', $user->userID ) }}" enctype="multipart/form-data" > 
                    @csrf
                    @method('PUT')
                <?php else :?>
                    <form method="post" action="{{ route('users.store') }}" enctype="multipart/form-data" > 
                    @csrf
                <?php endif;?>
                   
                    <div class="form-group row">
                        <div class="col-sm-3 mb-3 mb-sm-0">
                            <label>First Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-user" id="exampleFirstName" name="name" value="{{ old('name') ?? $user->name ?? ''}}" required="required" >
                        </div>

                        <div class="col-sm-3">
                            <label>Last Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-user" id="exampleLastName" required="required"  name="full_name" value="{{ old('full_name') ?? $user->full_name ?? ''}}" >
                        </div>
                       
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>Email Address <span class="text-danger">*</span></label>
                            <input type="email" class="form-control form-control-user" id="exampleInputEmail" name="email" value="{{ old('email') ?? $user->email ?? ''}}" required="required">
                            @error('email')
                                <span class="text-danger" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            
                        </div>
                        <?php if( isset( $user->userID ) && !empty( $user->userID ) ) :?>
                        <div class="col-sm-6">
                            <label>Mobile <span class="text-danger">*</span></label>
                            <input type="number"   class="form-control form-control-user" name="mobile" value="{{ old('mobile') ?? $user->mobile ?? ''}}" required="required" maxlength="10">
                            <span id="message"></span>
                            @error('mobile')
                                <span class="text-danger" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror 
                            @if(session('error'))
                            <strong class="text-danger">{{session('error')}}</strong>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            <label>Role <span class="text-danger">*</span></label>
                            <select name="role" class="form-control" required="required">
                                <option >--Select Role--</option>
                                <option value="1"<?php if($user->role ==  '1') echo ' selected="selected"'; ?>>Admin</option>
                                <option value="2"<?php if($user->role == '2') echo ' selected="selected"'; ?>>Customer</option>
                            </select>
                        </div>
                        <?php else :?>
                            <div class="col-sm-3">
                                <label>Mobile <span class="text-danger">*</span></label>
                                <input type="number"  class="form-control form-control-user"  name="mobile" value="{{ old('mobile') ?? $user->mobile ?? ''}}" required="required" maxlength="10">
                                <span id="message"></span>
                                 @error('mobile')
                                <span class="text-danger" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror 
                            </div>
                            <div class="col-sm-3">
                                <label>Role <span class="text-danger">*</span></label>
                                <select name="role" class="form-control">
                                    <option value="1"<?php if($user->role=='1') echo ' selected="selected"'; ?>>Admin</option>
                                    <option value="2"<?php if($user->role=='2') echo ' selected="selected"'; ?>>Customer</option>
                                </select>
                            </div>
                            <?php endif;?>
                            <?php if (isset($password) && !empty($password)): ?>
                            <div class="col-sm-3 mb-3 mb-sm-0">
                                <label>Password <span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-user" id="exampleInputPassword" required="required" name="password" value="{{ old('password') ??  $user->password ?? ''}}" >
                            </div>
                            <div class="col-sm-3">
                                <label>Confirm Password <span class="text-danger">*</span></label>
                                <input type="password" class="form-control form-control-user" name ="password_confirmation" id="exampleRepeatPassword" value="{{ old('password_confirmation') ??  ''}}">
                                @error('password_confirmation')
                                    <span class="text-danger" role="alert">
                                      <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <?php else: ?>
                                <input type="hidden" class="form-control form-control-user" id="exampleInputPassword"  name="password" value="12345678" >
                                <input type="hidden" class="form-control form-control-user" name ="password_confirmation" id="exampleRepeatPassword" value="12345678">
                            <?php endif ?>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 mb-3 mb-sm-0">
                            <label>Date of Birth <span class="text-danger">*</span></label>
                            <input type="date" class="form-control form-control-user" id="exampleInputPassword"  name="dob" value="{{ old('dob') ?? $user->dob ?? ''}}" required="required" >
                            @error('dob')
                                <span class="text-danger" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-sm-3">
                            <label>Company Name</label>
                            <input type="text" class="form-control form-control-user" id="exampleRepeatPassword"  name="company" value="{{ old('company') ?? $user->company ?? ''}}" >
                        </div>
                        <div class="col-sm-3 mb-3 mb-sm-0">
                            <label>Position</label>
                            <input type="text" class="form-control form-control-user" id="exampleInputPassword"   name="position" value="{{ old('position') ?? $user->position ?? ''}}"  pattern="[A-Za-z]+">
                        </div>
                        <div class="col-sm-3">
                            <label>Website</label>
                            <input type="text" class="form-control form-control-user" id="exampleRepeatPassword"  name="website" value="{{ old('website') ?? $user->website ?? ''}}" >
                            @error('website')
                                <span class="text-danger" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>Address <span class="text-danger">*</span></label>
                            <textarea name="address" class="form-control" required="required">{{ old('address') ??  $user->address ?? ''}}</textarea>
                        </div>
                        <div class="col-sm-2">
                            <label>State<span class="text-danger">*</span></label>
                            <select name="state" class="form-control" required="required">
                                <option value="">-Select State- </option>
                                @foreach($states as $state)
                                <option value="{{ $state['name'] }}">{{ $state['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2 mb-3 mb-sm-0">
                            <label>City<span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-user" id="exampleInputPassword"   name="city" value="{{ old('city') ?? $user->city ?? ''}}" required="required">
                        </div>
                        <div class="col-sm-2">
                            <label>Zip-Code<span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-user" id="exampleRepeatPassword"  name="zipcode" value="{{ old('zipcode') ?? $user->zipcode ?? ''}}" maxlength="6" required="required" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label for="enthusiast" >Gender<span class="text-danger">*</span></label><br/>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="male" name="gender" class="custom-control-input" value="0" required="required" <?php if($user->gender == "0"){ echo "checked"; } ?> >
                                <label class="custom-control-label" for="male" style="cursor: pointer;">Male</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="female" name="gender" class="custom-control-input" value="1" required="required" <?php if($user->gender == "1" ) { echo "checked"; } ?> >
                                <label class="custom-control-label" for="female" style="cursor: pointer;">Female</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="other" name="gender" class="custom-control-input" value="2" required="required" <?php if($user->gender ==  2) { echo "checked"; } ?> >
                                <label class="custom-control-label" for="other" style="cursor: pointer;">Other</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                           <label>Featured Image</label>
                           <input type='file' name="fileToUpload" onchange="readURL(this);" alt="your image"  />
                        </div>
                        <?php if( isset( $file->uri ) && !empty( $file->uri ) ) :?>
                           <div class="form-group col-md-3">
                            <img id="blah"  class="img-fluid" src="{{ asset('public/uploads/'.$file->uri ) }}" />
                            </div>
                         <?php else :?>
                            <div class="form-group col-md-3">
                            <img id="blah"  class="img-fluid" src="{{ $user->avatar }}" />
                            </div>
                         <?php endif;?>
                       </div>
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-success" >
                            <i class="bi bi-file-earmark-plus"></i>
                            <span>Save</span>
                        </button>

                        <a class="btn btn-outline-secondary ml-3" href="{{ URL::previous() }}" role="button" title="Go back">
                            <i class="bi bi-caret-left"></i>
                            <span>Back</span>
                        </a>
                    </div>
                </form>
            
            <?php if( isset( $user->userID ) && !empty( $user->userID ) && $store <= 0 ) :?>
            <form action="{{ route('users.destroy', $user->userID )}}" method="post">
                    <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are you sure you want to delete this user?');" title="Delete">
                        <i class="bi bi-trash"></i>
                        <span>Delete this User</span>
                    </button>
                    @csrf
                    @method('DELETE')
                </form>                
            </div>
            
            <?php endif;?>
        </div>
  
    
@endsection	
@section('stylesheets')
<style type="text/css">
    .img-fluid {
        max-height: 10rem;
        object-fit: contain;
        object-position: center;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            alert("Please enter only Numbers.");
            return false;
          }

          return true;
    }

   function check()
{

    var mobile = document.getElementById('mobile');
   
    
    var message = document.getElementById('message');

   var goodColor = "#0C6";
    var badColor = "#FF9B37";
  
    if(mobile.value.length!=10){
       
        mobile.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "required 10 digits, match requested format!"
    }}
</script>

@endsection	