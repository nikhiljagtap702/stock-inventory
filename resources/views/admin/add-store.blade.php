@extends('layouts/admin')
@section('title','Add Store') 
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            Add a Store
        </h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('stores.store') }}" >
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6 mx-auto">
                    <label><span class="text-danger">*</span>Mobile No</label>
                    <input type="tel" name="mobile" value="{{ old('mobile')}}" placeholder="Enter Mobile Number" maxlength="10" class="form-control" required="required">
                    @error('mobile')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @if(session('error'))
                    <strong class="text-danger">{{session('error')}}</strong>
                    @endif
                </div>
            </div>
            <div class="form-group col-md-6 mx-auto">
                <button type="submit" class="btn btn-success">
                    <i class="bi bi-file-earmark-plus"></i><span class="pl-2">Add</span>
                </button>
                <a class="btn btn-outline-secondary" href="{{ url('/admin/stores') }}" role="button" title="Go back">
                   <i class="bi bi-caret-left"></i>
                    <span>Back</span>
                </a>
                <a href="{{ route('owner.create')}}"><strong>Create User</strong></a>
            </form>
        </div>
    </div>
</div>
@endsection
