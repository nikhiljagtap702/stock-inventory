@extends('layouts/admin')
<?php if( isset( $data->storeID ) && !empty( $data->storeID ) ) :?>
    @section('title','Manage Store') 
<?php else :?>
    @section('title','Add Store') 
<?php endif;?>
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            <?php if( isset( $data->storeID ) && !empty( $data->storeID ) ) :?>
              Edit Store
            <?php else :?>
              Add a Store
            <?php endif;?>
        </h6>
    </div>
    <div class="card-body">
        <?php if(isset( $data->storeID) && !empty($data->storeID)) : ?>
            <form method="post" action="{{ route('stores.update', $data->storeID )}}" enctype="multipart/form-data" >
        <?php else : ?>
            <form method="post" action="{{ route('stores.update', 0)}}" enctype="multipart/form-data" >
        <?php endif; ?>
        @csrf
        @method('PUT')
        <div class="form-row">
            <div class="form-group col-md-3">
                <label>Company Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" value="{{ $data->title  ?? '' }}"  name="title" requireds="requireds">
                @error('title')
                <span class="feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group col-md-3">
                <label>Restaurant Name<span class="text-danger">*</span></label>
                <input type="text" class="form-control" value="{{ $data->restaurant_name  ?? '' }}"  name="restaurant_name" requireds="requireds">
                @error('title')
                    <span class="feedback" role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">
                <input type="hidden" name="owner" value="{{ $user->userID ?? '' }}">
                <h6>User Details</h6>
                <table class="table table-sm table-hover table-bordered">
                    <tr>
                        <th width="120">Name</th>
                        <td id="product-title">{{$user->name ?? ''}}</td>
                    </tr>
                    <tr>
                        <th width="120">Mobile</th>
                        <td id="product-category">{{$user->mobile ?? ''}}</td>
                    </tr>
                </table>
            </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>Tagline </label>
                    <input type="text" class="form-control" value="{{ $data->tagline ?? '' }}"  name="tagline">
                    @error('tagline')
                    <span class="feedback" role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label>Keyword </label>
                    <input type="text" class="form-control" value="{{ $data->keywords ?? '' }}"  name="keyword">
                    @error('keyword')
                    <span class="feedback" role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label>Email</label>
                    <input type="Email" name="email" class="form-control"  value="{{ $data->email ?? '' }}">
                </div>
                <div class="form-group col-md-3">
                    <label>Favicon </label>
                    <input type="text" class="form-control" value="{{ $data->favicon ??  '' }}"  name="favicon">
                    @error('favicon')
                    <span class="feedback" role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label>GSTIN <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" requireds="requireds" value="{{ $data->gstin ??  '' }}"  name="gst">
                </div>
                <div class="form-group col-md-3">
                    <label>Payment Status<span class="text-danger">*</span></label>
                    <select class="form-control" name="payment" requireds="requireds">
                        <option value="0" <?php if (isset($data) && $data->payment_status == 0) {
                            echo "selected";
                        } ?>>Enable</option>
                        <option value="1" <?php if (isset($data) && $data->payment_status == 1) {
                            echo "selected";
                        } ?>>Disable</option>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label> Order Limit <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" requireds="requireds" value="{{ $data->order_limit ??  '' }}"  name="order_limit">
                </div>
                <div class="form-group col-md-3">
                    <label> Credit Period <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" requireds="requireds" value="{{ $data->credit_period ??  '' }}"  name="credit_period">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Description</label>
                    <textarea class="form-control"   name="description">{{ $data->description ?? '' }}</textarea>
                    @error('description')
                    <span class="feedback" role="alert">
                      <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-md-3">
                    <label>Featured Image</label>
                    <input type='file' name="fileToUpload" onchange="readURL(this);" alt="your image" />
                    @error('fileToUpload')
                    <span class="feedback" role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <?php if( isset( $data->fileID ) && !empty( $data->fileID ) ){ ?>
                    <div class="form-group col-md-3">
                        <img id="blah"  class="img-fluid" src="{{asset('public/uploads/'.$file->uri )}}" />
                    </div>
                <?php }else{ ?>
                    <div class="form-group col-md-3">
                        <img id="blah" class="img-fluid" src="{{asset('public/uploads/default-product.png')}}" />
                    </div>
                <?php } ?>
            </div>
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary"> Enter Your Billing Address</h6>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Name<span class="text-danger">*</span></label>
                    <input type="text" name="name" class="form-control" requireds="requireds" value="{{ $billing->name ?? '' }}">
                </div>
                <div class="form-group col-md-4">
                    <label>Phone</label>
                    <input type="tel" name="mob" class="form-control" maxlength="10" value="{{ $billing->phone ?? '' }}">
                </div>
                <div class="form-group col-md-4">
                    <label>Address<span class="text-danger">*</span></label>
                    <textarea class="form-control" name="address" requireds="requireds">{{ $billing->address ?? ''}}</textarea>
                </div>
            </div>
            <div class="form-row">
               <div class="form-group col-md-4">
                    <label>Street</label>
                    <input type="text" name="street" class="form-control"  value="{{ $billing->street ?? ''}}">
                </div>
                <div class="form-group col-md-4">
                    <label>Locality</label>
                    <input type="text" name="locality" class="form-control"  value="{{ $billing->locality ?? ''}}">
                </div>
                <div class="form-group col-md-4">
                    <label>Country<span class="text-danger">*</span></label>
                    <select class="form-control">
                        <option value="india">India</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>State<span class="text-danger">*</span></label>
                    <input type="text" name="state" class="form-control" requireds="requireds" value="{{ $billing->state ?? ''}}">
                </div>
                 <div class="form-group col-md-2">
                    <label>City<span class="text-danger">*</span></label>
                    <input type="text" name="city" class="form-control" requireds="requireds" value="{{ $billing->city ?? ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label>Pin Code<span class="text-danger">*</span></label>
                    <input type="text" name="postal_code" class="form-control" requireds="requireds" value="{{ $billing->postal_code ?? ''}}">
                </div>
                <div class="form-group col-md-4 text-center" >
                    <strong>Shipping Address</strong><br/><br>
                    <label>Same as Billing Address<span class="text-danger">*</span></label><br/>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="yes" name="s_address" class="custom-control-input" value="yes" checked="checked" >
                        <label class="custom-control-label" for="yes" style="cursor: pointer;">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="no" name="s_address" class="custom-control-input" value="no" >
                        <label class="custom-control-label" for="no" style="cursor: pointer;">No</label>
                    </div>
                </div> 
            </div>
            <div class="form-row">
                
            </div>
            <section id="shippng">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary"> Enter Your Shipping Address </h6>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Name<span class="text-danger">*</span></label>
                        <input type="text" name="s_name" class="form-control"  value="{{ $shippng->name ?? '' }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Phone<span class="text-danger">*</span></label>
                        <input type="number" name="s_mob" class="form-control"  value="{{ $shippng->phone ?? '' }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Address<span class="text-danger">*</span></label>
                        <textarea class="form-control" name="s_address"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Street<span class="text-danger">*</span></label>
                        <input type="text" name="s_street" class="form-control" value="{{ $shippng->street ?? ''}}">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Locality<span class="text-danger">*</span></label>
                        <input type="text" name="s_locality" class="form-control"  value="{{ $shippng->locality ?? ''}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                    <label>Country<span class="text-danger">*</span></label>
                    <input type="text" name="s_country" class="form-control"  value="{{ $shippng->country ?? ''}}">
                </div>
                <div class="form-group col-md-4">
                    <label>State<span class="text-danger">*</span></label>
                    <input type="text" name="s_state" class="form-control" value="{{ $shippng->state ?? ''}}">
                </div>
                 <div class="form-group col-md-2">
                    <label>City<span class="text-danger">*</span></label>
                    <input type="text" name="s_city" class="form-control"  value="{{ $shippng->city ?? ''}}">
                </div>
                <div class="form-group col-md-2">
                    <label>Pin Code<span class="text-danger">*</span></label>
                    <input type="text" name="s_postal_code" class="form-control"  value="{{ $shippng->postal_code ?? ''}}">
                </div>
             </div>
        </section>
        <div class="button pt-3">
            <button type="submit" class="btn btn-primary">
              <i class="bi bi-file-earmark-plus"></i>
                <?php if( isset( $data->storeID ) && !empty( $data->storeID ) ) :?>
                Update 
                <?php else :?>
                    Add  
                <?php endif;?>
                    
                </button>
                 <a class="btn btn-outline-secondary" href="{{ url('/admin/stores') }}" role="button" title="Go back">
                    <i class="bi bi-caret-left"></i>
                    <span>Back</span>
                  </a>
            </div>
        </form>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>

    document.addEventListener("DOMContentLoaded", function() {
   
        $("#shippng").hide(); 
});

  $(document).ready(function(){
    $("input[type='radio']").change(function(){


        if($(this).val()=="no"){

        $("#shippng").show();

        }else {

        $("#shippng").hide(); 

        }

        });
        });
</script>

<script type="text/javascript">


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            alert("Please enter only Numbers.");
            return false;
          }

          return true;
    }

    function ValidateNo() {
          var phoneNo = document.getElementById('txtPhoneNo');

          if (phoneNo.value == "" || phoneNo.value == null) {
            alert("Please enter your Mobile No.");
            return false;
          }
          if (phoneNo.value.length < 10 || phoneNo.value.length > 10) {
            alert("Mobile No. is not valid, Please Enter 10 Digit Mobile No.");
            return false;
          }

          // alert("Success ");
          // return true;
    }


</script>

@endsection