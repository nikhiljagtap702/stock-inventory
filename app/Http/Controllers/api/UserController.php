<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use App\session;
use App\store;
use App\team;
use Illuminate\Support\Facades\Auth; 
use Validator;

use App\file;
use Illuminate\Support\Facades\Hash;

define( 'APP_NAME', 'supplyPort' );

/**
 * @group Users
 *
 * APIs for managing users
 */
class UserController extends Controller {
    //  Default API status code
    // public $apiStatusCode = 200;
    
    /** 
     * Get user data 
     * 
     * @queryParam userID required Unique user ID Example: 1
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_user( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        $valid = Validator::make( $request->all() , [
             'userID' => ['required'],
        ], [
             'userID.required' => 'Please enter a user ID'
        ]);

        if( !$valid->fails() ) {
            $user = User::latest()->leftjoin('files','users.fileID', '=', 'files.fileID')->select('users.*','files.uri')->where('users.userID',$request->userID)->first();
            if( isset( $user ) && !empty( $user ) ) {
                    $res = array();
                    $res['userID']=$user['userID'];
                    $res['firebaseID']=$user['firebaseID'];
                    $res['fileID']=$user['fileID'];
                    $res['countryPhoneCode']=$user['countryPhoneCode'];
                    $res['mobile']=$user['mobile'];
                    $res['email']=$user['email'];
                    $res['name']=$user['name'];
                    $res['full_name']=$user['full_name'];
                    $res['dob']=$user['dob'];
                    $res['gender']=$user['gender'];
                    $res['company']=$user['company'];
                    $res['position']=$user['position'];
                    $res['website']=$user['website'];
                    $res['address']=$user['address'];
                    $res['city']=$user['city'];
                    $res['state']=$user['state'];
                    $res['zipcode']=$user['zipcode'];
                    $res['verificationNo']=$user['verificationNo'];
                    $res['verificationFileID']=$user['verificationFileID'];
                    $res['verified']=$user['verified'];
                    $res['credits']=$user['credits'];
                    $res['status']=$user['status'];
                    $res['role']=$user['role'];
                    $res['notifications']=$user['notifications'];
                    $res['language']=$user['language'];
                    $res['timezone']=$user['timezone'];
                    $res['timezoneOffset']=$user['timezoneOffset'];
                    $res['notificationHour']=$user['notificationHour'];
                    $res['notified']=$user['notified'];
                    $res['login']=$user['login'];
                    $res['created']=$user['created'];
                    $res['updated']=$user['updated'];
                if(isset( $user['uri'] ) || !empty( $user['uri'] )) {
                    $res['fileUrl']   = url('public/uploads/' . $user['uri'] );
                    $res['avatar']    = url('public/uploads/thumbnail/' . $user['uri'] );
                }else{
                    $res['fileUrl']   = url('public/uploads/user.jpg');
                    $res['avatar']    = url('public/uploads/thumbnail/user.jpg' );

                }
                    
                    //  To be removed
                    $res['avatar'] = $res['fileUrl'];
         }   



            if( $user) {

                  $teams = team::latest('team.created')
                    ->join( 'stores AS s', 'team.storeID', '=', 's.storeID' )
                    ->leftjoin('files AS f', 's.fileID','=','f.fileID')
                    ->select( 's.storeID', 's.fileID', 's.slug', 's.title', 's.description', 'f.uri' )
                    ->where( 'team.userID', '=', $user->userID )
                    ->where( 's.status','=', 1)
                    ->orderBy( 's.title', 'ASC')
                    ->get();
                }

                     if( isset( $teams ) && !empty( $teams ) && count( $teams ) ) {
                        foreach( $teams as $team ) {
                            if( !isset( $team['directory'] ) || empty( $team['directory'] )) {
                                $team['directory'] = 'public/uploads';
                            }
                            $team['uri'] = $team['uri'];

                             if(isset( $team['uri'] ) || !empty( $team['uri'] )) {
                            $team['fileUrl']    = url( $team['directory'] . '/' . $team['uri'] );
                            $team['avatar']     = url( $team['directory'] . '/thumbnail/' . $team['uri'] );
                        }else{
                            $team['fileUrl'] = url( $team['directory'] . '/user.jpg');
                            $team['avatar']     = url( $team['directory'] . '/thumbnail/user.jpg');
                        }

                            unset( $team['uri'] );
                            unset( $team['image'] );
                            unset( $team['directory'] );
                        }
                    }
                  
         
             




                $response['data'] = $res;
                $response['data']['stores'] = $teams;
                $response['status'] = 'success';
                $response['messages'][] = array( 'message' => 'User exists', 'type' => 'success' );
            }else{
                $response['messages'][] = array( 'message' => 'User does not exist', 'type' => 'error' );
            }
        

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }
        return $response;
    }

    /** 
     * User login 
     * 
     * @queryParam mobile required Mobile number Example: 9999999999
     * @queryParam firebaseID required Mobile number Example: 9999999999
     * @queryParam device required Device type Example: android
     * @queryParam deviceID required Mobile number Example: a1b2c3d4
     *
     * @response {
     *      "data": {
     *
     *      },
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */ 
    public function login( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all(), [
            'countryPhoneCode'  => [],
            'mobile'            => ['required'],            
            'firebaseID'        => ['required'],
            'deviceID'          => ['required'],
            'fcmToken'          => ['required'],
            'device'            => ['required'],
            'os'                => [],
            'model'             => [],
            'version'           => [],
            'manufacturer'      => []
        ],[
            'mobile.required'   => 'Please enter your mobile number',
            'firebaseID.required' => 'Please enter a firebase ID',
            'deviceID.required'   => 'Please enter a device ID',
            'fcmToken.required'   => 'Please enter a FCM Token',
        ]);

        //  Add default values
        if( !isset( $request->countryPhoneCode ) || empty( $request->countryPhoneCode ) ) {
            $request->countryPhoneCode = 91;
        }
        $request->countryPhoneCode = 91;

        if( !$valid->fails() ) {
            $user = User::where('mobile', $request->mobile)
            ->where('countryPhoneCode', $request->countryPhoneCode)->first();

            if( $user && $user->status === 1 ) {
                //  Generate token
                if(Auth::loginUsingId(['userID' => $user->userID] ) ) { 
                    //update user table
                    $user= user::find($user->userID);
                    $user ->firebaseID = $request->input('firebaseID');
                    $user->save();

                    $response['data'] = $user;
                    $response['data']['token'] = $user->createToken( APP_NAME )->accessToken;
                    $response['data']['stores'] = array();

                    //  SELECT t.*, s.* FROM `team` t RIGHT JOIN `stores` s ON s.`storeID` = t.`storeID` WHERE t.`userID` = "2"
                    $teams = team::latest('team.created')
                    ->join( 'stores AS s', 'team.storeID', '=', 's.storeID' )
                    ->select( 's.storeID', 's.fileID', 's.slug', 's.title', 's.description', 's.image' )
                    ->where( 'team.userID', '=', $user->userID )
                    ->where( 's.status','=', 1)
                    ->orderBy( 's.title', 'ASC')
                    ->get();

                    if( isset( $teams ) && !empty( $teams ) && count( $teams ) ) {
                        foreach( $teams as $team ) {
                            if( !isset( $team['directory'] ) || empty( $team['directory'] )) {
                                $team['directory'] = 'public/uploads';
                            }
                            $team['uri'] = $team['image'];

                            $team['fileUrl']    = url( $team['directory'] . '/' . $team['image'] );
                            $team['avatar']     = url( $team['directory'] . '/thumbnail/' . $team['image'] );

                            unset( $team['uri'] );
                            unset( $team['image'] );
                            unset( $team['directory'] );
                        }
                        $response['data']['stores'] = $teams;

                        //  Create user session
                        $session_data = array(
                            'userID'        => $user->userID,
                            'fcmToken'      => $request->fcmToken,
                            'device'        => $request->device,
                            'deviceID'      => $request->deviceID,
                            'system'        => $request->os,
                            'model'         => $request->model,
                            'version'       => $request->version,
                            'manufacturer'  => $request->manufacturer,
                        );

                        $response['session'] = session::create( $session_data );
                        // $response['token'] = $response['data']['token'];
                        $response['status'] = 'success';
                        $response['messages'][] = array( 'message' => 'User login successfull', 'type' => 'success' );
                    }else {
                        $response['status'] = 'success';
                        $response['messages'][] = array( 'message' => 'No stores associated to this account', 'type' => 'success' );
                    }
                }else{
                    $response['messages'][] = array( 'message' => 'Unauthorised access', 'type' => 'error' );
                }
            }else if($user && $user->status === 0 ) {
                $response['messages'][] = array( 'message' => 'User verification is pendding', 'type' => 'error' );
            }else{
                $response['messages'][] = array( 'message' => 'User does not exist', 'type' => 'error' );
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }
        return $response;
    }

    /** 
     * Logout user
     * 
     * @queryParam userID required Unique user ID Example: 1
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function logout( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        if (Auth::check()) {
            Auth::user()->token()->revoke();
            $response['status'] = 'success';
            $response['messages'][] = array( 'message' => 'User has been logged out successfully', 'type' => 'error' );
        }else{
            $response['messages'][] = array( 'message' => 'Unable to logout user', 'type' => 'error' );
        }

        return $response;
    }
     /** 
     * Add a user 
     * 
     * @bodyParam name string required Unique user name Example: xyz
     * @bodyParam full_name string optional user full name Example: xyz
     * @bodyParam email string required Unique email ID Example: xyz@gmail.com
     * @bodyParam mobile required mobile number Example: 1234567891
     * @bodyParam dob required Date of Birth Example: 10/06/1997
     * @bodyParam gender int optional user gender Example: male=0 female =1
     * @bodyParam city int optional user city name Example: delhi
     * @bodyParam state  string optional user  state name Example: delhi
     * @bodyParam zipcode int optional user zipcode Example: 1234
     * @bodyParam address string optional   address  Example: xyz
     * @bodyParam fileToUpload optional user file Example: xyz.jpg
     * @bodyParam company optional company  Example: xyz
     * @bodyParam position optional  position  Example: xzy
     * @bodyParam website optional  website  Example: xyz
     *
     * @response {
     *      "data": {
     *   
     *
     *      },
     *      "messaegs": [],
     *      "status": "success"
     * }
     */
     public function store_user(Request $request)  {

        $valid=Validator::make($request->all(),[

            'name'         => ['required', 'string', 'max:255'],
            'full_name'    => [ ],
            'email'        => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'mobile'       => ['required'],
            'dob'          => ['required'],
            'company'      => [],
            'position'     => [],
            'website'      => [],
            'address'      => [],
            'zipcode'       => [],
            'state'        => [],
            'city'         => [],
            'fileToUpload' => [],

        ]);
        if($valid->fails()){
            return response()->json(["error"=>$valid->errors()],401);
        }
        $user =  User::create([
            'name' => $request->input('name'),
            'full_name' => $request->input('full_name'),
            'mobile' => $request->input('mobile'),
            'dob' => $request->input('dob'),
            'company' => $request->input('company'),
            'position' => $request->input('position'),
            'email' => $request->input('email'),
            'website' => $request->input('website'),
            'address' => $request->input('address'),
            'state' => $request->input('state'),
            'city' => $request->input('city'),
            'gender' => $request->input('gender'),
            'zipcode' => $request->input('zipcode'),
            
        ]);
        if($user) {
            if ($request->has('fileToUpload')) {
                $imgUrl = $request->get('fileToUpload');
                //$fileName = array_pop(explode('.', $imgUrl));
                $type = substr($imgUrl, 5, strpos($imgUrl, ';')-5);
                $type = explode("/",$type);
                $type = $type[1];
                $fileName = "abc.".$type;
                $image = file_get_contents($imgUrl);

                $destinationPath = base_path() . '/public/uploads/' . $fileName;

                file_put_contents($destinationPath, $image);
               // $attributes['image'] = $fileName;
            }
            $form_data =array(
                'userID'=> $user->id,
                'fileName' => $fileName
            );
            if($file=file::create($form_data)){
                $user= user::find($user->id);
                $user->fileID = $file->id;
                $user->save();
                return response()->json(["status"=>1, "massage"=>"user created"],200);
            }
        } else{
            return response()->json(["error"=>$valid->errors()],401);
        }
    }
     /** 
     * Upadet   user 
     * 
     * @bodyParam name string required Unique user name Example: xyz
     * @bodyParam full_name string required user full name Example: xyz
     * @bodyParam email string required Unique email ID Example: xyz@gmail.com
     * @bodyParam mobile required mobile number Example: 1234567891
     * @bodyParam dob required Date of Birth Example: 10/06/1997
     * @bodyParam gender int required user gender Example: male=0 female =1
     * @bodyParam city int required user city name Example: delhi
     * @bodyParam state  string required user  state name Example: delhi
     * @bodyParam zipcode int required user zipcode Example: 1234
     * @bodyParam address string required   address  Example: xyz
     * @bodyParam fileToUpload required user file Example: xyz.jpg
     * @bodyParam company required company  Example: xyz
     * @bodyParam position required  position  Example: xzy
     * @bodyParam website required  website  Example: xyz
     *
     * @response {
     *      "data": {
     *             
     *
     *      },
     *      "messaegs": [],
     *      "status": "success"
     * }
     */
     public function put_user( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'userID'    => ['required'],
            'name'      => ['required'],
            'full_name' => ['required'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password'  => ['required'],
            'mobile'    => ['required'],
            'dob'       => ['required'],
            'company'   => ['required'],
            'position'  => ['required'],
            'website'   => ['required'],
            'address'   => ['required'],
            'zipcode'   => ['required'],
            'state'     => ['required'],
            'city'      => ['required'],
    
        ]);

        if( !$valid->fails() ) {
            $response['status'] = 'success';
            $user = user::find( $request->userID );           
            $data = array(
                'name'      => $request->name,
                'full_name' => $request->full_name,
                'email'     => $request->email,
                'mobile'    => $request->mobile,
                'dob'       => $request->dob,
                'company'   => $request->company,
                'position'  => $request->position,
                'website'   => $request->website,
                'address'   => $request->address,
                'zipcode'   => $request->zipcode,
                'state'     => $request->state,
                'city'      => $request->city,
            );
            $user->update($data);
            $response['data'] = $user;
            $response['messages'][] = array( 'message' => 'User  has been Updated successfully', 'type' => 'success' );
        }
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

}
