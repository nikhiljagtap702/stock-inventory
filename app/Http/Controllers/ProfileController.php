<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\file;
use App\team;
use App\store;
use App\order;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $data = User::find($id);
         $file = file::find($data->fileID);

         $store = team::latest('team.created')
                ->join( 'stores AS s', 'team.storeID', '=', 's.storeID' )
                ->select( 's.storeID', 's.fileID', 's.slug', 's.title', 's.description' )
                ->where( 'team.userID', '=', $id )
                ->orderBy( 's.title', 'ASC')
                ->get();
         $order = order::where('userID',$data->userID)->count();

        return view('admin.profile' , compact('data','file','store','order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
