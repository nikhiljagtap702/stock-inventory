---
title: API Reference

language_tabs:
- bash
- javascript
- php

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://supplyport.in/docs/collection.json)

<!-- END_INFO -->

#Brands


APIs for managing brands
<!-- START_ec5d69c57dff08a10fad5f9cf208ad9f -->
## Get brands details

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/brand?brandID=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/brand"
);

let params = {
    "brandID": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/brand',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'brandID'=> '1',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {
        "brandID": 1,
        "fileID": 0,
        "title": "Brand name",
        "description": "Brief description",
        "body": "Exteded description about the brand",
        "fileUrl": "",
        "avatar": ""
    },
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/brand`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `brandID` |  required  | Unique brand ID

<!-- END_ec5d69c57dff08a10fad5f9cf208ad9f -->

<!-- START_49314ee162f7e839596684af0fed40e9 -->
## Get all brands

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/brands?page=1&limit=10" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/brands"
);

let params = {
    "page": "1",
    "limit": "10",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/brands',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'page'=> '1',
            'limit'=> '10',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {
            "brandID": 1,
            "fileID": 0,
            "title": "Brand name",
            "description": "Brief description",
            "body": "Exteded description about the brand",
            "fileUrl": "",
            "avatar": ""
        }
    ],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/brands`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `page` |  optional  | Page number
    `limit` |  optional  | number of items per request

<!-- END_49314ee162f7e839596684af0fed40e9 -->

#Feedbacks


APIs for managing feedbacks
<!-- START_c5664a28affeeb80e3b2c0ef796790a4 -->
## Manage feedback register data

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/feedback/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"store_name":"xyz","subject":"Unable to create an order","message":"This is a test message","referenceID":"abc1234","name":"xyz","email":"xyz@gmail.com","mobile":"68415324655"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/feedback/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "store_name": "xyz",
    "subject": "Unable to create an order",
    "message": "This is a test message",
    "referenceID": "abc1234",
    "name": "xyz",
    "email": "xyz@gmail.com",
    "mobile": "68415324655"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/feedback/register',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'store_name' => 'xyz',
            'subject' => 'Unable to create an order',
            'message' => 'This is a test message',
            'referenceID' => 'abc1234',
            'name' => 'xyz',
            'email' => 'xyz@gmail.com',
            'mobile' => '68415324655',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "status": "success"
}
```

### HTTP Request
`POST api/feedback/register`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `store_name` | optional |  optional  | store_name
        `subject` | optional |  optional  | What is this feedback about
        `message` | required |  optional  | Feedback message or query
        `referenceID` | optional |  optional  | Any reference ID
        `name` | optional |  optional  | name
        `email` | optional |  optional  | email
        `mobile` | optional |  optional  | mobile
    
<!-- END_c5664a28affeeb80e3b2c0ef796790a4 -->

<!-- START_cadad8c9a7cbcb11ced37c6856fbbc6a -->
## Manage feedback data

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/feedback" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":"1","storeID":"2","subject":"Unable to create an order","message":"This is a test message","module":"query","company_name":"xyz","referenceID":"abc1234"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/feedback"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": "1",
    "storeID": "2",
    "subject": "Unable to create an order",
    "message": "This is a test message",
    "module": "query",
    "company_name": "xyz",
    "referenceID": "abc1234"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/feedback',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => '1',
            'storeID' => '2',
            'subject' => 'Unable to create an order',
            'message' => 'This is a test message',
            'module' => 'query',
            'company_name' => 'xyz',
            'referenceID' => 'abc1234',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "status": "success"
}
```

### HTTP Request
`POST api/feedback`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | required |  optional  | Unique user ID
        `storeID` | required |  optional  | Unique store ID
        `subject` | optional |  optional  | What is this feedback about
        `message` | required |  optional  | Feedback message or query
        `module` | optional |  optional  | Feedback module
        `company_name` | optional |  optional  | Company name
        `referenceID` | optional |  optional  | Any reference ID
    
<!-- END_cadad8c9a7cbcb11ced37c6856fbbc6a -->

#Notifications


APIs for managing notifications
<!-- START_e65df2963c4f1f0bfdd426ee5170e8b7 -->
## Fetch notifications

Fetch notifications paginated by 10 per page

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/notifications?page=1&limit=10&userID=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/notifications"
);

let params = {
    "page": "1",
    "limit": "10",
    "userID": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/notifications',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'page'=> '1',
            'limit'=> '10',
            'userID'=> '1',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/notifications`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `page` |  optional  | Page number
    `limit` |  optional  | Number of items per request
    `userID` |  optional  | int required Unique user ID

<!-- END_e65df2963c4f1f0bfdd426ee5170e8b7 -->

#Offers


APIs for managing Offers
<!-- START_2957e28f2749a64b9bcd030a047be524 -->
## Get offers details

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/offer?offerID=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/offer"
);

let params = {
    "offerID": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/offer',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'offerID'=> '1',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {
        "offerID": 1,
        "fileID": 0,
        "title": "offer name",
        "description": "Brief description",
        "body": "Exteded description about the offer",
        "fileUrl": "",
        "avatar": ""
    },
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/offer`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `offerID` |  required  | Unique offer ID

<!-- END_2957e28f2749a64b9bcd030a047be524 -->

<!-- START_2741c31dd89e5d192b39977e6ec69bb2 -->
## Get all offers

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/offers?page=1&limit=10" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/offers"
);

let params = {
    "page": "1",
    "limit": "10",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/offers',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'page'=> '1',
            'limit'=> '10',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {
            "offerID": 1,
            "fileID": 0,
            "title": "offer name",
            "description": "Brief description",
            "body": "Exteded description about the offer",
            "fileUrl": "",
            "avatar": ""
        }
    ],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/offers`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `page` |  optional  | Page number
    `limit` |  optional  | number of items per request

<!-- END_2741c31dd89e5d192b39977e6ec69bb2 -->

#Orders


APIs for managing orders
<!-- START_f9301c03a9281c0847565f96e6f723de -->
## Get orders

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234,"storeID":1234,"order_status":"unpaid","shipping_status":0,"page":1,"limit":10}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234,
    "storeID": 1234,
    "order_status": "unpaid",
    "shipping_status": 0,
    "page": 1,
    "limit": 10
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/orders',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
            'storeID' => 1234,
            'order_status' => 'unpaid',
            'shipping_status' => 0,
            'page' => 1,
            'limit' => 10,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "status": "success"
}
```

### HTTP Request
`GET api/orders`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  required  | Unique user ID
        `storeID` | integer |  required  | Unique store ID
        `order_status` | string |  optional  | Order status
        `shipping_status` | integer |  optional  | Order shipping status
        `page` | integer |  optional  | page number
        `limit` | integer |  optional  | limit number
    
<!-- END_f9301c03a9281c0847565f96e6f723de -->

<!-- START_d2e080af51835880674d3e2496ed6e62 -->
## Get order details

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/order" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234,"storeID":1234,"orderID":1234}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/order"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234,
    "storeID": 1234,
    "orderID": 1234
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/order',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
            'storeID' => 1234,
            'orderID' => 1234,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "status": "success"
}
```

### HTTP Request
`GET api/order`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  required  | Unique user ID
        `storeID` | integer |  required  | Unique store ID
        `orderID` | integer |  required  | Unique order ID
    
<!-- END_d2e080af51835880674d3e2496ed6e62 -->

<!-- START_cd95d3e90339c282e0b608349e80a381 -->
## Manage order data

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/order" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234,"storeID":1234,"orderID":1234,"status":1}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/order"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234,
    "storeID": 1234,
    "orderID": 1234,
    "status": 1
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/order',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
            'storeID' => 1234,
            'orderID' => 1234,
            'status' => 1,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "status": "success"
}
```

### HTTP Request
`POST api/order`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  required  | Unique user ID
        `storeID` | integer |  required  | Unique store ID
        `orderID` | integer |  required  | Unique order ID
        `status` | integer |  required  | Unique order status
    
<!-- END_cd95d3e90339c282e0b608349e80a381 -->

<!-- START_79732098f2191361157da91553c8a836 -->
## Post order payment details

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/order/payment" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234,"storeID":1234,"orderID":1234,"transactionID":1234,"amount":1234,"type":"credit,debit","method":"xyz"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/order/payment"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234,
    "storeID": 1234,
    "orderID": 1234,
    "transactionID": 1234,
    "amount": 1234,
    "type": "credit,debit",
    "method": "xyz"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/order/payment',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
            'storeID' => 1234,
            'orderID' => 1234,
            'transactionID' => 1234,
            'amount' => 1234,
            'type' => 'credit,debit',
            'method' => 'xyz',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`POST api/order/payment`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  required  | Unique user ID
        `storeID` | integer |  required  | Unique store ID
        `orderID` | integer |  required  | Unique order ID
        `transactionID` | integer |  required  | Unique transaction ID
        `amount` | integer |  required  | amount
        `type` | string |  required  | type
        `method` | string |  required  | method
    
<!-- END_79732098f2191361157da91553c8a836 -->

<!-- START_d3c276a827d4d583d2c333c5b23aafdd -->
## Post Transaction details

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/order/transaction" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234,"storeID":1234,"fileID":1234,"transactionID":1234,"amount":1234,"type":"credit,debit","method":"xyz"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/order/transaction"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234,
    "storeID": 1234,
    "fileID": 1234,
    "transactionID": 1234,
    "amount": 1234,
    "type": "credit,debit",
    "method": "xyz"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/order/transaction',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
            'storeID' => 1234,
            'fileID' => 1234,
            'transactionID' => 1234,
            'amount' => 1234,
            'type' => 'credit,debit',
            'method' => 'xyz',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`POST api/order/transaction`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  required  | Unique user ID
        `storeID` | integer |  required  | Unique store ID
        `fileID` | integer |  required  | Unique file ID
        `transactionID` | integer |  required  | Unique transaction ID
        `amount` | integer |  required  | amount
        `type` | string |  required  | type
        `method` | string |  required  | method
    
<!-- END_d3c276a827d4d583d2c333c5b23aafdd -->

<!-- START_bac143ac4a8c5cb801f7f670b6754d0e -->
## Get order shipping data

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/order/shipping" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234,"storeID":1234,"orderID":1234}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/order/shipping"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234,
    "storeID": 1234,
    "orderID": 1234
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/order/shipping',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
            'storeID' => 1234,
            'orderID' => 1234,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/order/shipping`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  required  | Unique user ID
        `storeID` | integer |  required  | Unique store ID
        `orderID` | integer |  required  | Unique order ID
    
<!-- END_bac143ac4a8c5cb801f7f670b6754d0e -->

<!-- START_8896bf33ad9a0011f0d5b223a9a833db -->
## Get order product details

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/order/products" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"orderID":1234}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/order/products"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "orderID": 1234
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/order/products',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'orderID' => 1234,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/order/products`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `orderID` | integer |  required  | Unique order ID
    
<!-- END_8896bf33ad9a0011f0d5b223a9a833db -->

<!-- START_2da92c75671e5ad31c1149c52d737add -->
## Order Repeat

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/order/repeat" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234,"storeID":1234}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/order/repeat"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234,
    "storeID": 1234
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/order/repeat',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
            'storeID' => 1234,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "status": "success"
}
```

### HTTP Request
`GET api/order/repeat`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  required  | Unique user ID
        `storeID` | integer |  required  | Unique store ID
    
<!-- END_2da92c75671e5ad31c1149c52d737add -->

#Products


APIs for managing products
<!-- START_86e0ac5d4f8ce9853bc22fd08f2a0109 -->
## Get products data

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/products?storeID=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/products"
);

let params = {
    "storeID": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/products',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'storeID'=> '1',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/products`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `storeID` |  optional  | int optional Unique store ID

<!-- END_86e0ac5d4f8ce9853bc22fd08f2a0109 -->

<!-- START_dc538d69a8586a7a3c36d4393cee42e6 -->
## Get product details

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/product" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/product"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/product',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/product`


<!-- END_dc538d69a8586a7a3c36d4393cee42e6 -->

<!-- START_a21b95a62b90df6add3af3abc366f0cb -->
## Filter products

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/products/filters" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"userID":1234}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/products/filters"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "userID": 1234
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/products/filters',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'userID' => 1234,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [
        {}
    ],
    "status": "success"
}
```

### HTTP Request
`GET api/products/filters`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `userID` | integer |  optional  | optional user ID
    
<!-- END_a21b95a62b90df6add3af3abc366f0cb -->

<!-- START_f1e6c191e499ba96bca7cb780992a90e -->
## Post file

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/file/upload" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"fileToUpload":0}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/file/upload"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "fileToUpload": 0
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/file/upload',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'fileToUpload' => 0,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`POST api/file/upload`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `fileToUpload` | integer |  required  | file To Upload
    
<!-- END_f1e6c191e499ba96bca7cb780992a90e -->

#Stores


APIs for managing stores
<!-- START_3f8761ce88b9d8916be2cc7506661dc3 -->
## Add a Client

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"title":"xyz","slug":"xyz","tagline":"xyz","domain":"xyz.com","gstin":"1lkfb","description":"xyz","keywords":"xyz","userID":122}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "title": "xyz",
    "slug": "xyz",
    "tagline": "xyz",
    "domain": "xyz.com",
    "gstin": "1lkfb",
    "description": "xyz",
    "keywords": "xyz",
    "userID": 122
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/store',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'title' => 'xyz',
            'slug' => 'xyz',
            'tagline' => 'xyz',
            'domain' => 'xyz.com',
            'gstin' => '1lkfb',
            'description' => 'xyz',
            'keywords' => 'xyz',
            'userID' => 122,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`POST api/store`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `title` | string |  required  | Unique title
        `slug` | string |  optional  | optional  slug
        `tagline` | string |  optional  | optional  optional
        `domain` | required |  optional  | domain
        `gstin` | required |  optional  | GST number
        `description` | string |  optional  | optional description
        `keywords` | string |  optional  | optional keywords
        `userID` | integer |  required  | store owner userID name
    
<!-- END_3f8761ce88b9d8916be2cc7506661dc3 -->

<!-- START_2eaf2769ae1e92d4e2a261fba491136e -->
## Get store brand analytics

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/analytics/brands" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"storeID":1234,"start_date":"2020-01-01","end_date":"2020-03-31"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/analytics/brands"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "storeID": 1234,
    "start_date": "2020-01-01",
    "end_date": "2020-03-31"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/analytics/brands',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'storeID' => 1234,
            'start_date' => '2020-01-01',
            'end_date' => '2020-03-31',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/analytics/brands`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `storeID` | integer |  required  | Unique store ID
        `start_date` | date |  optional  | optional Analytics start date
        `end_date` | date |  optional  | optional Analytics end date
    
<!-- END_2eaf2769ae1e92d4e2a261fba491136e -->

<!-- START_b03e0eeb5aed032bc5fb65ec841603c8 -->
## Get store orders analytics

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/analytics/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"storeID":1234,"start_date":"2020-01-01","end_date":"2020-03-31"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/analytics/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "storeID": 1234,
    "start_date": "2020-01-01",
    "end_date": "2020-03-31"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/analytics/orders',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'storeID' => 1234,
            'start_date' => '2020-01-01',
            'end_date' => '2020-03-31',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/analytics/orders`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `storeID` | integer |  required  | Unique store ID
        `start_date` | date |  optional  | optional Analytics start date
        `end_date` | date |  optional  | optional  Analytics end date
    
<!-- END_b03e0eeb5aed032bc5fb65ec841603c8 -->

<!-- START_9b673c164ebd8542c20add444f3cd74e -->
## Get store products analytics

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/analytics/products" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"storeID":1234,"start_date":"2020-01-01","end_date":"2020-03-31"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/analytics/products"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "storeID": 1234,
    "start_date": "2020-01-01",
    "end_date": "2020-03-31"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/analytics/products',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'storeID' => 1234,
            'start_date' => '2020-01-01',
            'end_date' => '2020-03-31',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": [],
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/analytics/products`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `storeID` | integer |  required  | Unique store ID
        `start_date` | date |  optional  | optional Analytics start date
        `end_date` | date |  optional  | optional Analytics end date
    
<!-- END_9b673c164ebd8542c20add444f3cd74e -->

#Users


APIs for managing users
<!-- START_57e3b4272508c324659e49ba5758c70f -->
## User login

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/user/login?mobile=9999999999&firebaseID=9999999999&device=android&deviceID=a1b2c3d4" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/user/login"
);

let params = {
    "mobile": "9999999999",
    "firebaseID": "9999999999",
    "device": "android",
    "deviceID": "a1b2c3d4",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/user/login',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'mobile'=> '9999999999',
            'firebaseID'=> '9999999999',
            'device'=> 'android',
            'deviceID'=> 'a1b2c3d4',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`POST api/user/login`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `mobile` |  required  | Mobile number
    `firebaseID` |  required  | Mobile number
    `device` |  required  | Device type
    `deviceID` |  required  | Mobile number

<!-- END_57e3b4272508c324659e49ba5758c70f -->

<!-- START_2b6e5a4b188cb183c7e59558cce36cb6 -->
## Get user data

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/user?userID=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/user"
);

let params = {
    "userID": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/user',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'userID'=> '1',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/user`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `userID` |  required  | Unique user ID

<!-- END_2b6e5a4b188cb183c7e59558cce36cb6 -->

<!-- START_f0654d3f2fc63c11f5723f233cc53c83 -->
## Add a user

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"xyz","full_name":"xyz","email":"xyz@gmail.com","mobile":"1234567891","dob":"10\/06\/1997","gender":0,"city":0,"state":"delhi","zipcode":1234,"address":"xyz","fileToUpload":"xyz.jpg","company":"xyz","position":"xzy","website":"xyz"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "xyz",
    "full_name": "xyz",
    "email": "xyz@gmail.com",
    "mobile": "1234567891",
    "dob": "10\/06\/1997",
    "gender": 0,
    "city": 0,
    "state": "delhi",
    "zipcode": 1234,
    "address": "xyz",
    "fileToUpload": "xyz.jpg",
    "company": "xyz",
    "position": "xzy",
    "website": "xyz"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/user',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'xyz',
            'full_name' => 'xyz',
            'email' => 'xyz@gmail.com',
            'mobile' => '1234567891',
            'dob' => '10/06/1997',
            'gender' => 0,
            'city' => 0,
            'state' => 'delhi',
            'zipcode' => 1234,
            'address' => 'xyz',
            'fileToUpload' => 'xyz.jpg',
            'company' => 'xyz',
            'position' => 'xzy',
            'website' => 'xyz',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`POST api/user`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | Unique user name
        `full_name` | string |  optional  | optional user full name
        `email` | string |  required  | Unique email ID
        `mobile` | required |  optional  | mobile number
        `dob` | required |  optional  | Date of Birth
        `gender` | integer |  optional  | optional user gender
        `city` | integer |  optional  | optional user city name
        `state` | string |  optional  | optional user  state name
        `zipcode` | integer |  optional  | optional user zipcode
        `address` | string |  optional  | optional   address
        `fileToUpload` | optional |  optional  | user file
        `company` | optional |  optional  | company
        `position` | optional |  optional  | position
        `website` | optional |  optional  | website
    
<!-- END_f0654d3f2fc63c11f5723f233cc53c83 -->

<!-- START_ce7af384a8eb198ec28d752586f4ff17 -->
## Logout user

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/api/user/logout?userID=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/user/logout"
);

let params = {
    "userID": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/api/user/logout',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'userID'=> '1',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`POST api/user/logout`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `userID` |  required  | Unique user ID

<!-- END_ce7af384a8eb198ec28d752586f4ff17 -->

<!-- START_bbcc142cd49fac752f86cf88014951ee -->
## Get app dashboard

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/dashboard?userID=1&storeID=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"start_date":"2020-01-01","end_date":"2020-03-31"}'

```

```javascript
const url = new URL(
    "https://supplyport.in/api/dashboard"
);

let params = {
    "userID": "1",
    "storeID": "1",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "start_date": "2020-01-01",
    "end_date": "2020-03-31"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/dashboard',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'query' => [
            'userID'=> '1',
            'storeID'=> '1',
        ],
        'json' => [
            'start_date' => '2020-01-01',
            'end_date' => '2020-03-31',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "data": {},
    "messaegs": [],
    "status": "success"
}
```

### HTTP Request
`GET api/dashboard`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `userID` |  required  | Unique user ID
    `storeID` |  required  | Unique store ID
#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `start_date` | date |  optional  | optional Analytics start date
        `end_date` | date |  optional  | optional  Analytics end date
    
<!-- END_bbcc142cd49fac752f86cf88014951ee -->

<!-- START_1efcaa0a66d4ad882fa0da5f1b86d798 -->
## api/analytics
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/analytics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/analytics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/analytics',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "status": "success",
    "data": {
        "orders": [],
        "brands": [
            {
                "brandID": 23,
                "brand_name": "Monin",
                "total_val": "3",
                "total_amount": "3150.60"
            },
            {
                "brandID": 1,
                "brand_name": "Bisleri",
                "total_val": "3",
                "total_amount": "35.40"
            },
            {
                "brandID": 23,
                "brand_name": null,
                "total_val": "5",
                "total_amount": "5251.00"
            },
            {
                "brandID": 5,
                "brand_name": null,
                "total_val": "8",
                "total_amount": "12699.68"
            },
            {
                "brandID": 1,
                "brand_name": "2",
                "total_val": "8",
                "total_amount": "202.96"
            },
            {
                "brandID": 2,
                "brand_name": null,
                "total_val": "2",
                "total_amount": "236.00"
            },
            {
                "brandID": null,
                "brand_name": null,
                "total_val": "40",
                "total_amount": "7341.40"
            },
            {
                "brandID": 1,
                "brand_name": null,
                "total_val": "43",
                "total_amount": "4436.80"
            }
        ],
        "products": [],
        "0": {
            "orderproductChart": {
                "dataset": "ConsoleTVs\\Charts\\Classes\\Chartjs\\Dataset",
                "id": "mqyotcwuklvdhpfibgxjznsra",
                "datasets": [
                    {
                        "name": "Order-Product Value Reporting",
                        "type": "line",
                        "values": [],
                        "options": {
                            "borderWidth": 2,
                            "borderColor": "rgb(22,160,133, 0.2)",
                            "backgroundColor": "rgb(34,198,246, 0.2)"
                        },
                        "undefinedColor": "#22292F"
                    },
                    {
                        "name": "Product-Amount",
                        "type": "line",
                        "values": [],
                        "options": {
                            "borderWidth": 2,
                            "borderColor": "rgb(255, 99, 132)",
                            "backgroundColor": "rgb(255, 205, 86, 0.2)"
                        },
                        "undefinedColor": "#22292F"
                    }
                ],
                "labels": [],
                "container": "charts::chartjs.container",
                "options": {
                    "maintainAspectRatio": false,
                    "scales": {
                        "xAxes": [],
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true
                                }
                            }
                        ]
                    }
                },
                "plugins": [],
                "pluginsViews": [],
                "script": "charts::chartjs.script",
                "type": "",
                "api_url": "",
                "loader": true,
                "loaderColor": "#22292F",
                "height": 400,
                "width": null,
                "scriptAttributes": {
                    "type": "text\/javascript"
                }
            },
            "brandChart": {
                "dataset": "ConsoleTVs\\Charts\\Classes\\Chartjs\\Dataset",
                "id": "uawfgkdnsrptcizbvomjleyxh",
                "datasets": [
                    {
                        "name": "Order-Brand Reporting",
                        "type": "line",
                        "values": [
                            "3",
                            "3",
                            "5",
                            "8",
                            "8",
                            "2",
                            "40",
                            "43"
                        ],
                        "options": {
                            "borderWidth": 2,
                            "borderColor": "rgb(255, 99, 132)",
                            "backgroundColor": "rgb(255, 99, 132)",
                            "fill": false,
                            "lineTension": 0.1,
                            "borderDash": [
                                5
                            ]
                        },
                        "undefinedColor": "#22292F"
                    }
                ],
                "labels": [
                    "Monin",
                    "Bisleri",
                    null,
                    null,
                    "2",
                    null,
                    null,
                    null
                ],
                "container": "charts::chartjs.container",
                "options": {
                    "maintainAspectRatio": false,
                    "scales": {
                        "xAxes": [],
                        "yAxes": [
                            {
                                "ticks": {
                                    "beginAtZero": true
                                }
                            }
                        ]
                    }
                },
                "plugins": [],
                "pluginsViews": [],
                "script": "charts::chartjs.script",
                "type": "",
                "api_url": "",
                "loader": true,
                "loaderColor": "#22292F",
                "height": 400,
                "width": null,
                "scriptAttributes": {
                    "type": "text\/javascript"
                }
            }
        }
    },
    "messages": []
}
```

### HTTP Request
`GET api/analytics`


<!-- END_1efcaa0a66d4ad882fa0da5f1b86d798 -->

#general


<!-- START_0c068b4037fb2e47e71bd44bd36e3e2a -->
## Authorize a client to access the user&#039;s account.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/oauth/authorize" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/authorize"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/oauth/authorize',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET oauth/authorize`


<!-- END_0c068b4037fb2e47e71bd44bd36e3e2a -->

<!-- START_e48cc6a0b45dd21b7076ab2c03908687 -->
## Approve the authorization request.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/oauth/authorize" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/authorize"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/oauth/authorize',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST oauth/authorize`


<!-- END_e48cc6a0b45dd21b7076ab2c03908687 -->

<!-- START_de5d7581ef1275fce2a229b6b6eaad9c -->
## Deny the authorization request.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/oauth/authorize" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/authorize"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/oauth/authorize',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE oauth/authorize`


<!-- END_de5d7581ef1275fce2a229b6b6eaad9c -->

<!-- START_a09d20357336aa979ecd8e3972ac9168 -->
## Authorize a client to access the user&#039;s account.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/oauth/token" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/token"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/oauth/token',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST oauth/token`


<!-- END_a09d20357336aa979ecd8e3972ac9168 -->

<!-- START_d6a56149547e03307199e39e03e12d1c -->
## Get all of the authorized tokens for the authenticated user.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/oauth/tokens" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/tokens"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/oauth/tokens',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET oauth/tokens`


<!-- END_d6a56149547e03307199e39e03e12d1c -->

<!-- START_a9a802c25737cca5324125e5f60b72a5 -->
## Delete the given token.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/oauth/tokens/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/tokens/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/oauth/tokens/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE oauth/tokens/{token_id}`


<!-- END_a9a802c25737cca5324125e5f60b72a5 -->

<!-- START_abe905e69f5d002aa7d26f433676d623 -->
## Get a fresh transient token cookie for the authenticated user.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/oauth/token/refresh" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/token/refresh"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/oauth/token/refresh',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST oauth/token/refresh`


<!-- END_abe905e69f5d002aa7d26f433676d623 -->

<!-- START_babcfe12d87b8708f5985e9d39ba8f2c -->
## Get all of the clients for the authenticated user.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/oauth/clients" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/clients"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/oauth/clients',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET oauth/clients`


<!-- END_babcfe12d87b8708f5985e9d39ba8f2c -->

<!-- START_9eabf8d6e4ab449c24c503fcb42fba82 -->
## Store a new client.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/oauth/clients" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/clients"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/oauth/clients',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST oauth/clients`


<!-- END_9eabf8d6e4ab449c24c503fcb42fba82 -->

<!-- START_784aec390a455073fc7464335c1defa1 -->
## Update the given client.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/oauth/clients/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/clients/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/oauth/clients/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT oauth/clients/{client_id}`


<!-- END_784aec390a455073fc7464335c1defa1 -->

<!-- START_1f65a511dd86ba0541d7ba13ca57e364 -->
## Delete the given client.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/oauth/clients/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/clients/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/oauth/clients/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE oauth/clients/{client_id}`


<!-- END_1f65a511dd86ba0541d7ba13ca57e364 -->

<!-- START_9e281bd3a1eb1d9eb63190c8effb607c -->
## Get all of the available scopes for the application.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/oauth/scopes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/scopes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/oauth/scopes',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET oauth/scopes`


<!-- END_9e281bd3a1eb1d9eb63190c8effb607c -->

<!-- START_9b2a7699ce6214a79e0fd8107f8b1c9e -->
## Get all of the personal access tokens for the authenticated user.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/oauth/personal-access-tokens" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/personal-access-tokens"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/oauth/personal-access-tokens',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET oauth/personal-access-tokens`


<!-- END_9b2a7699ce6214a79e0fd8107f8b1c9e -->

<!-- START_a8dd9c0a5583742e671711f9bb3ee406 -->
## Create a new personal access token for the user.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/oauth/personal-access-tokens" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/personal-access-tokens"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/oauth/personal-access-tokens',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST oauth/personal-access-tokens`


<!-- END_a8dd9c0a5583742e671711f9bb3ee406 -->

<!-- START_bae65df80fd9d72a01439241a9ea20d0 -->
## Delete the given token.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/oauth/personal-access-tokens/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/oauth/personal-access-tokens/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/oauth/personal-access-tokens/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE oauth/personal-access-tokens/{token_id}`


<!-- END_bae65df80fd9d72a01439241a9ea20d0 -->

<!-- START_b8171a4fd016c86f64f8106d911dabd2 -->
## api/faqs
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/api/faqs" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/api/faqs"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/api/faqs',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "status": "success",
    "data": [
        {
            "nodeID": 1,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 2,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 3,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 4,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 5,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 6,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 7,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 8,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 9,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        },
        {
            "nodeID": 10,
            "title": "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "body": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        }
    ],
    "messages": []
}
```

### HTTP Request
`GET api/faqs`


<!-- END_b8171a4fd016c86f64f8106d911dabd2 -->

<!-- START_679ea4e19d49028fd5a7bd6ee9f0f308 -->
## contact
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/contact" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/contact"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/contact',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET contact`


<!-- END_679ea4e19d49028fd5a7bd6ee9f0f308 -->

<!-- START_5d6ce2ec72b9360a929e0425b42a2a8a -->
## terms
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/terms" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/terms"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/terms',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET terms`


<!-- END_5d6ce2ec72b9360a929e0425b42a2a8a -->

<!-- START_b31353d8308de761c8cb9e4d41a07df3 -->
## privacy
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/privacy" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/privacy"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/privacy',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET privacy`


<!-- END_b31353d8308de761c8cb9e4d41a07df3 -->

<!-- START_f2eb1b4afdba987dadfd79d23b503733 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/notifications" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/notifications"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/notifications',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/notifications`


<!-- END_f2eb1b4afdba987dadfd79d23b503733 -->

<!-- START_02bda79f8ddc2b99eb2bdb6413424103 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/notifications/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/notifications/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/notifications/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/notifications/create`


<!-- END_02bda79f8ddc2b99eb2bdb6413424103 -->

<!-- START_ca0486e490b8bffc7eb36ad1df1b1283 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/notifications" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/notifications"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/notifications',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/notifications`


<!-- END_ca0486e490b8bffc7eb36ad1df1b1283 -->

<!-- START_3516d95b5bbf683fc4d0a3603cd4fe14 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/notifications/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/notifications/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/notifications/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/notifications/{notification}`


<!-- END_3516d95b5bbf683fc4d0a3603cd4fe14 -->

<!-- START_6e16c9f6db3a633fed1dba32ad04dc97 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/notifications/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/notifications/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/notifications/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/notifications/{notification}/edit`


<!-- END_6e16c9f6db3a633fed1dba32ad04dc97 -->

<!-- START_859f0ecdd4c6a0114f976a68d6a51690 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/notifications/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/notifications/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/notifications/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/notifications/{notification}`

`PATCH admin/notifications/{notification}`


<!-- END_859f0ecdd4c6a0114f976a68d6a51690 -->

<!-- START_eff1769b68a5be740b38949064983e7c -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/notifications/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/notifications/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/notifications/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/notifications/{notification}`


<!-- END_eff1769b68a5be740b38949064983e7c -->

<!-- START_e98ad77a3f1792da07c25059f8e13676 -->
## sendbasicemail
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/sendbasicemail" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/sendbasicemail"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/sendbasicemail',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET sendbasicemail`


<!-- END_e98ad77a3f1792da07c25059f8e13676 -->

<!-- START_7a6ce5fc1feace323e4bb5b36cedee7b -->
## admin/feedbacks
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/feedbacks" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/feedbacks"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/feedbacks',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/feedbacks`


<!-- END_7a6ce5fc1feace323e4bb5b36cedee7b -->

<!-- START_c43923c65631ded2c2cbfda87d340052 -->
## admin/feedbacks/store
> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/feedbacks/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/feedbacks/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/feedbacks/store',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/feedbacks/store`


<!-- END_c43923c65631ded2c2cbfda87d340052 -->

<!-- START_56e3d95d96030557d8f37451a1213d1e -->
## admin/feedback/edit
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/feedback/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/feedback/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/feedback/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/feedback/edit`


<!-- END_56e3d95d96030557d8f37451a1213d1e -->

<!-- START_3a0a2eabdf9d29b5aa9b91652f9e0ab6 -->
## admin/feedback/show/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/feedback/show/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/feedback/show/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/feedback/show/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/feedback/show/{id}`


<!-- END_3a0a2eabdf9d29b5aa9b91652f9e0ab6 -->

<!-- START_8b38224b6ff1dfcb9233ae970fc321eb -->
## admin/feedbacks/{type?}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/feedbacks/" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/feedbacks/"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/feedbacks/',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/feedbacks/{type?}`


<!-- END_8b38224b6ff1dfcb9233ae970fc321eb -->

<!-- START_726508780075e80293aa4eb2e46871c2 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/contact/registers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/contact/registers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/contact/registers',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/contact/registers`


<!-- END_726508780075e80293aa4eb2e46871c2 -->

<!-- START_1f97b9500bc834fd675fbb455d78d0b3 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/contact/registers/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/contact/registers/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/contact/registers/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/contact/registers/create`


<!-- END_1f97b9500bc834fd675fbb455d78d0b3 -->

<!-- START_356533dc3c5f931aa0c6a18272e2f537 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/contact/registers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/contact/registers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/contact/registers',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/contact/registers`


<!-- END_356533dc3c5f931aa0c6a18272e2f537 -->

<!-- START_4654f069d0d47f901541b098840a948d -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/contact/registers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/contact/registers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/contact/registers/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/contact/registers/{register}`


<!-- END_4654f069d0d47f901541b098840a948d -->

<!-- START_4d47558a507e5093640f989a4e2d7747 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/contact/registers/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/contact/registers/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/contact/registers/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/contact/registers/{register}/edit`


<!-- END_4d47558a507e5093640f989a4e2d7747 -->

<!-- START_43179a8f6b4574139a5b4efc02b9723d -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/contact/registers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/contact/registers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/contact/registers/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/contact/registers/{register}`

`PATCH admin/contact/registers/{register}`


<!-- END_43179a8f6b4574139a5b4efc02b9723d -->

<!-- START_540c5db6c8f441f4844247363b639f5c -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/contact/registers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/contact/registers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/contact/registers/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/contact/registers/{register}`


<!-- END_540c5db6c8f441f4844247363b639f5c -->

<!-- START_66e08d3cc8222573018fed49e121e96d -->
## Show the application&#039;s login form.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/login',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET login`


<!-- END_66e08d3cc8222573018fed49e121e96d -->

<!-- START_ba35aa39474cb98cfb31829e70eb8b74 -->
## Handle a login request to the application.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/login',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST login`


<!-- END_ba35aa39474cb98cfb31829e70eb8b74 -->

<!-- START_e65925f23b9bc6b93d9356895f29f80c -->
## Log the user out of the application.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/logout',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST logout`


<!-- END_e65925f23b9bc6b93d9356895f29f80c -->

<!-- START_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->
## Show the application registration form.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/register',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET register`


<!-- END_ff38dfb1bd1bb7e1aa24b4e1792a9768 -->

<!-- START_d7aad7b5ac127700500280d511a3db01 -->
## Handle a registration request for the application.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/register',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST register`


<!-- END_d7aad7b5ac127700500280d511a3db01 -->

<!-- START_d72797bae6d0b1f3a341ebb1f8900441 -->
## Display the form to request a password reset link.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/password/reset',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET password/reset`


<!-- END_d72797bae6d0b1f3a341ebb1f8900441 -->

<!-- START_feb40f06a93c80d742181b6ffb6b734e -->
## Send a reset link to the given user.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/password/email" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/password/email"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/password/email',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST password/email`


<!-- END_feb40f06a93c80d742181b6ffb6b734e -->

<!-- START_e1605a6e5ceee9d1aeb7729216635fd7 -->
## Display the password reset view for the given token.

If no token is present, display the link request form.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/password/reset/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/password/reset/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/password/reset/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET password/reset/{token}`


<!-- END_e1605a6e5ceee9d1aeb7729216635fd7 -->

<!-- START_cafb407b7a846b31491f97719bb15aef -->
## Reset the given user&#039;s password.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/password/reset',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST password/reset`


<!-- END_cafb407b7a846b31491f97719bb15aef -->

<!-- START_b77aedc454e9471a35dcb175278ec997 -->
## Display the password confirmation view.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/password/confirm" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/password/confirm"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/password/confirm',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET password/confirm`


<!-- END_b77aedc454e9471a35dcb175278ec997 -->

<!-- START_54462d3613f2262e741142161c0e6fea -->
## Confirm the given user&#039;s password.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/password/confirm" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/password/confirm"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/password/confirm',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST password/confirm`


<!-- END_54462d3613f2262e741142161c0e6fea -->

<!-- START_7614490a3eef5fbcba402080d0369e6a -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/users',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/users`


<!-- END_7614490a3eef5fbcba402080d0369e6a -->

<!-- START_5480f74e868e50a30ac924242a423503 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/users/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/users/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/users/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/users/create`


<!-- END_5480f74e868e50a30ac924242a423503 -->

<!-- START_84cdb3581c8df106c62233f1ebb35d8b -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/users" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/users"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/users',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/users`


<!-- END_84cdb3581c8df106c62233f1ebb35d8b -->

<!-- START_efce1b78e6391078c4024f200af60be8 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/users/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/users/{user}`


<!-- END_efce1b78e6391078c4024f200af60be8 -->

<!-- START_f8b3cec767336a1c2280a2a3173678d9 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/users/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/users/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/users/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/users/{user}/edit`


<!-- END_f8b3cec767336a1c2280a2a3173678d9 -->

<!-- START_d7f417f614d8614811f624203f4e63cd -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/users/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/users/{user}`

`PATCH admin/users/{user}`


<!-- END_d7f417f614d8614811f624203f4e63cd -->

<!-- START_d5165e9382f90b24649e6ea2a27ea85d -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/users/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/users/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/users/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/users/{user}`


<!-- END_d5165e9382f90b24649e6ea2a27ea85d -->

<!-- START_448804a3f334c412b1556540cc07b159 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/brands" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/brands"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/brands',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/brands`


<!-- END_448804a3f334c412b1556540cc07b159 -->

<!-- START_4fe33a5f3bfdfd381672da6722beac18 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/brands/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/brands/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/brands/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/brands/create`


<!-- END_4fe33a5f3bfdfd381672da6722beac18 -->

<!-- START_0a2ae4c5680f2ba5054e9f6161c31921 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/brands" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/brands"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/brands',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/brands`


<!-- END_0a2ae4c5680f2ba5054e9f6161c31921 -->

<!-- START_1eddb0f05fba2fa26e96f71dedbe07d1 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/brands/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/brands/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/brands/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/brands/{brand}`


<!-- END_1eddb0f05fba2fa26e96f71dedbe07d1 -->

<!-- START_9b574f3e119dd9b19443ec7b5ac08a07 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/brands/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/brands/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/brands/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/brands/{brand}/edit`


<!-- END_9b574f3e119dd9b19443ec7b5ac08a07 -->

<!-- START_e321b0b1ffd23c3242e3ce738dd8f35a -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/brands/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/brands/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/brands/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/brands/{brand}`

`PATCH admin/brands/{brand}`


<!-- END_e321b0b1ffd23c3242e3ce738dd8f35a -->

<!-- START_e1453b0634f588b64f483b666755df9c -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/brands/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/brands/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/brands/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/brands/{brand}`


<!-- END_e1453b0634f588b64f483b666755df9c -->

<!-- START_97d8dbec7e03446818858582ab4f9c7b -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/offers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/offers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/offers',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/offers`


<!-- END_97d8dbec7e03446818858582ab4f9c7b -->

<!-- START_5f0f0a0725fbe22fb5bd41102c2ca9ed -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/offers/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/offers/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/offers/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/offers/create`


<!-- END_5f0f0a0725fbe22fb5bd41102c2ca9ed -->

<!-- START_28b17730b216bd07ff38352278d91b5a -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/offers" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/offers"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/offers',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/offers`


<!-- END_28b17730b216bd07ff38352278d91b5a -->

<!-- START_b4f6dbce7dbd186caf0c15e37ff84a7b -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/offers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/offers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/offers/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/offers/{offer}`


<!-- END_b4f6dbce7dbd186caf0c15e37ff84a7b -->

<!-- START_8038874dc663cd1f5d2a88de99ec50f8 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/offers/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/offers/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/offers/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/offers/{offer}/edit`


<!-- END_8038874dc663cd1f5d2a88de99ec50f8 -->

<!-- START_ab1abda0d3fc4be2ce09c51e3b54e1d8 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/offers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/offers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/offers/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/offers/{offer}`

`PATCH admin/offers/{offer}`


<!-- END_ab1abda0d3fc4be2ce09c51e3b54e1d8 -->

<!-- START_a86eb84c424611721b32af50554d0c7f -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/offers/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/offers/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/offers/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/offers/{offer}`


<!-- END_a86eb84c424611721b32af50554d0c7f -->

<!-- START_59e089effe621e2d2819ec8e5f47a789 -->
## admin/analytics
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/analytics" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/analytics"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/analytics',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/analytics`


<!-- END_59e089effe621e2d2819ec8e5f47a789 -->

<!-- START_ffbe360d12e64f035db64ab56d7d048c -->
## admin/analytics/{compare}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/analytics/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/analytics/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/analytics/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/analytics/{compare}`


<!-- END_ffbe360d12e64f035db64ab56d7d048c -->

<!-- START_3454863585da885781d52be92022f9c3 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/products" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/products"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/products',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/products`


<!-- END_3454863585da885781d52be92022f9c3 -->

<!-- START_ed713b61191bdebf2b0db1797a53ccd7 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/products/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/products/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/products/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/products/create`


<!-- END_ed713b61191bdebf2b0db1797a53ccd7 -->

<!-- START_075ee910ac89ee753cb942257accd01a -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/products" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/products"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/products',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/products`


<!-- END_075ee910ac89ee753cb942257accd01a -->

<!-- START_6a8c3144dc28b1c9ab73fb0d68568989 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/products/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/products/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/products/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/products/{product}`


<!-- END_6a8c3144dc28b1c9ab73fb0d68568989 -->

<!-- START_00f5a0dd610bc119d9ff3c226319fa2a -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/products/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/products/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/products/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/products/{product}/edit`


<!-- END_00f5a0dd610bc119d9ff3c226319fa2a -->

<!-- START_6380e3862cabbd94ac6032029b0382ea -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/products/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/products/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/products/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/products/{product}`

`PATCH admin/products/{product}`


<!-- END_6380e3862cabbd94ac6032029b0382ea -->

<!-- START_6016a2a97a0b96706495e53140d8e2a7 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/products/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/products/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/products/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/products/{product}`


<!-- END_6016a2a97a0b96706495e53140d8e2a7 -->

<!-- START_c69f045cbf136324af43095492e76ff2 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/owner" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/owner"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/owner',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/store/owner`


<!-- END_c69f045cbf136324af43095492e76ff2 -->

<!-- START_0a9f1a5ac90887ded320153d20af339b -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/owner/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/owner/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/owner/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/store/owner/create`


<!-- END_0a9f1a5ac90887ded320153d20af339b -->

<!-- START_6cadbdd05355e8d37ffed2015575c25f -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/store/owner" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/owner"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/store/owner',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/store/owner`


<!-- END_6cadbdd05355e8d37ffed2015575c25f -->

<!-- START_a6a55e09352e3afbb7828bce148dbd3d -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/owner/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/owner/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/owner/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/store/owner/{owner}`


<!-- END_a6a55e09352e3afbb7828bce148dbd3d -->

<!-- START_25436063e051165a0c310e7216d924de -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/owner/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/owner/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/owner/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/store/owner/{owner}/edit`


<!-- END_25436063e051165a0c310e7216d924de -->

<!-- START_564f22a64bf174d0c389766e597397c2 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/store/owner/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/owner/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/store/owner/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/store/owner/{owner}`

`PATCH admin/store/owner/{owner}`


<!-- END_564f22a64bf174d0c389766e597397c2 -->

<!-- START_64559a3f1238b35af914a3ec0677b244 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/store/owner/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/owner/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/store/owner/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/store/owner/{owner}`


<!-- END_64559a3f1238b35af914a3ec0677b244 -->

<!-- START_c3cb914af4b075f1e78f9f24012000d7 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/user/password/change" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/user/password/change"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/user/password/change',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/user/password/change`


<!-- END_c3cb914af4b075f1e78f9f24012000d7 -->

<!-- START_3256d8c64d376b3a78918aae8e66869d -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/user/password/change/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/user/password/change/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/user/password/change/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/user/password/change/create`


<!-- END_3256d8c64d376b3a78918aae8e66869d -->

<!-- START_af93db6fc9f794a53659b6c0633ac210 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/user/password/change" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/user/password/change"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/user/password/change',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/user/password/change`


<!-- END_af93db6fc9f794a53659b6c0633ac210 -->

<!-- START_451b4611760d271d042ab43fa84b0bd0 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/user/password/change/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/user/password/change/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/user/password/change/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/user/password/change/{change}`


<!-- END_451b4611760d271d042ab43fa84b0bd0 -->

<!-- START_4c3155e34d5b914e807d9ead5aed1064 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/user/password/change/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/user/password/change/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/user/password/change/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/user/password/change/{change}/edit`


<!-- END_4c3155e34d5b914e807d9ead5aed1064 -->

<!-- START_c80222f6c956e11806ac2c7821354be6 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/user/password/change/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/user/password/change/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/user/password/change/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/user/password/change/{change}`

`PATCH admin/user/password/change/{change}`


<!-- END_c80222f6c956e11806ac2c7821354be6 -->

<!-- START_077df4317eba361b357279aad24ae8ad -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/user/password/change/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/user/password/change/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/user/password/change/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/user/password/change/{change}`


<!-- END_077df4317eba361b357279aad24ae8ad -->

<!-- START_2b573e6e1d43c73d7cca65562a4e5b27 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/profile" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/profile"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/profile',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/profile`


<!-- END_2b573e6e1d43c73d7cca65562a4e5b27 -->

<!-- START_ed4418c8756e3908524436ba76a11bcf -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/profile/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/profile/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/profile/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/profile/create`


<!-- END_ed4418c8756e3908524436ba76a11bcf -->

<!-- START_a7774baac3bba086a9ae63a8f01857cf -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/profile" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/profile"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/profile',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/profile`


<!-- END_a7774baac3bba086a9ae63a8f01857cf -->

<!-- START_61cd7153d57029163ddd4a475cb8e7d1 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/profile/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/profile/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/profile/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/profile/{profile}`


<!-- END_61cd7153d57029163ddd4a475cb8e7d1 -->

<!-- START_aa3ba3638adcba2ff6c04e64da295c2c -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/profile/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/profile/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/profile/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/profile/{profile}/edit`


<!-- END_aa3ba3638adcba2ff6c04e64da295c2c -->

<!-- START_ce933f6bb67a65a596b2f455f9a46685 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/profile/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/profile/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/profile/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/profile/{profile}`

`PATCH admin/profile/{profile}`


<!-- END_ce933f6bb67a65a596b2f455f9a46685 -->

<!-- START_26e0bb8a3c00800441babc4d024ba2ab -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/profile/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/profile/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/profile/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/profile/{profile}`


<!-- END_26e0bb8a3c00800441babc4d024ba2ab -->

<!-- START_1ce9dffee438c9a45c3bd99b373e6817 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/stores" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/stores',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/stores`


<!-- END_1ce9dffee438c9a45c3bd99b373e6817 -->

<!-- START_eb11a37c95ee1874e992e280a46902b8 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/stores/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/stores/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/stores/create`


<!-- END_eb11a37c95ee1874e992e280a46902b8 -->

<!-- START_fd06b9bdddcacf3f11ad49e734181511 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/stores" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/stores',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/stores`


<!-- END_fd06b9bdddcacf3f11ad49e734181511 -->

<!-- START_f456d57e1a90d71222fd9033c1235239 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/stores/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/stores/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/stores/{store}`


<!-- END_f456d57e1a90d71222fd9033c1235239 -->

<!-- START_1d0faf5fd5ec3961b0fd63490a3eff0e -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/stores/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/stores/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/stores/{store}/edit`


<!-- END_1d0faf5fd5ec3961b0fd63490a3eff0e -->

<!-- START_4844667976e2f296e3ab94c08069d183 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/stores/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/stores/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/stores/{store}`

`PATCH admin/stores/{store}`


<!-- END_4844667976e2f296e3ab94c08069d183 -->

<!-- START_2dcaefb2f98b57a8f1aa27ec3887938a -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/stores/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/stores/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/stores/{store}`


<!-- END_2dcaefb2f98b57a8f1aa27ec3887938a -->

<!-- START_9ad08f5d810e5c0f73cfd7c7179bcb08 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/categories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/categories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/categories',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/categories`


<!-- END_9ad08f5d810e5c0f73cfd7c7179bcb08 -->

<!-- START_ce2c6d94fb61a4bb262563b97e5f7aa3 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/categories/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/categories/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/categories/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/categories/create`


<!-- END_ce2c6d94fb61a4bb262563b97e5f7aa3 -->

<!-- START_1c760aaf6fa8dfeb072fd2bcda7b6502 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/categories" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/categories"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/categories',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/categories`


<!-- END_1c760aaf6fa8dfeb072fd2bcda7b6502 -->

<!-- START_b8d01d523190686bd9a80be751978651 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/categories/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/categories/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/categories/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/categories/{category}`


<!-- END_b8d01d523190686bd9a80be751978651 -->

<!-- START_ebad456b854a248f3c1181386c63d38c -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/categories/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/categories/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/categories/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/categories/{category}/edit`


<!-- END_ebad456b854a248f3c1181386c63d38c -->

<!-- START_bf1e99f3a2fb6790a5899b4a7b6172e3 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/categories/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/categories/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/categories/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/categories/{category}`

`PATCH admin/categories/{category}`


<!-- END_bf1e99f3a2fb6790a5899b4a7b6172e3 -->

<!-- START_94773401487e54a4eef5ba3fffddfdb7 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/categories/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/categories/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/categories/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/categories/{category}`


<!-- END_94773401487e54a4eef5ba3fffddfdb7 -->

<!-- START_30cba1924428c8197100a774ba4b5075 -->
## admin/store/cart/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/cart/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/cart/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/cart/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/cart/{id}`


<!-- END_30cba1924428c8197100a774ba4b5075 -->

<!-- START_741c7917e7fe7ecda5938f695bc3057b -->
## admin/store/cart/add/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/cart/add/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/cart/add/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/cart/add/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/cart/add/{id}`


<!-- END_741c7917e7fe7ecda5938f695bc3057b -->

<!-- START_ee7ff9d080dc73c8ca9b880376fd2e77 -->
## admin/store/cart/store
> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/store/cart/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/cart/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/store/cart/store',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/store/cart/store`


<!-- END_ee7ff9d080dc73c8ca9b880376fd2e77 -->

<!-- START_a7726988f6589ebc3c932dc9603a5ca9 -->
## admin/store/cart/edit/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/cart/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/cart/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/cart/edit/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/cart/edit/{id}`


<!-- END_a7726988f6589ebc3c932dc9603a5ca9 -->

<!-- START_8f200faad46bf494532cb750a35ff931 -->
## admin/store/cart/update/{id}
> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/store/cart/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/cart/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/store/cart/update/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/store/cart/update/{id}`


<!-- END_8f200faad46bf494532cb750a35ff931 -->

<!-- START_df0e5609da3f3d8b0fe3145ba9874bf7 -->
## admin/store/cart/update/place/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/cart/update/place/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/cart/update/place/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/cart/update/place/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (302):

```json
null
```

### HTTP Request
`GET admin/store/cart/update/place/{id}`


<!-- END_df0e5609da3f3d8b0fe3145ba9874bf7 -->

<!-- START_0eb2998809b0b21d39ff98edcd251313 -->
## admin/store/cart/delete/{id}
> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/store/cart/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/cart/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/store/cart/delete/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/store/cart/delete/{id}`


<!-- END_0eb2998809b0b21d39ff98edcd251313 -->

<!-- START_851118788cd304d660bbacf9ba681c2a -->
## admin/store/team/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/team/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/team/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/team/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/team/{id}`


<!-- END_851118788cd304d660bbacf9ba681c2a -->

<!-- START_8cf0ae9e7d9c46f17712d02404b8e433 -->
## admin/store/add/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/add/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/add/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/add/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/add/{id}`


<!-- END_8cf0ae9e7d9c46f17712d02404b8e433 -->

<!-- START_719e0760e869324d40d80712dc22aae0 -->
## admin/store/store
> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/store/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/store/store',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/store/store`


<!-- END_719e0760e869324d40d80712dc22aae0 -->

<!-- START_6ee5b9b7e9efdb372f99b8e65cf081fd -->
## admin/store/edit/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/edit/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/edit/{id}`


<!-- END_6ee5b9b7e9efdb372f99b8e65cf081fd -->

<!-- START_cbf6cd20547f2460af5646a9d6f7dcc0 -->
## admin/store/update/{id}
> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/store/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/store/update/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/store/update/{id}`


<!-- END_cbf6cd20547f2460af5646a9d6f7dcc0 -->

<!-- START_7d4c7e8a870d623c8a408b04fb233714 -->
## admin/store/delete/{id}
> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/store/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/store/delete/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/store/delete/{id}`


<!-- END_7d4c7e8a870d623c8a408b04fb233714 -->

<!-- START_fe18500026ca47bbc66cb635d8f5a968 -->
## admin/store/product/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/product/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/product/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/product/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/product/{id}`


<!-- END_fe18500026ca47bbc66cb635d8f5a968 -->

<!-- START_d0450e66d803b6447cc7abc2405c7d69 -->
## admin/store/product/add/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/product/add/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/product/add/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/product/add/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/product/add/{id}`


<!-- END_d0450e66d803b6447cc7abc2405c7d69 -->

<!-- START_4aa3119f8ea26a0c6d1fca49091de266 -->
## admin/store/product/store
> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/store/product/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/product/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/store/product/store',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/store/product/store`


<!-- END_4aa3119f8ea26a0c6d1fca49091de266 -->

<!-- START_575d70135ad2319c08f8f379900d92c8 -->
## admin/store/product/edit/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/store/product/edit/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/product/edit/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/store/product/edit/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/store/product/edit/{id}`


<!-- END_575d70135ad2319c08f8f379900d92c8 -->

<!-- START_7aca878491dd499fc03b445557f8933f -->
## admin/store/product/update/{id}
> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/store/product/update/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/product/update/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/store/product/update/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/store/product/update/{id}`


<!-- END_7aca878491dd499fc03b445557f8933f -->

<!-- START_a988a92d04377dc6a3696a7e58d60f71 -->
## admin/store/product/delete/{id}
> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/store/product/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/store/product/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/store/product/delete/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/store/product/delete/{id}`


<!-- END_a988a92d04377dc6a3696a7e58d60f71 -->

<!-- START_ef74fc88219ad75b0acc28af7b662090 -->
## admin/orders
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/orders" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/orders',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/orders`


<!-- END_ef74fc88219ad75b0acc28af7b662090 -->

<!-- START_5477d13a483c4e02bccb479c0bf730f4 -->
## admin/orders/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/orders/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/orders/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/orders/{id}`


<!-- END_5477d13a483c4e02bccb479c0bf730f4 -->

<!-- START_cbc20bcc71ba014dc8db7d290ef9d9c6 -->
## admin/orders/view/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/orders/view/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/view/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/orders/view/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/orders/view/{id}`


<!-- END_cbc20bcc71ba014dc8db7d290ef9d9c6 -->

<!-- START_0a222853646ad751f8eafea308885adf -->
## admin/orders/shipping/view/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/orders/shipping/view/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/shipping/view/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/orders/shipping/view/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/orders/shipping/view/{id}`


<!-- END_0a222853646ad751f8eafea308885adf -->

<!-- START_e33f900a66fa09bc745a1d961bc8682c -->
## admin/orders/create/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/orders/create/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/create/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/orders/create/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/orders/create/{id}`


<!-- END_e33f900a66fa09bc745a1d961bc8682c -->

<!-- START_af8569519987eec14a6e168c19ad7bc5 -->
## admin/orders/billing/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/orders/billing/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/billing/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/orders/billing/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/orders/billing/{id}`


<!-- END_af8569519987eec14a6e168c19ad7bc5 -->

<!-- START_2e92ad4881dd98263dde77e45c3212ba -->
## admin/orders/updatebilling/{id}
> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/orders/updatebilling/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/updatebilling/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/orders/updatebilling/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/orders/updatebilling/{id}`


<!-- END_2e92ad4881dd98263dde77e45c3212ba -->

<!-- START_e7a2f5fe7a5fc6f77808ea2d1644e9c6 -->
## admin/orders/store
> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/orders/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/orders/store',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/orders/store`


<!-- END_e7a2f5fe7a5fc6f77808ea2d1644e9c6 -->

<!-- START_ae3cf36c7b98817881e78281fd80a611 -->
## admin/orders/delete/{id}
> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/orders/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/orders/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/orders/delete/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/orders/delete/{id}`


<!-- END_ae3cf36c7b98817881e78281fd80a611 -->

<!-- START_165efbcd75ee5ed0d102292f7ca92148 -->
## admin/purchase/orders/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/purchase/orders/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/purchase/orders/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/purchase/orders/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/purchase/orders/{id}`


<!-- END_165efbcd75ee5ed0d102292f7ca92148 -->

<!-- START_72efccf2eefdfefdedde44a5e226b70f -->
## admin/purchase/order/products/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/purchase/order/products/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/purchase/order/products/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/purchase/order/products/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/purchase/order/products/{id}`


<!-- END_72efccf2eefdfefdedde44a5e226b70f -->

<!-- START_c1468d73cdbe21eb25b6b731b9e1408e -->
## admin/purchase/cart
> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/purchase/cart" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/purchase/cart"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/purchase/cart',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/purchase/cart`


<!-- END_c1468d73cdbe21eb25b6b731b9e1408e -->

<!-- START_da52a644a0fcca69b8323aa6e68ecbfa -->
## admin/purchase/carts/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/purchase/carts/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/purchase/carts/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/purchase/carts/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/purchase/carts/{id}`


<!-- END_da52a644a0fcca69b8323aa6e68ecbfa -->

<!-- START_0e444a60fc6e8bbdfce3a746c46d1a86 -->
## admin/purchase/delete/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/purchase/delete/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/purchase/delete/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/purchase/delete/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/purchase/delete/{id}`


<!-- END_0e444a60fc6e8bbdfce3a746c46d1a86 -->

<!-- START_368d5e835c28db9e1c09b36e6da571c6 -->
## admin/purchase/order/store
> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/purchase/order/store" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/purchase/order/store"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/purchase/order/store',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/purchase/order/store`


<!-- END_368d5e835c28db9e1c09b36e6da571c6 -->

<!-- START_ae2d2d3e2e3aff512c8f602392e5ba6b -->
## admin/order/pdf/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/order/pdf/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/order/pdf/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/order/pdf/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
null
```

### HTTP Request
`GET admin/order/pdf/{id}`


<!-- END_ae2d2d3e2e3aff512c8f602392e5ba6b -->

<!-- START_c2b64d642e850b49a38c894e4b26fac1 -->
## admin/stores/{compare}/{id}
> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/stores/1/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/stores/1/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/stores/1/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/stores/{compare}/{id}`


<!-- END_c2b64d642e850b49a38c894e4b26fac1 -->

<!-- START_e1acc7f9abb69454807d49dd756ccb9a -->
## Show the application dashboard.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/home" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/home"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/home',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST home`


<!-- END_e1acc7f9abb69454807d49dd756ccb9a -->

<!-- START_654cf1f134bf09cad8122a9b83cbd485 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/supplier" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/supplier"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/supplier',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/supplier`


<!-- END_654cf1f134bf09cad8122a9b83cbd485 -->

<!-- START_0d2387e54d1d28b20dab34feffc75fa2 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/supplier/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/supplier/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/supplier/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/supplier/create`


<!-- END_0d2387e54d1d28b20dab34feffc75fa2 -->

<!-- START_247b95ecfb5fa340a6d164db2cbd8d2b -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/supplier" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/supplier"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/supplier',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/supplier`


<!-- END_247b95ecfb5fa340a6d164db2cbd8d2b -->

<!-- START_922efedea59c31348c21c1b8927f334e -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/supplier/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/supplier/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/supplier/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/supplier/{supplier}`


<!-- END_922efedea59c31348c21c1b8927f334e -->

<!-- START_bf306abd9d85153d478c353c77789cf2 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/supplier/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/supplier/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/supplier/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/supplier/{supplier}/edit`


<!-- END_bf306abd9d85153d478c353c77789cf2 -->

<!-- START_072954d26b3bb87e11c69b4e9527a927 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/supplier/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/supplier/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/supplier/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/supplier/{supplier}`

`PATCH admin/supplier/{supplier}`


<!-- END_072954d26b3bb87e11c69b4e9527a927 -->

<!-- START_14c13ed04d7fbc816004967b9b687abb -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/supplier/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/supplier/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/supplier/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/supplier/{supplier}`


<!-- END_14c13ed04d7fbc816004967b9b687abb -->

<!-- START_0045614f2ac9458326ebd6a9cbb4ac8d -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/employees" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/employees"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/employees',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/employees`


<!-- END_0045614f2ac9458326ebd6a9cbb4ac8d -->

<!-- START_4f7786116449f3385b956430be310c33 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/employees/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/employees/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/employees/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/employees/create`


<!-- END_4f7786116449f3385b956430be310c33 -->

<!-- START_4b55114ebbf85f1bdce716d4d8dee740 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/employees" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/employees"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/employees',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/employees`


<!-- END_4b55114ebbf85f1bdce716d4d8dee740 -->

<!-- START_5d552cddf1d8a8ebd0a9faf482811db4 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/employees/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/employees/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/employees/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/employees/{employee}`


<!-- END_5d552cddf1d8a8ebd0a9faf482811db4 -->

<!-- START_575c0d4cf86eb3f55f0e09306188cf40 -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/employees/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/employees/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/employees/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/employees/{employee}/edit`


<!-- END_575c0d4cf86eb3f55f0e09306188cf40 -->

<!-- START_a0f7ded4c335e760b839753dd09868ed -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/employees/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/employees/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/employees/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/employees/{employee}`

`PATCH admin/employees/{employee}`


<!-- END_a0f7ded4c335e760b839753dd09868ed -->

<!-- START_882f9a965b67fe8d0d33c2da1a727ad1 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/employees/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/employees/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/employees/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/employees/{employee}`


<!-- END_882f9a965b67fe8d0d33c2da1a727ad1 -->

<!-- START_bbe9ff8e8a1ac4bffd723647c7b844e9 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/clients" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/clients"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/clients',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (500):

```json
{
    "message": "Server Error"
}
```

### HTTP Request
`GET admin/clients`


<!-- END_bbe9ff8e8a1ac4bffd723647c7b844e9 -->

<!-- START_602b35ab554f156ef8006c88389ccd12 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/clients/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/clients/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/clients/create',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/clients/create`


<!-- END_602b35ab554f156ef8006c88389ccd12 -->

<!-- START_c25cf0448a98b66e3c9afcdcd36a9baa -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "https://supplyport.in/admin/clients" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/clients"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'https://supplyport.in/admin/clients',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`POST admin/clients`


<!-- END_c25cf0448a98b66e3c9afcdcd36a9baa -->

<!-- START_58553384f18f796dc6f14b00dd238bc2 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/clients/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/clients/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/clients/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/clients/{client}`


<!-- END_58553384f18f796dc6f14b00dd238bc2 -->

<!-- START_e86937c70a88430ec1a2ea8ca307292a -->
## Show the form for editing the specified resource.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/admin/clients/1/edit" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/clients/1/edit"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/admin/clients/1/edit',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`GET admin/clients/{client}/edit`


<!-- END_e86937c70a88430ec1a2ea8ca307292a -->

<!-- START_bcf2b31adb2e2cf243b144fefd21be78 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "https://supplyport.in/admin/clients/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/clients/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'https://supplyport.in/admin/clients/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`PUT admin/clients/{client}`

`PATCH admin/clients/{client}`


<!-- END_bcf2b31adb2e2cf243b144fefd21be78 -->

<!-- START_acaf3105e37befa2539828d2f8b43ac0 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "https://supplyport.in/admin/clients/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/admin/clients/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'https://supplyport.in/admin/clients/1',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```



### HTTP Request
`DELETE admin/clients/{client}`


<!-- END_acaf3105e37befa2539828d2f8b43ac0 -->

<!-- START_cb859c8e84c35d7133b6a6c8eac253f8 -->
## Show the application dashboard.

> Example request:

```bash
curl -X GET \
    -G "https://supplyport.in/home" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "https://supplyport.in/home"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'https://supplyport.in/home',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET home`


<!-- END_cb859c8e84c35d7133b6a6c8eac253f8 -->


