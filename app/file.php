<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class file extends Model {

	const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';

	protected $primaryKey = 'fileID';
    
   protected $fillable = [
      'userID',
      'referenceID',
      'module',      
      'fileName',
      'description',
      'directory',
      'uri',
      'extension',
      'keywords',
      'width',
      'height',
      'fileSize',
      'fileMime',
      'fileType',
      'bgPrimary',
      'bgSecondary',
      'useage',
      'status',
      'weight',
      'location',
   ];
}
