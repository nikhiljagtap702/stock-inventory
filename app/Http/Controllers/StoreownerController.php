<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Image;
use Helpers;
use App\file;
use Illuminate\Support\Facades\Hash;

class StoreownerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = collect([
            'userID' => 0,
            'name' => '',
            'full_name' => '',
            'gender' => 0,
        ]);
         $in_states = url(asset('/public/data/state.json'));
         $states =  json_decode(file_get_contents($in_states), true);
         $password = 1;
        
        
        return view( 'admin.user-manage', compact('user','states', 'password') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_data = array(
            'name'      => $request['name'],
            'full_name' => $request['full_name'],
            'mobile'    => $request['mobile'],
            'dob'       => $request['dob'],
            'company'   => $request['company'],
            'position'  => $request['position'],
            'email'     => $request['email'],
            'website'   => $request['website'],
            'address'   => $request['address'],
            'state'     => $request['state'],
            'city'      => $request['city'],
            'gender'    => $request['gender'],
            'zipcode'   => $request['zipcode'],
            'password' => Hash::make($request['password']),
            'role'      => $request['role'],
            'status'    => 1,
        );

       
        //  Check for featured image
        if( $request->fileToUpload ) {
            
          $user_data['fileID'] = Helpers::post_file($request->fileToUpload);
  
        }

       
            $User = User::create( $user_data );
            $msg = 'user created successfully';

        return redirect( '/admin/stores/create' )->with('success', $msg );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
