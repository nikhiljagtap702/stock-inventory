@extends('layouts/admin')

@section('title','Order page')

@section('content')
<h3>Shipping Progress</h3>
<div class="row">

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>Shipping Status </th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>waiting for pickup</td>
                <td>
                    <?php if(in_array('1',$watchd) ){
                        echo "Done";
                    }else{ ?>
                    <form method="post" action="{{ url('/admin/orders/store') }}">
                        @csrf
                        <input type="hidden" name="shipping" value="1">
                        <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                        <input type="hidden" name="sms" value="out for delivery">
                        <button type="submit" name="submit" class="btn btn-primary" value="Done" onclick="javascript:return confirm('Are you sure you want to update this Shipping?');"> Done</button>
                    </form>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td>picked up</td>
                <td>
                    <?php if(in_array('2',$watchd) ){
                        echo "Done";
                    }else{ ?>
                    <form method="post" action="{{ url('/admin/orders/store') }}">
                        @csrf
                        <input type="hidden" name="shipping" value="2">
                        <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                        <input type="hidden" name="sms" value="out for delivery">
                        <button type="submit" name="submit" class="btn btn-primary" value="Done" onclick="javascript:return confirm('Are you sure you want to update this Shipping?');"> Done</button>
                    </form>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td>out for delivery</td>
                <td>
                    <?php if(in_array('3',$watchd) ){
                        echo "Done";
                    }else{ ?>
                    <form method="post" action="{{ url('/admin/orders/store') }}">
                        @csrf
                        <input type="hidden" name="shipping" value="3">
                        <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                        <input type="hidden" name="sms" value="out for delivery">
                        <button type="submit" name="submit" class="btn btn-primary" value="Done" onclick="javascript:return confirm('Are you sure you want to update this Shipping?');"> Done</button>
                    </form>
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td>delivered</td>
                <td>
                    <?php if(in_array('4',$watchd) ){
                        echo "Done";
                    }else{ ?>
                    <form method="post" action="{{ url('/admin/orders/store') }}">
                        @csrf
                        <input type="hidden" name="shipping" value="4">
                        <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                        <input type="hidden" name="sms" value="out for delivery">
                        <button type="submit" name="submit" class="btn btn-primary" value="Done" onclick="javascript:return confirm('Are you sure you want to update this Shipping?');"> Done</button>
                    </form>
                <?php } ?>
                </td>
            </tr>
        </tbody> 
    </table>
    <!-- <div class="col-6">
        <div class="list-group" id="list-tab" role="tablist">
            <?php if(in_array('1',$watchd) ){ 

            }else{ ?>

                <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">waiting for pickup</a>  
        </div>

            <?php }if(in_array('2',$watchd) ){ 
            }else{ ?>

                <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">picked up</a>

            <?php } if(in_array('3',$watchd)){ 
            }else{ ?> 

                <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">out for delivery</a>
            <?php }if(in_array('4',$watchd)){ 
            }else{ ?>
                <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">delivered</a>
            <?php } ?>
        </div>
    </div>
    <div class="col-6">
        <div class="tab-content" id="nav-tabContent">
            <?php if(in_array('1',$watchd)){
            }else{ ?>
                <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                    <form method="post" action="{{ url('/admin/orders/store') }}">
                        @csrf
                        <input type="hidden" name="shipping" value="1">
                        <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                        <input type="hidden" name="sms" value="waiting for pickup">
                        <input type="submit" name="submit" class="btn btn-primary" value="Done">
                    </form>
                </div>
            <?php } ?>
            <?php if(in_array('2',$watchd)){ }else{ ?>
                <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                    <form method="post" action="{{ url('/admin/orders/store') }}">
                        @csrf
                        <input type="hidden" name="shipping" value="2">
                        <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                        <input type="hidden" name="sms" value="picked up">
                        <input type="submit" name="submit" class="btn btn-primary" value="Done">
                    </form>
                </div>
            <?php } ?>
            <?php if(in_array('3',$watchd)){ }else{ ?>
             <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                <form method="post" action="{{ url('/admin/orders/store') }}">
                    @csrf
                    <input type="hidden" name="shipping" value="3">
                    <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                    <input type="hidden" name="sms" value="out for delivery">
                    <input type="submit" name="submit" class="btn btn-primary" value="Done">
                </form>
            </div>
        <?php } ?>
        <?php if(in_array('4',$watchd)){ }else{ ?>
            <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                <form method="post" action="{{ url('/admin/orders/store') }}">
                    @csrf
                    <input type="hidden" name="shipping" value="4">
                    <input type="hidden" name="order" value="<?php echo Request::segment(4); ?>">
                    <input type="hidden" name="sms" value="delivered">
                    <input type="submit" name="submit" class="btn btn-primary" value="Done">
                </form>
            </div>
        <?php } ?>
    </div>
</div> -->
<div class="pt-3">
    <a class="btn btn-outline-secondary" href="{{ url('/admin/orders/view/'.Request::segment(4))}}" role="button" title="Go back">
        <i class="fas fa-chevron-left"></i>
        <span>Back</span>
    </a>
</div>
@endsection


