<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title') | {{ config('app.name', 'SupplyPort') }}</title>

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/admin.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('public/css/all.min.css') }}">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
    <link rel="icon" href="./favicon.ico" type="image/x-icon">
    @yield('stylesheets')
</head>
<body>
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark sticky-top flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('home') }}" title="{{ config('app.name','SupplyPort') }}">
            <!-- <div class=""> -->
            <?php print file_get_contents( asset('public/media/icon-logo-1.svg') );?>
            <!-- </div> -->
        </a>
        
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="profileDropDown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profileDropDown">
                   <a class="dropdown-item" href="{{ route('profile.show',Auth::user()->userID)}}">
                        <i class="bi bi-person"></i>
                        <span>Profile</span>
                    </a>
                    <a class="dropdown-item" href="{{ route('change.edit',Auth::user()->userID ) }}" >
                        <i class="bi bi-key"></i>
                        <span>Change Password</span>
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="bi bi-unlock"></i>
                        <span>{{ __('Logout') }}</span>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>
                </div>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="sidebar-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('home')) ? ' active' : '' }}" href="{{url('home') }}">
                                <i class="bi bi-house-door"></i>
                                <span>Home</span>
                            </a>
                        </li>
                        
                        @if( Auth::user()->role == 1 )
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/orders*')) ? ' active' : '' }}" href="{{ url('/admin/orders')}}">
                                <i class="bi bi-cart4"></i>
                                <span>Orders</span>
                            </a>
                        </li>
                        @endif
                        
                        @if( in_array( Auth::user()->role, array( 1, 2, 3 ) ) )
                            <!-- <li class="nav-item">
                                <a class="nav-link{{ (request()->is('admin/purchase/orders*')) ? ' active' : '' }}" href="{{ url('admin/purchase/orders') }}">
                                    <i class="bi bi-bag"></i>
                                    <span>Purchase Orders</span>
                                </a>
                            </li>  --> 
                            <li class="nav-item">
                                <a class="nav-link{{ (request()->is('admin/stores*')) ? ' active' : '' }}" href="{{ url('/admin/stores') }}">
                                    <i class="bi bi-shop"></i>
                                    <span>Stores</span>
                                </a>
                            </li>                         
                        @endif

                        @if( Auth::user()->role == 1 )
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/clients*')) ? ' active' : '' }}" href="{{ url('/admin/clients') }}">
                                <i class="bi bi-person"></i>
                                <span>Clients</span>
                            </a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/transactions*')) ? ' active' : '' }}" href="{{ url('/admin/transactions') }}">
                                <i class="bi bi-wallet"></i>
                                <span>Transactions</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/employees*')) ? ' active' : '' }}" href="{{ url('/admin/employees') }}">
                                <i class="bi bi-person-bounding-box"></i>
                                <span>Employees</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/supplier*')) ? ' active' : '' }}" href="{{ url('/admin/supplier') }}">
                                <i class="bi bi-truck"></i>
                                <span>Suppliers</span>
                            </a>
                        </li>  
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/analytics')) ? ' active' : '' }}" href="{{url('admin/analytics') }}">
                                <i class="bi bi-bar-chart"></i>
                                <span>Analytics</span>
                            </a>
                        </li>
                         
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/products*')) ? ' active' : '' }}" href="{{ url('/admin/products') }}">
                                <i class="bi bi-box-seam"></i>
                                <span>Products</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/brands*')) ? ' active' : '' }}" href="{{ url('/admin/brands')}}">
                                <i class="bi bi-puzzle"></i>
                                <span>Brands</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/categories*')) ? ' active' : '' }}" href="{{ url('/admin/categories')}}">
                                <i class="bi bi-tags"></i>
                                <span>Categories</span>
                            </a>
                        </li>
                        @endif
                        
                        @if( in_array( Auth::user()->role, array( 1, 2, 3 ) ) )
                         <li class="nav-item">
                            <a class="nav-link{{ (request()->is('/admin/notifications*')) ? ' active' : '' }}" href="{{ url('/admin/notifications') }}">
                                <i class="bi bi-bell"></i>
                                <span>Notifications</span>
                            </a>
                        </li>
                       
                        @endif
                        @if( Auth::user()->role == 1 )
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/users*')) ? ' active' : '' }}" href="{{ url('/admin/users') }}">
                                <i class="bi bi-people"></i>
                                <span>Users</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/offers*')) ? ' active' : '' }}" href="{{ url('/admin/offers')}}">
                                <i class="bi bi-tag"></i>
                                <span>Offers</span>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/feedbacks*')) ? ' active' : '' }}" href="{{ url('/admin/feedbacks') }}">
                                <i class="bi bi-mailbox"></i>
                                <span>Feedback</span>
                            </a>
                        </li>   
                         <li class="nav-item">
                            <a class="nav-link{{ (request()->is('admin/contact/registers*')) ? ' active' : '' }}" href="{{ url('/admin/contact/registers') }}">
                                <i class="bi bi-receipt"></i>
                                <span>Contact Registers</span>
                            </a>
                        </li>                         
                        @endif                      
                     </ul>
                </div>
            </aside>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                 @yield('content')
            </main>
        </div>
    </div>

    <script src="{{ asset('public/js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap.bundle.min.js') }}"></script>
    <!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
    <!-- <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script> -->
    <!-- <script>feather.replace();</script> -->
    <?php if( request()->is('analytics') ) :?>
         <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <?php endif;?>
    @yield('scripts')
</body>
</html>