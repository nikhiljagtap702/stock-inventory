@extends('layouts/admin')

@section('title','store page')

@section('content')
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Create Store </h6>
      </div>
     <div class="card-body">
		<form method="post" action="{{ route('stores.store') }}" enctype="multipart/form-data" >
       @csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label >Users</label>
      <select id="inputState" class="form-control" name="Users">
        <option>---Select user---</option>
         @foreach($user as $value)
        <option value="{{ $value->id }}" >{{ $value->name }}</option>
      
        @endforeach
      </select>
    </div>
    <div class="form-group col-md-6"> 
      <label>title </label>
      <input type="text" class="form-control"  name="title">
      @error('title')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
</div>
  <div class="form-row">
   <div class="form-group col-md-6">
      <label>slug </label>
      <input type="text" class="form-control"   name="slug">
       @error('slug')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
   <div class="form-group col-md-3">
      <label>tagline </label>
      <input type="text" class="form-control"  name="tagline">
       @error('tagline')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
     <div class="form-group col-md-3">
      <label>keyword </label>
      <input type="text" class="form-control"  name="keyword">
       @error('keyword')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
</div>

  <div class="form-row">
     <div class="form-group col-md-6">
      <label>domain </label>
      <input type="text" class="form-control"  name="domain">
       @error('domain')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
   <div class="form-group col-md-6">
      <label>favicon </label>
      <input type="text" class="form-control"  name="favicon">
       @error('favicon')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
</div>

 <div class="form-row">
     <div class="form-group col-md-6">
    <label>description</label>
    <textarea class="form-control"   name="description"></textarea> 
      @error('description')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
    <div class="form-group col-md-6">
      <label >Future Pictures</label>
     <input type="file" name="fileToUpload" class="form-control">
      @error('fileToUpload')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
<button type="submit" class="btn btn-primary">ADD STORE</button>
</form>
</div>
</div>
@endsection