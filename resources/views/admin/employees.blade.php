@extends('layouts/admin')

@section('title','Employees')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Employees</h6>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('employees.create') }}" class="btn btn-primary">+ADD EMPLOYEE</a>
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Mobile</th>
                            <th scope="col">Company</th>
                         <!--    <th scope="col">Stores</th> -->
                            <th scope="col">Joined</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($data as $value)
                        <tr>
                            <td scope="row">{{ $value->userID }}</td>
                            <td>
                                <a href="{{ route('employees.edit', $value->userID ) }}">
                                   <?php if($value->fileID > 0){ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/'.$value->uri )}}" height="32" width="auto" style="height: 2rem;">
                                <?php }else{ ?>
                                          <img class="img-fluid" src="{{ asset('public/uploads/user.jpg')}}" height="32" width="auto" style="height: 2rem;">
                               <?php  } ?>
                               <?php echo ucfirst($value->name); ?></a>
                            </td>
                            <td>{{ $value->email }}</td>
                            <td>+{{ $value->countryPhoneCode }}-{{ $value->mobile }}</td>
                            <td>{{ $value->company }}</td>
                            <!-- <td>{{ $value->stores ?? 0 }}</td> -->
                            <td><?php echo  date('d M Y',strtotime($value->created)); ?></td>
                                <!-- <td>

                                 <a href="{{ route('change.edit', $value->userID )}}" class="btn btn-success">Change Password</i></a>
                             </td> -->

                            <!-- <td>
                                <form action="{{ route('users.destroy', $value->userID ) }}" method="post">

                                    <a href="{{ route('users.show', $value->userID )}}" class="btn btn-info"><i class="fa fa-eye" ></i></a>

                                    <a href="{{ route('users.edit', $value->userID )}}" class="btn btn-success"><i class="fa fa-edit" ></i></a>

                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    @csrf @method('DELETE')

                                </form>
                            </td> -->
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.card-body -->
    </div><!-- /.card -->
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : []
        });
    });
</script>
@endsection