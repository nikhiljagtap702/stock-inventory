@extends('layouts/admin') 

<?php if( isset( $data->productID ) && !empty( $data->productID ) ) :?>
    @section('title','Manage Product') 
<?php else :?>
    @section('title','Add Product') 
<?php endif;?>

@section('content')
<style type="text/css">
    .img-fluid {
        max-height: 10rem;
        object-fit: contain;
        object-position: center;
    }
</style>
<div class="card shadow mb-4">
        <div class="card-header">
            <h6 class="m-0 font-weight-bold text-primary">
            <?php if( isset( $data->productID ) && !empty( $data->productID ) ) :?>
                Edit Product 
            <?php else :?>
                Add a Product
            <?php endif;?>
        </h6>
        </div>
        <div class="card-body">
            <?php if( isset( $data->productID ) && !empty( $data->productID ) ) :?>
               <form method="post" action="{{ route('products.update', $data->productID ) }}" enctype="multipart/form-data">
            <?php else :?>
                <form method="post" action="{{ route('products.update', 0 ) }}" enctype="multipart/form-data">
            <?php endif;?>
            
                @csrf 
                @method('PUT')
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Title<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" value="{{ $data->title ?? '' }}" name="title" required="required"> @error('title')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                     <div class="form-group col-md-3">
                        <label>Hsn Code</label>
                        <input type="text" class="form-control" value="{{ $data->hsn ?? '' }}" name="hsn"> @error('hsn')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <label>Brand <span class="text-danger">*</span></label>
                        <select  class="form-control" name="brand" required>
                            <option value="">---Select Brand---</option>
                            @foreach($brand as $value)
                            <?php if( isset( $data->productID ) && !empty( $data->productID ) ) :?>
                                <option value="{{ $value->brandID }}" <?php if($data->brandID == $value->brandID){ echo "selected "; } ?> >{{ $value->title }}</option>
                                <?php else :?>
                                    <option value="{{ $value->brandID }}">{{ $value->title }}</option>
                                <?php endif;?>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Category<span class="text-danger">*</span></label>
                        <select  class="form-control"  name="category"  required >
                            <option value=""> ---Select Category--- </option>
                            @foreach($categories as $category)
                            <option value="{{ $category->categoryID  }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Body</label>
                        <textarea class="form-control" name="body"> {{ $data->body ?? '' }}</textarea>
                        @error('body')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label>Description</label>
                        <textarea class="form-control" name="description">{{ $data->description ?? '' }}</textarea>
                        @error('description')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Maximum Retail Price<span class="text-danger">*</span></label>
                        <input type="number" class="form-control" value="{{ $data->max_price ?? '' }}" name="max_price" step=".01"> 
                        @error('max_price')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <label>Sale Price<span class="text-danger">*</span></label>
                        <input type="number" class="form-control" value="{{ $data->price ?? '' }}" name="min_price" step=".01">
                        @if(session('error'))
                        <strong class="text-danger">{{session('error')}}</strong>
                        @endif
                    </div>
                    
                    <div class="form-group col-md-3">
                        <label>Weight</label>
                        <input type="number" class="form-control" value="{{ $data->weight ?? '' }}" name="weight" step=".01"> 
                        @error('weight')
                        <span class="feedback" role="alert">
                        <   strong class="text-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <label>Unit</label>
                        <select id="inputState" class="form-control" name="unit">
                            <?php if( isset( $data->productID ) && !empty( $data->productID ) ) :?>
                                <option value="{{ $data->unit }}">{{ $data->unit }}</option>
                            <?php else :?>
                                <option>---Select Unit---</option>
                            <?php endif;?>
                            <option value=" liter">Liter</option>
                            <option value=" ml">Milliliters</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label>Tax </label>
                        <input type="number" class="form-control" value="{{ $data->tax ?? '' }}" name="tax" step=".01"> @error('tax')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                     <div class="form-group col-md-2">
                        <label>Cess </label>
                        <input type="number" class="form-control" value="{{ $data->cess ?? '' }}" name="cess" step=".01"> @error('tax')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-2">
                        <label>Margin </label>
                        <input type="number" class="form-control" value="{{ $data->margin ?? '' }}" name="margin" step=".01"> @error('margin')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-2">
                        <label>Discount </label>
                        <input type="number" class="form-control" value="{{ $data->discount ?? '' }}" name="discount" step=".01"> @error('discount')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-2">
                        <label>Rating </label>
                        <input type="number" class="form-control" value="{{ $data->rating ?? '' }}" name="rating" step="1"> @error('rating')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>

                     <div class="form-group col-md-2">
                        <label>Stock<span class="text-danger">*</span> </label>
                        <input type="number" class="form-control" value="{{ $data->stock ?? '' }}" name="stock" > @error('rating')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label>Min. Pieces Per Box<span class="text-danger">*</span></label>
                        <input type="number" class="form-control" value="{{ $data->pieces ?? '' }}" name="pieces"> @error('pieces')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-2">
                        <label>Box/Container Type<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" value="{{ $data->box  ?? '' }}" name="box"> @error('box')
                        <span class="feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-2">
                        <label>Stock Keeping Unit</label>
                        <input type="text" class="form-control" value="{{ $data->sku ?? '' }}" name="sku"> @error('sku')
                        <span class="feedback" role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                        </span> @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <label>Featured Image<span class="text-danger">*</span></label>
                        <div class="pt-2">
                            <input type='file' name="fileToUpload" onchange="readURL(this);" alt="your image" /> @error('fileToUpload')
                            <span class="feedback" role="alert">
                                <strong class="text-danger">{{ $message }}</strong>
                            </span> @enderror
                        </div>
                    </div>
                    <?php if( isset( $data->uri ) && !empty( $data->uri ) ) :?>
                        <div class="form-group col-md-3">
                            <img id="blah" class="img-fluid" src="{{asset('public/uploads/'.$data->uri )}}" />
                        </div>
                        <?php else : ?>
                        <div class="form-group col-md-3">
                            <img id="blah" class="img-fluid" src="{{asset('public/uploads/default-product.png')}}" />
                        </div>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-success">
                    <i class="bi bi-file-earmark-plus"></i>
                    <span>Save</span>
                </button>
                <a class="btn btn-outline-secondary" href="{{ url('/admin/products') }}" role="button" title="Go back">
                    <i class="bi bi-caret-left"></i>
                    <span>Back</span>
                </a>
            </form>
            <?php if( isset( $data->productID ) && !empty( $data->productID ) ) :?>
            <form action="{{ route('products.destroy', $data->productID )}}" method="post">
                <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are you sure you want to delete this product?');" title="Delete">
                    <i class="bi bi-trash"></i>
                    <span>Delete this product</span>
                </button>
                @csrf
                @method('DELETE')
            </form>
            <?php endif;?>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>