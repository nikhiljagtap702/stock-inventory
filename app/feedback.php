<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
	const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';

	protected $primaryKey = 'feedbackID';
	public $table = 'feedback';

	protected $fillable = [
			'userID',
			'referenceID',
			'company_name',
			'name',
			'mobile',
			'module',
			'type',
			'email',
			'subject',
			'message',
			'status'
	];
}
