<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
	const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';
    protected $primaryKey = 'cartID';
    
    protected $fillable = [
   		'userID',
   		'supplierID',
        'productID',
        'status'
   	];
}
