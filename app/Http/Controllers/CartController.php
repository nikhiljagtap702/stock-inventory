<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\store;
use App\product;
use App\store_products;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view("admin.revieworder");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = order::where('storeID', '=', $id)->get();
         return view('admin.carts',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        echo $id;
        exit;
      $data = store_products::find($id)->join('products', 'store_products.productID', '=', 'products.productID')->select('products.*', 'store_products.price as productstore')->get();
         return view('admin.addcart',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo $id;
        die();

        for ($i=0; $i < count($request->get('title')) ; $i++) { 
            if(isset($request['quantity'][$i]) && $request['quantity'][$i] > 0):
                $res[$i]['title'] =  $request['title'][$i];
                $res[$i]['image'] =  $request['image'][$i];
                $res[$i]['box'] =  $request['box'][$i];
                $res[$i]['brandName'] =  $request['brandName'][$i];
                $res[$i]['pieces'] =  $request['pieces'][$i];
                $res[$i]['Price'] =  $request['Price'][$i];
                $res[$i]['quantity'] =  $request['quantity'][$i];
                $res[$i]['total'] =  $request['total'][$i];
                // $res[$i]['orderTotal'] =  $request['orderTotal'][$i];
               
            endif;
            
        }
        $orderTotal =$request['orderTotal'];

        return view('admin.revieworder',compact('res','orderTotal'));
       // return view('permission.create')->withControllers($controllers);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
