<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model {
	
	const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';
   protected $primaryKey = 'orderID';
    
    protected $fillable = [
   		'userID',
         'storeID',
   		'referenceID',
   		'transactionID',
         'shippingID',
         'billing_address',
         'shipping_address',
   		'name',
   		'address',
   		'city',
   		'state',
   		'zipcode',
   		'country',
   		'email',
   		'phone',
   		'items',
   		'products',
   		'adjustments',
   		'currency',
   		'discount',
   		'tax',
   		'amount',
   		'taxes',
   		'type',
   		'gateway',
   		'method',
   		'log',
         'due_date',
         'shipping_status',
         'status'
   	];
}
