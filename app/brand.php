<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brand extends Model {
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
    
    protected $primaryKey = 'brandID';

    protected $fillable = [
        'brandID',
        'fileID',
        'title',
        'description',
        'body',
        'products',
        'status',
        'uri',
        'sales'
    ];
}
