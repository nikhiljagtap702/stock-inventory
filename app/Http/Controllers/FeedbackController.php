<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\feedback;
use App\User;
use Illuminate\Support\Facades\Validator;



class FeedbackController extends Controller {

    public function index(Request $request){

	    if (isset($request->type) && $request->type != 'all') {
	    	$type = $request->type;
	    	$data = feedback::latest()->where('feedback.status',1)->whereIn('module', [$request->type])->get();
	    }elseif (isset($request->type) && $request->type == 'xyz') {
	    	$type = $request->type;
	    	$data = feedback::latest()->where('feedback.status',1)->whereIn('module', ['feedback'])->get();
	    }else{
	    	$type = 'all';
	    	$data = feedback::latest()->where('status',1)->whereNotIn('module', ['register'])->get();
	    }

        return view("admin/feedbacks",compact('data','type'))->with('i');

       
     }

    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'mobile' => 'required|digits:10|numeric',
            'name'   => 'required|regex:/^[\pL\s\-]+$/u'

        ]);
        if ($validator->fails()) {

            return redirect('/#contact')
                        ->withErrors($validator)
                        ->withInput();
        }else{
    
            $data = array(
                'name'         => $request->name,
                'email'        => $request->email,
                'mobile'       => $request->mobile,
                'company_name' => $request->company_name,
                'message'      => $request->message,
                'module'       => 'query',
                'status'       =>  1,
            );

            feedback::create( $data );
            return redirect('/#contact')->with('success','Feedback has been  created successfully');
        }
    }

 
    public function show($id)
    {
        $data = feedback::where('feedbackID',$id)->first();
        return view('admin/viewfeedback')->with(compact('data'));
    }

   
    public function edit($id)
    {
        $type = $id;
        $data = feedback::latest()->where('feedback.status',1)->whereIn('module', [$id])->get();

        return view("admin/feedbacks",compact('data','type'))->with('i');
        
    }

   
    public function destroy($id){

        $feedback = feedback::find($id);
        $feedback->delete();
       return redirect("/admin/feedbacks")->with('success','Feedback Deleted successfully');
        
    }
}

