<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//	Public endpoints
Route::post('user/login', 'api\UserController@login');
Route::post('feedback/register', 'api\FeedbackController@post_register');

// Route::group(['middleware' => 'auth:api'], function() {
	//	USER	
	Route::get('user', 'api\UserController@get_user');
	Route::post('user', 'api\UserController@store_user');
	Route::post('user/logout', 'api\UserController@logout');
	// Route::post('/register','api\Register@store');

	//	PRODUCTS
	Route::get('products', 'api\ProductController@get_products');
	Route::get('product', 'api\ProductController@get_product');
	Route::get('products/filters', 'api\ProductController@filter_products');
	// Route::post('product', 'api\ProductController@post_product');
	// Route::put('product', 'api\ProductController@put_product');
	// Route::delete('product', 'api\ProductController@delete_product');

	//	ORDERS
	Route::get('orders', 'api\OrderController@get_orders');
	Route::get('order', 'api\OrderController@get_order');
	Route::post('order', 'api\OrderController@post_order');	
	// Route::put('order', 'api\OrderController@put_order');
	// Route::delete('order', 'api\OrderController@delete_order');
	// Route::get('ordersummary', 'api\OrderController@get_summary');
	Route::post('order/payment', 'api\OrderController@post_order_payment');
	Route::post('order/transaction', 'api\OrderController@post_transation');
	Route::get('order/shipping', 'api\OrderController@get_order_shipping');

	
	Route::get('order/products', 'api\OrderController@get_order_products');
	Route::get('order/repeat', 'api\OrderController@order_products');
	Route::post('file/upload', 'api\ProductController@file_upload');

	
	//	NOTIFICATIONS
	Route::get('notifications', 'api\notificationController@get_notifications');
	// Route::get('notification', 'api\notificationController@get_notification');

	//	ANALYTICS
	Route::post('store', 'api\StoreController@post_client');	
	Route::get('analytics/brands', 'api\storeController@get_analytics_brands');
	Route::get('analytics/orders', 'api\storeController@get_analytics_orders');
	Route::get('analytics/products', 'api\storeController@get_analytics_products');

	//	FEEDBACK
	Route::post('feedback', 'api\FeedbackController@post_feedback');
	

	//	DASHBOARD
	Route::get('dashboard','api\dashboardController@get_dashboad');
	Route::get('analytics','api\dashboardController@get_analytics');

	//Brand
	Route::get('brand','api\BrandController@get_brand');
	Route::get('brands','api\BrandController@get_brands');

	//offer
	Route::get('offer','api\OfferController@get_offer');
	Route::post('offer','api\OfferController@post_offer');
	Route::get('offers','api\OfferController@get_offers');	

	Route::get('faqs', 'api\FaqController@get_faqs');
// });

Route::fallback(function () {
    return response()->json([ 'messages' => array( 'message' => 'Invalid API endpoint.', 'type' => 'error' ), 'status' => 'error'], 404);
});
