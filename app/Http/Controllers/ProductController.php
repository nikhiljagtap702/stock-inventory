<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use Image;
use Helpers;
use App\file;
use App\brand;
use App\categories;
use App\Charts\UserChart;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $data = product::latest()->where('stock' ,'<=', 2)->get();
            $title = array();
            $total_val = array();
            
            foreach( $data as $item ) {
                $title[]      =  $item['title'];
                $total_val[]  =  $item['stock'];
            }
             //products data
            $productChart = new UserChart;
            $productChart->labels($title);
            $productChart->dataset('min stock trigger', 'line',$total_val)
                            ->color("rgb(255, 99, 132)")
                            ->backgroundcolor("rgb(255, 99, 132)")
                            ->fill(false)
                            ->linetension(0.1)
                            ->dashed([5]);
            $data=Product::latest()->leftjoin('files','products.fileID','=','files.fileID')->select('products.*','files.uri')->get();
            return view("admin.products",[ 'productChart' => $productChart ],compact('data'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = null;
        $brand=brand::all();
        $categories=categories::all();
        return view("admin.product-manage",compact('brand','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'sku' => ['required'],
            'category' => ['required'],
            'type' => ['required'],
            'title' => ['required'],
            'hsn' => $request['hsn'],
            'cess' => $request['cess'],
            'body' => ['required'],
            'box' => ['required'],
            'Max_price' => ['required'],
            'min_price' =>  ['required'],
            'weight' => ['required'],
            'unit' => ['required'],
            'tax' => ['required'],
            'margin' => ['required'],
            'discount' => ['required'],
            'rating' => ['required'],
            'Stock' => ['required'],
            'description' => ['required'],
            'fileToUpload' => ['required'],
        ]);

        if(floatval($request['max_price']) < floatval($request['min_price'])) {
            return redirect()->back()->with( 'error', 'The Sale price must be at least Max price.' );
        }

        $product =  product::create([
            'sku' => $request['sku'],
            'category' => $request['category'],
            'type' => $request['type'],
            'tagline' => $request['tagline'],
            'title' => $request['title'],
            'body' => $request['body'],
            'box' => $request['box'],
            'min_price' => $request['min_price'],
            'Max_price' => $request['Max_price'],
            'weight' => $request['weight'],
            'unit' => $request['unit'],
            'tax' => $request['tax'],
            'margin' => $request['margin'],
            'discount' => $request['discount'],
            'rating' => $request['rating'],
            'stock' => $request['Stock'],
            'description' => $request['description'],
        ]); 

    if($product) {
         
    $image = $request->file('fileToUpload');

     $new_name = rand() . '.' . $image->getClientOriginalExtension();

     $image->move(public_path('uploads'), $new_name);

         $form_data =array(
            'fileName' => $new_name,
             );
                 if($file=file::create($form_data)){

                    $product= product::find($product->productID);
                    $product->fileID = $file->fileID;
                    $product->save();

             return redirect("/admin/products")
            ->with('success','Product created successfully');

            }
           
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $data = product::find($id);
        // return view('admin.viewProduct',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand=brand::all();
        $categories=categories::all();
        $data = product::where('productID',$id)->leftjoin('files','products.fileID','=','files.fileID')->select('products.*','files.uri')->first();
        return view('admin.product-manage', compact('data','brand','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id )
    {
       
        if(floatval($request['max_price']) < floatval($request['min_price'])) {

            // echo "The Sale price must be at least Max price.";
            return redirect()->back()->with( 'error', 'The Sale price must be at least Max price.' );
        }

        $product_data=array(
            'brandID' => $request['brand'],
            'sku' => $request['sku'],
            'hsn' => $request['hsn'],
            'cess' => $request['cess'],
            'title' => $request['title'],
            'body' => $request['body'],
            'box' => $request['box'],
            'price' => $request['min_price'],
            'max_price' => $request['max_price'],
            'weight' => $request['weight'],
            'unit' => $request['unit'],
            'tax' => $request['tax'],
            'margin' => $request['margin'],
            'discount' => $request['discount'],
            'stock' => $request['stock'],
            'rating' => $request['rating'],
            'pieces' => $request['pieces'],
            'category' => $request['category'],
            'description' => $request['description'],
        ); 

        //  Check for featured image
        if( $request->fileToUpload ) {
            
            $product_data['fileID'] = Helpers::post_file($request->fileToUpload);
        }

        if( $id > 0 ) {
            $product = product::find( $id );
            $product->update( $product_data );
            $msg = 'Product updated successfully';
        }else {
            $product = product::create( $product_data );
            $brand = brand::find($product->brandID);
            $brand_data =array(
                'product'  => $brand['product'] + 1,
            );

            $brand->update($brand_data);
            $msg = 'Product created successfully';
        }
    

        return redirect( '/admin/products' )->with('success', $msg );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=product::find($id);
        $product->delete();
         return redirect("/admin/products")
     ->with('success','Product Deleted successfully');
    }

}
