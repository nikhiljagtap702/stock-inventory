<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\order;
use Carbon\Carbon;
use App\User;
use App\store;
use App\team;
use App\order_product;
use DB;
use App\Charts\UserChart;
use App\product;
use App\brand;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){
        $total_order        =  0;
        $total_products     =  0;
        $total_suppliers    =  0;
        $total_brands       =  0;
        $total_clients      =  0;
        $new_order          =  0;

        if (Auth::check() && Auth::User()->role == 2) {

            $total_order        = order::join('team','orders.storeID','team.storeID')->where('team.userID',Auth::User()->userID)
                                  ->join('stores','team.storeID','=','stores.storeID')->whereNotIn('orders.status', [0])->count();
            $new_order          = order::join('team','orders.storeID','team.storeID')->where('team.userID',Auth::User()->userID)
                                  ->join('stores','team.storeID','=','stores.storeID')
                                  ->whereDate('orders.created',Carbon::today())->whereNotIn('orders.status', [0])->count();
            $total_clients      =  team::where('team.userID', '=', Auth::User()->userID)->join('stores','team.storeID','=','stores.storeID')->count();
            
        }else{

            $total_order        =  order::all()->whereNotIn('status', [0])->count();
            $new_order          =  order::whereDate('created',Carbon::today())->whereNotIn('orders.status', [0])->count();
            $total_products     =  product::all()->count();
            $total_suppliers    =  User::all()->count();
            $total_brands       =  brand::all()->count();
            $total_clients      =  store::all()->count();
        }
        return view('admin.index')->with(compact('total_order','total_clients','total_suppliers','total_products','total_brands','new_order'));
   }
}



