<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\User;
use Auth;
use App\order_payment;
use App\team;
use App\session;
use App\watchdog;
use App\address;
use App\order_product;
use App\notification;
use Carbon\Carbon;

class ShippingController extends Controller
{
    public function view(Request $request){

       if ($request->id == 1) {
        if (Auth::check() && Auth::User()->role == 2) {
            $data = order::latest()->join('team','orders.storeID','team.storeID')->where('team.userID',Auth::User()->userID)->join('stores','team.storeID','=','stores.storeID')->whereNotIn('orders.status', [0])->whereNotIn('stores.status',[ 2 ])->select('stores.title','orders.*')->whereDate('orders.created',Carbon::today())->get();
        }else{
            $data = order::latest()->join('stores','orders.storeID','=','stores.storeID')->whereNotIn('orders.status', [0])->whereNotIn('stores.status',[ 2 ])->select('stores.title','orders.*')->whereDate('orders.created',Carbon::today())->get();

        }
        }elseif ($request->id == 2) {
            $data =  order::latest()->join('team','orders.storeID','team.storeID')->where('team.userID',Auth::User()->userID)->join('stores','team.storeID','=','stores.storeID')->whereNotIn('orders.status', [0])->whereNotIn('stores.status',[ 2 ])->select('stores.title','orders.*')->get();
        }else{
            $data = order::latest()->join('stores','orders.storeID', '=' , 'stores.storeID')->whereNotIn('orders.status', [0])->whereNotIn('stores.status',[ 2 ])->select('stores.title','orders.*')->get();
        }
        return view('admin.orders',compact('data'))->with('i');
    }

    public function create($id){

        $watchdog = watchdog::where('referenceID',$id)->select('status')->get();
        $watchd = [];
        foreach ($watchdog as $wd) {
            array_push($watchd,$wd->status);
        }
        return view('admin/shipping')->with(compact('watchd'));
    }

    public function store(Request $request){

        $order_data = array('shipping_status' => $request['shipping'], );
        $order= order::find($request['order']);
        $order->update($order_data);
        $store_id = $order->storeID;
        $user_id = $order->userID;
        //create Notification
        $device = array();
        $team = team::where('storeID',$store_id)->get();
        foreach ($team as $value) {
            $devices['userID'] = $value->userID;
            $devices['storeID'] = $order->storeID;
            $devices['referenceID'] = $order->orderID;
            $devices['senderID'] = 1;
            $devices['title'] = "order";
            $devices['message'] = $request['sms'];
            $devices['body'] = "body";
            $devices['module'] = "orders/shipping";
            post_notification($devices);
        }
        $ship_data = array(
            'referenceID' => $request['order'],
        	'userID' => \Auth::user()->userID,
        	'storeID' => $store_id,
        	'module' => 'shipping',
            'title' => 'order',
        	'message' => $request['sms'],
        	'status' => $request['shipping'],
        );
        watchdog::create($ship_data);
        return redirect( "/admin/orders/view/".$order->orderID)->with('success','shipping status updated  successfully');
    }

    public function billing($id){
        $order = order::find($id);
        $transaction = order_payment::latest()->where('userID','\Auth::user()->userID')->take(10)->get();
        return view('admin/orderbilling',compact('order','transaction'));
    }

    public function billingupdated(Request $request,$id){

        $data = array(
            'orderID' =>$id,
            'transactionID' => $request['transactionID'],
            'amount' => $request['amount'] ,
            'type' => $request['type'],
            'method' => $request['method'],
            'gateway' => $request['gateway'],
            'status' => 1,
        );
        order_payment::create($data);
        $order = order::find($id);
       
        watchdog::create(array(
            'referenceID' => $id,
            'userID' => $order->userID,
            'storeID' => $order->storeID,
            'module' => 'payment',
            'title' => 'payment',
            'message' => 'payment done.#'.$id,
            'status' => 1,
        ));
        $device = array();
        $devices['userID'] = $order->userID;
        $devices['storeID'] = $order->storeID;
        $devices['referenceID'] = $id;
        $devices['senderID'] = $order->userID;
        $devices['title'] = "order #".$id;
        $devices['message'] = 'payment has been  done for order #'.$id;
        $devices['body'] = 'payment has been  done for order #'.$id;
        $devices['module'] = "orders";
        post_notification($devices);
     
    
         $order_data = array(
            'transactionID' => $request['transactionID'],
            'status' => 2,
        );
        $order->update($order_data);
        return redirect( "/admin/orders/view/".$id)->with('success','Order Payment submit successfully');
    }

    public function show($id){

        // $data = order::find($id);
        // $watchdog = watchdog::where('referenceID',$id)->select('status')->get();
        // $watchd = [];
        // foreach ($watchdog as $wd) {
        //     array_push($watchd,$wd->status);
        // }
        // $order_product = order_product::where('orderID',$id)->get();
        // $ship = address::find($data->shipping_address);
        // $bill = address::find($data->billing_address);

        $data = order::where('orders.orderID',$id)->join('stores','orders.storeID','=','stores.storeID')->select('orders.*')->first();
        $watchdog = watchdog::where('referenceID',$id)->select('status')->get();
        $watchd = [];

        foreach ($watchdog as $wd) {
            array_push($watchd,$wd->status);
        }

        $order_product = order_product::where('orderID',$id)->get();
        $ship = address::find($data->shipping_address);
        $bill = address::find($data->billing_address);
        $data['taxes'] = $data['tax'] + $data['cess'];
        $total = $data['amount']*$data['taxes']/100;

        $data['com'] = ( $data['amount']*$data['taxes'] / 100 );

        $data['sub_total'] = $total+$data['amount'];
        

        return view("admin.viewOrder",compact('data','ship','bill','watchd','order_product'));
    }

    public function show_shipping($id){

        $data = order::find($id);
        $watchdog = watchdog::where('referenceID',$id)->select('status')->get();
        $watchd = [];
        foreach ($watchdog as $wd) {
            array_push($watchd,$wd->status);
        }
        $order_product = order_product::where('orderID',$id)->get();
        $ship = address::find($data->shipping_address);
        $bill = address::find($data->billing_address);
        return view("admin.viewOrder",compact('data','ship','bill','watchd','order_product'));
    }

    public function destroy($id){

        $order = order::find($id);
        $order->delete();
        order_product::where('orderID',$id)->delete();
        return redirect( "admin/orders" )->with('success','Order Placed successfully');
    }
}
