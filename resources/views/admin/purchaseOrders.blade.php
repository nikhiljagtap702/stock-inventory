@extends('layouts/admin')

@section('title', 'Purchase Orders')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
            	<div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Purchase Orders</h6>
                </div>
                <div class="col-md-6 text-right">
                     <a href="{{ url('/admin/purchase/order/products/'.$supplier .'/') }}" class="btn btn-primary"><i class="bi bi-cart-plus"></i> Orders</a>
                </div>
            </div>
        </div>
        
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                    <tr>
                        <th>OrderID</th>
                        <th>supplier Name</th>
                        <th>Items</th>
                        <th>Total</th>
                        <th>Placed </th>
                        <th>Updated </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $val)
                        <tr>
                            <td class="align-middle">#{{ $val->orderID }}</td>
                            <td class="align-middle" ><a href="#">{{ $val->name }}</a></td>
                            <td class="align-middle">{{ $val->items }} </td>
                            <td class="align-middle">₹{{ $val->amount }}</td>
                            <td class="align-middle"><?php echo date('d M Y',strtotime($val->created)); ?></td>
                            <td class="align-middle"><?php echo date('d M Y',strtotime($val->updated)); ?></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : [],
          "pageLength": 25
        });
    });
</script>
@endsection