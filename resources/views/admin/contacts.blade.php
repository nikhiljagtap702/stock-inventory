@extends('layouts/admin')

@section('title','Feedbacks')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Contact Registers</h6>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <span>{{ $message }}</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="active">
                        @foreach($data as $value)
                        <td>{{++$i}}</td>
                        <td>{{ $value->name }}</td>
                        <td><a  href="{{ route('registers.show',$value->feedbackID) }}" >{{ $value->email}}</a></td>
                        <td>{{ $value->mobile }}</td>
                        <td>{{ $value->subject}}</td>
                        <td>{{ $value->message}}</td>
                        <td><?php echo  date('d M Y',strtotime($value->created)); ?></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : [],
          "pageLength": 25
        });
    });
</script>
@endsection