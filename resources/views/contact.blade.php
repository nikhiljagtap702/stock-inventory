@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Contact US</div>

                <div class="card-body">
                    Get in touch
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
