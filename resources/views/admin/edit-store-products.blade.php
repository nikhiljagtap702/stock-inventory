@extends('layouts/admin')

@section('title', 'Edit Store Products')

@section('content')
    <form method="post" action="{{ url('/admin/store/product/update/'.$storeID.'/') }}" > 
    @csrf
    @method('PUT')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row align-items-center">
                <div class="col-md-6">
                     <h6 class="m-0 font-weight-bold text-primary"> EDIT STORE PRODUCTS</h6>
                </div>
                <div class="col-md-6 text-right">
                    <a class="btn btn-outline-secondary" href="{{ url('/admin/store/product/'.$storeID) }}" role="button" title="Go back">
                        <i class="bi bi-caret-left"></i>
                        <span>Back</span>
                    </a> 
                   <button type="submit" class="btn btn-success">
                    <i class="bi bi-file-earmark-plus"></i>
                    <span>Save</span>
                </button>
                </div>
            </div>
        </div>
       <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">M.R.P</th>
                            <th scope="col">Sale Price</th>
                            <th scope="col">Store Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; ?>
                        @foreach($data as $store)
                        <tr class="active"> 
                            <td scope="row">{{ ++$i }}</td> 
                            <td>
                                <?php if($store->fileID > 0){?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/'.$store->uri )}}" height="32" width="auto" style="height: 2rem;">
                                <?php }else{ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" height="32" width="auto" style="height: 2rem;">
                                <?php  } ?>
                                {{ $store->producttitle ?? '' }}
                            </td> 
                            <td>{{ $store->max_price }}</td>
                            <td>{{ $store->sale_price ?? ''}}</td>
                            <td><input type="number"  name="price[ {{ $store->id }} ][]" value="{{ $store->price ?? '0' }}" placeholder="Enter Price" aria-label="Amount (to the nearest dollar)">
                           </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
