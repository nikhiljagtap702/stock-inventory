@extends('layouts/admin')

@section('title',$store->title)

@section('content')
<!-- main content start-->
<?php   $segment1 =  Request::segment(3);
        $segment =  date('d M Y',strtotime( $segment1));
?>
<div class="card shadow mb-4">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4">
                    <h6 class="m-0 font-weight-bold text-primary">{{ $store->title }}</h6>
                </div>
                <div class="col-md-4 text-right">
                    <select class="form-control"  id="compare">
                        <option value="{{ url( '/admin/stores/'.$today.'/'.$store->storeID ) }}"> Today</option>
                        <option value="{{ url( '/admin/stores/'.$yesterday.'/'.$store->storeID ) }}"  <?php if($segment == date('d M Y',strtotime( $yesterday))) echo ' selected="selected"'; ?>>Yesterday</option>
                        <option value="{{ url( '/admin/stores/'.$last_7_day.'/'.$store->storeID ) }}" <?php if($segment == date('d M Y',strtotime( $last_7_day))) echo ' selected="selected"'; ?>>Last 7 days</option>
                        <option value="{{ url( '/admin/stores/'.$last_30_day.'/'.$store->storeID ) }}" <?php if($segment == date('d M Y',strtotime( $last_30_day))) echo ' selected="selected"'; ?>>Last 30 days</option>
                        <option value="{{ url( '/admin/stores/'.$lastmonths ) }}" <?php if($segment == date('d M Y',strtotime( $lastmonths))) echo ' selected="selected"'; ?>>Last month</option>
                        <option value="{{ url( '/admin/stores/last_year/'.$store->storeID ) }}" <?php if($segment1 == 'last_year' ) echo ' selected="selected"'; ?>>Last year</option>
                        <option value="{{ url( '/admin/stores/secondOfQuarter/'.$store->storeID ) }}"  <?php if($segment1 == 'secondOfQuarter' ) echo ' selected="selected"'; ?> >2rd Quarter</option>
                        <option value="{{ url( '/admin/stores/firstOfQuarter/'.$store->storeID ) }}" <?php if($segment1 == 'firstOfQuarter' ) echo ' selected="selected"'; ?> >1st Quarter</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <?php if(Auth::check() && Auth::User()->role == 2){ ?>
                     <a href="{{ url('/admin/store/product/'.$store->storeID.'/') }}" class="btn btn-primary" title="View products">
                        <i class="bi bi-box-seam"></i><span class="d-none d-md-block">Products</span>
                    </a>
                    <a href="{{ url('/admin/store/team/'.$store->storeID.'/') }}" class="btn btn-success" title="View members">
                        <i class="bi bi-people"></i><span class="d-none d-md-block">Members</span>
                    </a>
                    <a href="{{ route('stores.edit', $store->storeID ) }}" class="btn btn-primary" title="Edit this Client"><i class="bi bi-pencil-square"></i><span class="d-none d-md-block">Edit</span></a>
                <?php }else{  ?>
                    <a href="{{ url('/admin/store/product/'.$store->storeID.'/') }}" class="btn btn-primary" title="View products">
                        <i class="bi bi-box-seam"></i><span class="d-none d-md-block">Products</span>
                    </a>
                    <a href="{{ url('/admin/store/team/'.$store->storeID.'/') }}" class="btn btn-success" title="View members">
                        <i class="bi bi-people"></i><span class="d-none d-md-block">Members</span>
                    </a>
                    <a href="{{ route('stores.edit', $store->storeID ) }}" class="btn btn-primary" title="Edit this Client" ><i class="bi bi-pencil-square"></i><span class="d-none d-md-block">Edit</span></a>
                    <form class="d-inline-block" action="{{ route('stores.destroy', $store->storeID )}}"  method="post">
                        <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are You Confirm Deletion');" title="Delete this Client" >
                            <i class="bi bi-trash"></i><span class="d-none d-md-block">Delete</span>
                        </button>
                        @csrf
                        @method('DELETE')
                    </form>
                <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
              {!! $orderproductChart->container() !!}
            </div>
            <div class="col-md-6">
              {!! $brandChart->container() !!}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
  
  {!! $orderproductChart->script() !!}
  {!! $brandChart->script() !!}
    <script type="text/javascript">
        $(document).ready( function () {
            $('#compare').on('change', function () {
                var url = $(this).val();
                if (url) {
                    window.location = url;
                }
                return false;
            });
        });
    </script>
@endsection