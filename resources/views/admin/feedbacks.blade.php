@extends('layouts/admin')

@section('title','Feedbacks')

@section('content')
<?php 
$baseUrl = 'admin/feedbacks';
$bannerType = 'all';
if( isset( $type ) && !empty($type)) {
   $feedbackType = $type;
}
?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Feedbacks</h6>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <span>{{ $message }}</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card-body">
         <div class="row mb-3">
                <label for="feedback_type" class="col-md-auto">Filter by module</label>
                <div class="col-md-3">
                    <select class="form-control form-control-sm" id="feedback_type">
                        <option value="{{ url( $baseUrl. '/all/' ) }}"<?php if($type == 'all') echo ' selected="selected"'; ?>>All</option>
                        <option value="{{ url( $baseUrl. '/query/' ) }}"<?php if($type == 'query') echo ' selected="selected"'; ?>>Query</option>
                        <option value="{{ url( $baseUrl. '/feedback/' ) }}"<?php if($type == 'feedback') echo ' selected="selected"'; ?>>Feedback</option>
                    </select>
                </div>
        <div class="table-responsive">
            <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Message</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="active">
                        @foreach($data as $value)
                        <td>{{++$i}}</td>
                        <td><a  href="{{ url('admin/feedback/show/'.$value->feedbackID.'/') }}" >{{ $value->email}}</a></td>
                        <td>{{ $value->message}}</td>
                        <td><?php echo  date('d M Y',strtotime($value->created)); ?></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable();

        $('#feedback_type').on('change', function () {
            var url = $(this).val();
            if (url) {
                window.location = url;
            }
          return false;
        });
    });
</script>

@endsection