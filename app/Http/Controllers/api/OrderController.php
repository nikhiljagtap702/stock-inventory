<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\store;
use App\order;
use App\User;
use App\session;
use App\order_product;
use App\order_payment;
use Notifications;
use App\watchdog;
use Validator;
use DB;
use Mail;
use App\product;
use Helpers;

/**
 * @group Orders
 *
 * APIs for managing orders
 */
class OrderController extends Controller {

    /** 
     * Get orders
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam order_status string Order status Example: unpaid
     * @bodyParam shipping_status int Order shipping status Example: 0
     * @bodyParam page int page number Example: 1
     * @bodyParam limit int limit number Example: 10
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     *
     */
    public function get_orders( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'dues' => array(),  'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'userID'        => ['required'],
            'storeID'       => ['required'],
            'order_status'  => [],
            'shipping_status' =>[],
            'page'          => [],
            'limit'         => [],

        ], [
            'userID.required'  => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID'
        ]);

        $page = 0;
        if( isset( $request->page ) && !empty( $request->page ) ) {
            $page = $request->page;
        }
        
        $limit = 10;
        if( isset( $request->limit ) && !empty( $request->limit ) ) {
            $limit = $request->limit;
        }

        if( isset( $request->order_status ) && !empty( $request->order_status ) ) {
            if ( $request->order_status === "paid" ) {
                $request->status = 2;
            }else if ( $request->order_status === "unpaid" ) {
                 $request->status = 4;
            }else if ($request->order_status === "draft" ) {
                $request->status = 0;
            }else if($request->order_status === "placed" ){
                $request->status = 1;
             }
        }

        if( isset( $request->shipping_status ) && !empty( $request->shipping_status ) ) {
            if ( $request->shipping_status === "Pickup" ) {
                $request->s_status = 1;
            }else if ( $request->shipping_status === "In Transit" ) {
                 $request->s_status = 2;
            }else if ($request->shipping_status === "Out for Delivery" ) {
                $request->s_status = 3;
            }else if($request->shipping_status === "Shipped" ){
                $request->s_status = 4;
             }
        }

        
        if( !$valid->fails() ) {
            //$page = $request->page * $limit;

            // $orders = order::where('storeID', '=', $request->storeID);

            // $orders = DB::table('orders')->where('storeID', '=', $request->storeID)->get();
            if( isset( $request->status ) && !empty( $request->status ) ) {

                 $orders = order::latest()->where('storeID', '=', $request->storeID)->where('status',$request->status)->skip($page*$limit)->take($limit)->get(); 
            }else if( isset( $request->s_status ) && !empty( $request->s_status ) ) {

                $orders = order::latest()->where('storeID',$request->storeID)->where('shipping_status',$request->s_status)->skip($page*$limit)->take($limit)->get();
            }else{

            $orders = order::latest()->where('storeID', '=', $request->storeID)->skip($page*$limit)->take($limit)->get(); 

           }

            // $pendding_order = order::selectRaw('year(created) year, monthname(created) month, count(*) data,sum(amount) data')
            //      ->groupBy('year', 'month')
            //      ->get();            

            // print_r($pendding_order);
            // exit();
            // $due  = array();
            // foreach ($pendding_order as $value) {
            //     $due['title']  = $value['month'];
            //     $due['orders'] = $value['data'];
            //     $due['amount'] = $value['amount'];
            //     // $due['date']   = $value['new_date'];
            // }

            // print_r($due);
            // exit();

           $due = array(array(  
                            'title'   => 'January',
                            'orders'  => 5,
                            'amount' => 50000,
                            'date'    => date( 'c', strtotime( '2021-01-31' ) ),
                        ),
                        array(
                            'title'   => 'February',
                            'orders'  => 2,
                            'amount'  => 25000,
                            'date'    => date( 'c', strtotime( '2021-02-28' ) ),
                        ),
                    );
             $response['data'] = $orders;
            if( isset( $response['data'] ) && !empty( $response['data'] ) ) {
                $response['status'] = 'success';
                foreach ($response['data'] as $value) {

                    if( $value['status'] == 0 ) {
                        $value['statusName'] = 'draft';
                    }else if( $value['status'] == 1 ) {
                        $value['statusName'] = 'placed';
                    }else if( $value['status'] == 2 ) {
                        $value['statusName'] = 'Success with payment';
                    }else if( $value['status'] == 3 ) {
                        $value['statusName'] = 'Failure';
                    }else if( $value['status'] == 4 ) {
                        $value['statusName'] = 'payment unpaid';
                    }else {
                        $value['statusName'] = 'Unknown';
                    } 

                    if( $value['shipping_status'] == 0 ) {
                        $value['shipping_statusName'] = '';
                    }else if( $value['shipping_status'] == 1 ) {
                        $value['shipping_statusName'] = 'Pickup';
                    }else if( $value['shipping_status'] == 2 ) {
                        $value['shipping_statusName'] = 'In Transit';
                    }else if( $value['shipping_status'] == 3 ) {
                        $value['shipping_statusName'] = 'Out for Delivery';
                    }else if( $value['shipping_status'] == 4 ) {
                        $value['shipping_statusName'] = 'Shipped';
                    }else {
                        $value['shipping_statusName'] = 'Unknown';
                    } 
                }
                
                $response['dues'] = $due;
            }else {
                $response['status'] = 'error';
            }

        }

        if( isset( $response['status'] ) && ( $response['status'] == 'error' ) ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }


    /** 
     * Get order details 
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam orderID int required Unique order ID Example: 1234
     *
     * @response {
     *      "data": {},
     *      "status": "success"
     * }
     *
     */
    public function get_order( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        $valid = Validator::make( $request->all() , [
            'userID'  => ['required'],
            'storeID' => ['required'],
            'orderID' => ['required'],
        ], [
            'userID.required'  => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID',
            'orderID.required' => 'Please enter a order ID',
        ]);

        if( !$valid->fails() ) {

         $data = order::where([ ['orderID',$request->orderID],['storeID',$request->storeID] ])->get();

            if( isset( $data[0]['orderID'] ) && !empty( $data[0]['orderID'] ) ) {  

                $order = $data[0];
                $response['status'] = 'success';

                 if( $order['status'] == 0 ) {
                    $orde['statusName'] = 'draft';
                }else if( $order['status'] == 1 ) {
                    $order['statusName'] = 'placed';
                }else if( $order['status'] == 2 ) {
                    $order['statusName'] = 'Success with payment';
                }else if( $order['status'] == 3 ) {
                    $order['statusName'] = 'Failure';
                }else if( $order['status'] == 4 ) {
                    $order['statusName'] = 'payment unpaid';
                }else {
                    $order['statusName'] = 'Unknown';
                }

                $order['created'] = date( 'c', strtotime( $order->created ) );
                $order['updated'] = date( 'c', strtotime( $order->updated ) );


                if( $order['shipping_status'] == 0 ) {
                    $orde['shipping_statusName'] = '';
                }else if( $order['shipping_status'] == 1 ) {
                    $order['shipping_statusName'] = 'Pickup';
                }else if( $order['shipping_status'] == 2 ) {
                    $order['shipping_statusName'] = 'In Transit';
                }else if( $order['shipping_status'] == 3 ) {
                    $order['shipping_statusName'] = 'Out for Delivery';
                }else if( $order['shipping_status'] == 4 ) {
                    $order['shipping_statusName'] = 'Shipped';
                }else {
                    $order['shipping_statusName'] = 'Unknown';
                }

                $response['data'] = $order;
                                
                /*foreach( $data as $order ) {
                    $res = array();
                    $res['orderID'] = $order['orderID'];
                    $res['userID'] = $order['userID'];
                    $res['storeID'] = $order['storeID'];
                    $res['billing_address'] = $order['billing_address'];
                    $res['shipping_address']  = $order['shipping_address'];
                    $res['amount'] = $order['amount'];
                    $res['shipping_status'] = $order['shipping_status'];
                    $res['order_status'] = $order['status'];
                    $res['created'] = $order['created'];
                   
                  $response['data'][] = $res;
                }*/
          }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
     /** 
     *  Order Repeat 
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     *
     * @response {
     *      "data": {},
     *      "status": "success"
     * }
     *
     */

    public function order_products( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        $valid = Validator::make( $request->all() , [
            'userID'  => ['required'],
            'storeID' => ['required'],
        ], [
            'userID.required'  => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID',
        ]);

        if( !$valid->fails() ) {

            $response['status'] = 'success';

            $data = order::where('storeID',$request->storeID)->orderBy('created', 'desc')->first();
            if (isset($data->orderID) && !empty($data->orderID)) {
                $products = order_product::join('products','order_products.productID','=','products.productID')->select('order_products.*','products.stock','products.weight','products.unit','products.box')->where('order_products.orderID',$data->orderID)->where('products.stock' ,'>',0)->get();
            }
            if( isset( $products ) && !empty( $products ) ) {  
                $response['data'] = $products;
                /*$response['status'] = 'success';
                
                foreach( $products as $data ) {
                    $res = array();
                    $res['productID'] = $data['productID'];
                    $res['brandID']  = $data['brandID'];
                    $res['productName'] = $data['title'];
                     $res['quantity'] = $data['quantity'];
                     $res['price'] = $data['price'];
                     $res['amount'] = $data['amount'];
                   
                    $response['data'][] = $res;
                }
            }




          //   if( isset( $data) && !empty( $data  ) ){  

          //       $order = $data;
          //       $response['status'] = 'success';

          //        if( $order['status'] == 0 ) {
          //           $orde['statusName'] = 'draft';
          //       }else if( $order['status'] == 1 ) {
          //           $order['statusName'] = 'placed';
          //       }else if( $order['status'] == 2 ) {
          //           $order['statusName'] = 'Success with payment';
          //       }else if( $order['status'] == 3 ) {
          //           $order['statusName'] = 'Failure';
          //       }else if( $order['status'] == 4 ) {
          //           $order['statusName'] = 'payment unpaid';
          //       }else {
          //           $order['statusName'] = 'Unknown';
          //       }

          //       $order['created'] = date( 'c', strtotime( $order->created ) );
          //       $order['updated'] = date( 'c', strtotime( $order->updated ) );


          //       if( $order['shipping_status'] == 0 ) {
          //           $orde['shipping_statusName'] = '';
          //       }else if( $order['shipping_status'] == 1 ) {
          //           $order['shipping_statusName'] = 'Pickup';
          //       }else if( $order['shipping_status'] == 2 ) {
          //           $order['shipping_statusName'] = 'In Transit';
          //       }else if( $order['shipping_status'] == 3 ) {
          //           $order['shipping_statusName'] = 'Out for Delivery';
          //       }else if( $order['shipping_status'] == 4 ) {
          //           $order['shipping_statusName'] = 'Shipped';
          //       }else {
          //           $order['shipping_statusName'] = 'Unknown';
          //       }

          //       $response['data'] = $order;
                                
          //       /*foreach( $data as $order ) {
          //           $res = array();
          //           $res['orderID'] = $order['orderID'];
          //           $res['userID'] = $order['userID'];
          //           $res['storeID'] = $order['storeID'];
          //           $res['billing_address'] = $order['billing_address'];
          //           $res['shipping_address']  = $order['shipping_address'];
          //           $res['amount'] = $order['amount'];
          //           $res['shipping_status'] = $order['shipping_status'];
          //           $res['order_status'] = $order['status'];
          //           $res['created'] = $order['created'];
                   
          //         $response['data'][] = $res;
          //       }*/
          // }
        }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Manage order data
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam orderID int required Unique order ID Example: 1234
     * @bodyParam status int required Unique order status Example: 1= pendding payment, 2= Paid 
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     */
    public function post_order( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
             'orderID'       => [],
             'userID'        => ['required'],
             'storeID'       => ['required'],
             'products'      => ['required'],
             'transactionID' => [],
             'amount'        => [],
             'type'          => [],
             'gateway'       => [],
             'method'        => [],
             'discount'      => [],
             'tax'           => [],
             'status'        => ['required'],
        ], [
            'userID.required'  => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID',
            'products.required' => 'Please add at least one product to the cart'
        ]);

        if( !$valid->fails() ) {
            $response['status'] = 'success';

            $store    = store::find( $request->storeID );
            $users    = User::where('role',1)->get();
            $date     = $store->credit_period;
            $due_date  = date('d/m/Y',strtotime('+'.$date.' days',strtotime(str_replace('/', '-', date("d/m/Y"))))) . PHP_EOL;
    
            
            if ($store['payment_status'] == 1) {
                $response['messages'][] = array( 'message' => 'Oops! Your store has blocked', 'type' => 'error' );
                return $response;
            }
                     

            $data = array(
                'userID'           => $request->userID,
                'storeID'          => $request->storeID,
                'billing_address'  => $store->billing_address,
                'shipping_address' => $store->shipping_address,
                'products'         => array(),
                'brands'           => array(),
                'items'            =>  0,
                'amount'           =>  0,
                'tax'              =>  0,
                'due_date'         => $due_date,
                'status'           => $request->status,
            );

            $data['order_products'] = array();
            foreach ( $request->products as $product ) {
                $tax = $product['tax'] + $product['cess'];
                $product_taxt =  ($product['price'] * $product['quantity'] * $tax  ) / 100;
                $data['order_products'][] = array(
                    'orderID'   => null,
                    'storeID'   => $request['storeID'],
                    'title'     => $product['title'],
                    'productID' => $product['productID'],
                    'tax'       => $product['tax'],
                    'hsn'       => $product['hsn'],
                    'cess'      => $product['cess'],
                    'image'     => $product['image'],
                    'brand_name'=> $product['brandName'],
                    'brandID'   => $product['brandID'],
                    'quantity'  => $product['quantity'],
                    'price'     => $product['price'] / ('1.'.$tax),
                    
                    'amount'    => ($product['price'] * $product['quantity'] ),
                );
                

                $data['items'] += $product['quantity'];
                $data['amount'] += ($product['price'] * $product['quantity'] );
                $data['tax'] +=  $tax;
                if( !in_array( $product['productID'], $data['products'] ) ) {
                    $data['products'][] = $product['productID'];
                }

                if( !in_array( $product['brandID'], $data['brands'] ) ) {
                    $data['brands'][] = $product['brandID'];
                }
                $product_update = product::find($product['productID']);
                $product_data = array(
                            'stock' => $product_update['stock'] - $product['quantity'],
                        );
                $product_update->update($product_data);
            }

            // $data['products'] = json_encode( $request->products );

            $data['products'] = json_encode( $data['products'], JSON_NUMERIC_CHECK );
            $data['brands'] = json_encode( $data['brands'], JSON_NUMERIC_CHECK );
            if($request->orderID > 0 ){

                $order = order::find($request->orderID);
                $order->update($data);

                foreach ( $data['order_products'] as $product ) {
                    $product['orderID'] = $request->orderID;
                    $order_product = order_product::where( 'orderID', '=', $request->orderID );
                    $order_product->update( $product );
                } 
            }else{

               $order = order::create($data);

                foreach ( $data['order_products'] as $product ) {
                    $product['orderID'] = $order->orderID;

                    order_product::create( $product );
                }

            //create Notification
            $device = array();

            $user = User::find($order->userID);
            $devices['userID'] = $order->userID;
            $devices['storeID'] = $order->storeID;
            $devices['referenceID'] = $order->orderID ;
            $devices['senderID'] = $order->userID;
            $devices['title'] = "order #".$order->orderID;
            $devices['message'] = $user['name']." has generated a new order #".$order->orderID;
            $devices['body'] = $user['name']." has generated a new order #".$order->orderID;
            $devices['module'] = "orders";
            post_notification($devices);
        }

            // $order_user = User::find($request->userID);

            // $email= $order_user['email'];
            // $sms = 'xyz';
            // $to_name = 'Supplyport';
            // $to_email = $email;
            // $data = array('name'=>"Cloudways (sender_name)", "body" => "tast mail");
            // Mail::send('admin.mail', $data, function($message) use ($to_name, $to_email) {
            //     $message->to($to_email, $to_name)
            //     ->subject('');
            //     $message->from('donotreply@supplyport.com','Supply Port');
            // });
            // foreach ($users as $mail) {
            //     $data = array('name'=>"Cloudways (sender_name)", "body" => "tast mail");
            //     Mail::send('admin.mail', $data, function($message) use ($to_name, $mail) {
            //          $message->to($mail['email'], $to_name)
            //          ->subject('Supplyport order');
            //          $message->from('donotreply@supplyport.com','Supply Port');
            //    });
            // }

            // $order_data = order::find( $orderID );

        $user = User::find($request->userID);
        $response['data'] = $order;
        $response['data']['mobile'] = $user['mobile'];
        $response['messages'][] = array( 'message' => 'Order has been placed successfully', 'type' => 'success' );            
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /*public function put_order( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
             'orderID' => ['required'],
             'transactionID' => ['required'],
             'products' => ['required'],
             'amount' => [],
             'type' => [],
             'gateway' => [],
             'method' => [],
             'discount' => [],
             'tax' => [],
             'status' => [],

        ]);

        if( !$valid->fails() ) {
            $response['status'] = 'success';
            $response['values'] = $request->all();

            $order = order::find($request->orderID);

            $data = array(
                'userID' => $request->userID,
                'storeID' => $request->storeID,
                'billing_address' => $store->billing_address,
                'shipping_address' => $store->shipping_address,
                'products' => array(),
                'brands' => array(),
                'items' =>  0,
                'amount' => 0,
                'tax' => '12',
                'status' => 0,
               
            );
        $order->update($data);

         $data['order_products'] = array();
            foreach ( $request->products as $product ) {
                $data['order_products'][] = array(
                    'orderID'   => null,
                    'title'     => $product['title'],
                    'productID' => $product['productID'],
                    'brandID'   => $product['brandID'],
                    'quantity'  => $product['quantity'],
                    'price'     => $product['price'],
                    'amount'    => ($product['price']*$product['quantity']),
                );  

                $data['items'] += $product['quantity'];
                $data['amount'] += ($product['price']*$product['quantity']);

                if( !in_array( $product['productID'], $data['products'] ) ) {
                    $data['products'][] = $product['productID'];
                }

                if( !in_array( $product['brandID'], $data['brands'] ) ) {
                    $data['brands'][] = $product['brandID'];
                }
            }

            $data['products'] = json_encode( $request->products );

            // $data['products'] = json_encode( $data['products'], JSON_NUMERIC_CHECK );
            $data['brands'] = json_encode( $data['brands'], JSON_NUMERIC_CHECK );

            // $response['data'] = $data;
            // return $response;

            foreach ( $data['order_products'] as $product ) {
                $product['orderID'] = $order->orderID;

                order_product::update( $product );      
            }
                    
            $response['messages'][] = array( 'message' => 'Order Updated successfull', 'type' => 'success' );
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return response()->json( $response );

    }*/
    
    /** 
     * Post order payment details
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam orderID int required Unique order ID Example: 1234
     * @bodyParam transactionID int required Unique transaction ID Example: 1234
     * @bodyParam amount int required  amount Example: 1234
     * @bodyParam type string required  type Example: credit,debit
     * @bodyParam method string required  method Example: xyz
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function post_order_payment( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [ 
            'userID'    => ['required'],
            'storeID'   => ['required'],
            'orderID'   => ['required'],
            'transactionID' => ['required'],
            'amount'    => ['required'],
            'type'      => ['required'],
            'method'    => ['required'],
        ]);


        if( !$valid->fails() ) {
            $response['status'] = 'success';

            $order = order::find($request['orderID']);

            if($order['amount'] == $request['amount']){

                $data = array(
                    'orderID'       => $request['orderID'],
                    'transactionID' => $request['transactionID'],
                    'amount'        => $request['amount'],
                    'type'          => $request['type'],
                    'method'        => $request['method'],
                    'status'        => 1,
                );

                $orderpay = order_payment::create($data);
                Helpers::post_log($request->all());
                if ($request['method'] == 'upi') {
                    $order->update( array(
                            'transactionID' => $request->transactionID,
                            'status'   => '2',
                    ));
                }
        
                $response['messages'][] = array( 'message' => 'Payment received successfully', 'type' => 'success' );
                $response['data'] = $orderpay;
            }else{
                $response['messages'][] = array( 'message' => 'Payment received not successfull', 'type' => 'success' );
                
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;

    }

    /** 
     * Get order product details 
     * 
     * @bodyParam orderID int required Unique order ID Example: 1234
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
     public function get_order_products( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'userID'    => ['required'],
            'storeID'   => ['required'],
            'orderID'   => ['required'],
        ], [
            'userID.required'  => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID',
            'orderID.required' => 'Please enter a order ID',
        ]);

        if( !$valid->fails() ) {
            
            // $order = order::where('orderID',$request->orderID)->get();
            /*foreach( $order as $data ) {
                $res = array();
                $res['orderID'] = $data['orderID'];
                $res['storeID']  = $data['storeID'];
                $res['transactionID'] = $data['transactionID'];
                $res['created'] = $data['created'];
               
                $response['data'][] = $res;
            }*/

            $response['status'] = 'success';
            
            $response['data']['orderID'] = $request->orderID;
            $response['data']['storeID'] = $request->storeID;
            $response['data']['products']= array();
            $order = order::find( $request->orderID );
            if( $order['status'] == 0 ) {
                    $response['data']['status'] = 'draft';
                }else if( $order['status'] == 1 ) {
                    $response['data']['status'] = 'placed';
                }else if( $order['status'] == 2 ) {
                    $response['data']['status'] = 'Success with payment';
                }else if( $order['status'] == 3 ) {
                    $response['data']['status'] = 'Failure';
                }else if( $order['status'] == 4 ) {
                    $response['data']['status'] = 'payment unpaid';
                }else {
                    $response['data']['status'] = 'Unknown';
                } 

                if( $order['shipping_status'] == 0 ) {
                     $response['data']['shipping_statusName'] = 'N/A';
                }else if( $order['shipping_status'] == 1 ) {
                     $response['data']['shipping_statusName'] = 'Pickup';
                }else if( $order['shipping_status'] == 2 ) {
                     $response['data']['shipping_statusName'] = 'In Transit';
                }else if( $order['shipping_status'] == 3 ) {
                     $response['data']['shipping_statusName'] = 'Out for Delivery';
                }else if( $order['shipping_status'] == 4 ) {
                     $response['data']['shipping_statusName'] = 'Shipped';
                }else {
                     $response['data']['shipping_statusName'] = 'Unknown';
                }

            $products = order_product::where( 'orderID', $request->orderID )->get();
            if( isset( $products ) && !empty( $products ) ) {  
                $response['data']['products'] = $products;
                /*$response['status'] = 'success';
                
                foreach( $products as $data ) {
                    $res = array();
                    $res['productID'] = $data['productID'];
                    $res['brandID']  = $data['brandID'];
                    $res['productName'] = $data['title'];
                     $res['quantity'] = $data['quantity'];
                     $res['price'] = $data['price'];
                     $res['amount'] = $data['amount'];
                   
                    $response['data'][] = $res;
                }*/
            }
        }
        
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Post Transaction details
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam fileID int required Unique file ID Example: 1234
     * @bodyParam transactionID int required Unique transaction ID Example: 1234
     * @bodyParam amount int required  amount Example: 1234
     * @bodyParam type string required  type Example: credit,debit
     * @bodyParam orderID int optional  order ID Example: 101
     * @bodyParam method string required  method Example: xyz
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function post_transation( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [ 
            'userID'        => ['required'],
            'orderID'       => [],
            'storeID'       => ['required'],
            'fileID'        => ['required'],
            'transactionID' => ['required'],
            'amount'        => ['required'],
            'type'          => ['required'],
            'method'        => ['required'],
        ]);


        if( !$valid->fails() ) {
            $response['status'] = 'success';
            $data = array(
                'userID'        => $request['userID'],
                'fileID'        => $request['fileID'],
                'fileID'        => $request['orderID'],
                'transactionID' =>$request['transactionID'],
                'amount'        => $request['amount'],
                'type'          => $request['type'],
                'method'        => $request['method'],
                'status'        => 0,
            );

            $orderpay = order_payment::create($data);
            $response['data'] = $orderpay;

            $response['messages'][] = array( 'message' => 'Transaction has been created  successfully', 'type' => 'success' );
                
        
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;

    }

    /** 
     * Get order shipping data 
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam orderID int required Unique order ID Example: 1234
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */   
    public function get_order_shipping( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'userID'  => ['required'],
            'storeID' => ['required'],
            'orderID' => ['required'],
        ], [
            'userID.required'  => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID',
            'orderID.required' => 'Please enter a order ID',
        ]);

        if( !$valid->fails() ) {

            $response['status'] = 'success';

            $response['data']['orderID'] = $request->orderID;
            $response['data']['storeID'] = $request->storeID;
            $response['data']['shipping']= array();

            $watchdog = watchdog::where([ [ 'referenceID', $request->orderID], [ 'storeID', $request->storeID] ] )->orderBy('status' , 'desc' )->get();

            if( isset( $watchdog ) && !empty( $watchdog ) ) {
                $response['data']['shipping'] = $watchdog;
                /*foreach( $watchdog as $log ) {
                    $response['data']['shipping'][] = $log;
                }*/
            }

            $response['data']['status'] = count( $response['data']['shipping'] );

            if( $response['data']['status'] == 0 ) {
                $response['data']['statusName'] = 'processing';
            }else if( $response['data']['status'] == 1 ) {
                $response['data']['statusName'] = 'Pickup';
            }else if( $response['data']['status'] == 2 ) {
                $response['data']['statusName'] = 'In Transit';
            }else if( $response['data']['status'] == 3 ) {
                $response['data']['statusName'] = 'Out for Delivery';
            }else if( $response['data']['status'] == 4 ) {
                $response['data']['statusName'] = 'Shipped';
            }else {
                $response['data']['statusName'] = 'Unknown';
            }
        }
        
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Delete an order 
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam orderID int required Unique order ID Example: 1234
     *
     * @response {
     *      "data": [],
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */   
    public function delete_order( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        
        $valid = Validator::make( $request->all() , [
            'userID'  => ['required'],
            'storeID' => ['required'],
            'orderID' => ['required'],
        ], [
            'userID.required'  => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID',
            'orderID.required' => 'Please enter a order ID',
        ]);

        if( !$valid->fails() ) {
            $response['status'] = 'success';

            $order = order::find( $request->orderID );

            $order->delete();

            $response['messages'][] = array( 'message' => 'Order Deleted successfull', 'type' => 'success' );
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

     /** 
     * Get order summary 
     * 
     * @return \Illuminate\Http\Response 
     */

    public function get_summary( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        
        $response['status'] = 'success';

        $orders = order::latest()->get();

        if( isset( $orders ) && !empty( $orders ) ) {  

            $response['status'] = 'success';
                
            foreach( $orders as $item ) {
                    $res = array();
                    $res['orderID'] = $item['orderID'];

                    if($item['status'] == 0 ) {

                        $res['status'] =  "Draft";

                     }elseif($item['status']== 1) {

                        $res['status'] = "Placed";

                     
                     }else{

                        $res['status'] = "success";
                     }

                    $res['Placed']   = $item['created'];
                    $res['Last updated']  = $item['updated'];
                    ksort( $res );
                   
                    $response['data'][] = $res;
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
}
