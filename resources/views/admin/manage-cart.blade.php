@extends('layouts/admin')

@section('title','Order page')

@section('content')
<div class="cart_section">
     <div class="container-fluid">
         <div class="row">
             <div class="col-lg-12">
                 <div class="cart_container">
                     <div class="cart_title"><strong>Shopping Cart</strong><small> ( <?php echo $cart; ?> items in your cart) </small></div>
                     <div class="cart_items mt-3">
                     	<table class="table">
                     		<thead>
                     			<th>Product</th>
                     			<th>Quantity</th>
                     			<th>Old Price</th>
                     			<th>Price<small> ( per unit price) </small></th>
                     			<th>Action</th>
                     		</thead>
                     		<form action="{{ url('/admin/purchase/order/store') }}" method="post">
                     		@csrf
                     		<tbody>
                     			@foreach($products as $product )
                     			<tr>
                     				<td>
                     					<?php if($product->fileID > 0){?>
                     						<img class="img-fluid" src="{{ asset('public/uploads/thumbnail/'.$product->uri )}}" height="32" width="auto" style="height: 2rem;">
                     						<input type="hidden" name="image[]" value="{{ asset('public/uploads/thumbnail/'.$product->uri )}}">
                     					<?php }else{ ?>
                     						<img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" height="32" width="auto" style="height: 2rem;">
                     						<input type="hidden" name="image[]" value="{{ asset('public/media/default-product.png') }}">
                     					<?php  } ?>
                     					{{ $product->title ?? '' }}
                     					<input type="hidden" name="title[]" value="{{  $product->title ?? '' }}">
                     					<input type="hidden" name="productID[]" value="{{  $product->productID ?? '' }}">
                     					<input type="hidden" name="brandID[]" value="{{  $product->brandID ?? '' }}">
                     					<input type="hidden" name="hsn[]" value="{{  $product->hsn ?? '' }}">

                     				</td>
                     				<td>
                     					<input type="number" name="quantity[]" required="required">
                     				</td>
                     				<td>
                     					₹ {{ $product->max_price ?? '' }}
                     				</td>
                     				<td>
                     					₹ <input type="number" name="Price[]" required="required">
                     				</td>
                     				<td>
                     					<a href ="{{ url('/admin/purchase/delete/'.$product->cartID)}}" class="btn btn-danger" onclick="javascript:return confirm('Are you sure you want to delete this Product?');" title="Delete">
                     						<i class="bi bi-trash"></i>
                     						<span></span>
                     					</a>
                     				</td>
                     			</tr>
                     			@endforeach
                     			<input type="hidden" name="supplier" value="{{ $supplier ?? ''}}">
                     		</tbody>
                     	</table>
                     </div>
                     <!-- <div class="order_total">
                        <div class="order_total_content text-md-right">
                            <div class="order_total_title"><strong>Order Total:</strong></div>
                             ₹ <input type="number" name="order_total" required="required">
                             
                        </div>
                    </div> -->
                     <div class="cart_buttons">
                     	<a href="{{ url('admin/purchase/order/products/'.$supplier)}}" class="btn btn-outline-secondary">Continue Shopping</a> 
                     	<button type="submit" class="btn btn-success">Purchase</button>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('stylesheets')
<style type="text/css">



.order_total {
    width: 100%;
    height: 60px;
    margin-top: 30px;
    border: solid 1px #e8e8e8;
    box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.1);
    padding-right: 46px;
    padding-left: 15px;
    background-color: #fff
}

.order_total_title {
    display: inline-block;
    font-size: 14px;
    line-height: 60px
}

.order_total_amount {
    display: inline-block;
    font-size: 18px;
    font-weight: 500;
    margin-left: 26px;
    line-height: 60px
}

.cart_buttons {
    margin-top: 60px;
    text-align: right
}



</style>
@endsection
