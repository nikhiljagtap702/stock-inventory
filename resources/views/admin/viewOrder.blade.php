@extends('layouts/admin')
@section('title','Order #'.$data->orderID)
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="order-box">
      <div class="order-details-box">
        <?php $id = $data->orderID;
        $status = $data->status;
        $Shipping = $data->shipping_status;
        ?>
        <div class="order-main-info">
          <span>Order : </span><strong>{{$data->orderID}}</strong>
        </div>
        <div class="order-sub-info">
           <?php if(in_array('4',$watchd)){ ?>
                  <a href="{{url('admin/order/pdf/'.$data->orderID)}}">Download Invoice</a>
               <?php  }else{ ?>
                <a href="{{url('admin/order/pdf/'.$data->orderID)}}">Download Purchase order </a>
                
            <?php } ?>
          <!-- <a href="{{url('admin/order/pdf/'.$data->orderID)}}">Download Invoice</a> -->
          <span class="pl-2">Placed On : </span><strong><?php echo  date('d M Y',strtotime($data->created)); ?></strong>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
    <div class="row pt-3">
        <div class="col-md-12">
            <div class="cart">
                    <table class="table">
                        <thead>
                            <th>Item</th>
                            <th>HSN</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Tax(%)</th>
                            <th>Total</th>
                        </thead>
                        <tbody>
                        <?php  $sub_total = $data->amount;
                             $tax = $data->tax;
                        // $data = json_decode($data->products, true);  ?>
                        @foreach($order_product as $orders)
                        <tr>
                            <td>
                                <?php if($orders['image']){?>
                                       <img  src="{{ $orders['image']}}" alt="..." class="img-fluid">
                                <?php }else{ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" >
                                <?php } ?>
                                {{ $orders['title'] }}
                            </td>
                            <td>{{ $orders['hsn'] }}</td>
                            <td>{{ $orders['price'] }}</td>
                            <td>{{ $orders['quantity'] }}</td>
                            <td> {{ $orders['tax'] + $orders['cess'] }}</td>
                            <td> {{ $orders['amount'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row my-5">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Invoice Address</h5>
                    <p class="card-text"><?php if($bill){ ?>
                        <p>{{ $bill->name }}<br>{{ $bill->email }}<br>{{ $bill->phone }}<br>{{ $bill->street }}<br>{{ $bill->locality }}<br>{{ $bill->region }}<br>{{ $bill->address }}<br>{{ $bill->city }}<br>{{ $bill->state }}<br>{{$bill->postal_code}}<br>{{$bill->country}}</p><?php } ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Shipping Address</h5>
                    <p class="card-text"> <?php if($ship){ ?>
                          {{ $ship->name }}<br>{{$ship->email}}<br>{{$ship->phone}}<br>{{$ship->street}}<br>{{$ship->locality}}<br>{{$ship->region}}<br>{{$ship->address}}<br>{{$ship->city}}<br>{{$ship->state}}<br>{{$ship->postal_code}}<br>{{$ship->country}}
                          </p>
                        <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Shipping</h5>
                    <p class="card-text">
                        <ul class="order-summary mb-0 list-unstyled">
                        <li class="order-summary-item">
                <p>waiting for pickup<span><?php if(in_array('1',$watchd)){
                  echo '<i class="fa fa-check" aria-hidden="true"></i>';
                } ?></span></p>
              </li>
              <li class="order-summary-item">
                <p>picked up<span> <?php if(in_array('2',$watchd)){
                  echo '<i class="fa fa-check" aria-hidden="true"></i>';
                } ?></span></p>
              </li>
              <li class="order-summary-item border-0">
                <p>out for delivery<strong class="order-summary-total"><?php if(in_array('3',$watchd)){
                  echo '<i class="fa fa-check" aria-hidden="true"></i>';
                } ?></strong></p>
              </li>
              <li class="order-summary-item border-0">
                <p>delivered<strong class="order-summary-total"> <?php if(in_array('4',$watchd)){
                  echo '<i class="fa fa-check" aria-hidden="true"></i>';
                } ?></strong></p>
              </li>
            </ul>
            <?php if(in_array(['1','2','3','4'],$watchd)){ ?>

           <?php }else{ ?>
            <?php if (Auth::check() && Auth::User()->role == 2) { ?>
             
            <?php }else{ ?>
            <a href="{{ url('/admin/orders/create/'.$id.'/') }}" class="btn btn-success">Update Shipping</a>
              
            <?php } } ?></p>
      </div>
    </div>
  </div>
   <div class="col-sm-3">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Order Summary</h5>
        <p class="card-text">
          <table class="table">
            <tr>
              <td> <strong> Subtotal </strong></td>
              <?php $total = $sub_total / ('1.'.$tax); ?>

              <td>&#8377;{{ number_format( $total ?? 0) }}.00</td>
            </tr>
            <tr>
              <td><strong> Tax </strong></td>
              <td>&#8377; {{ number_format($sub_total - $total) ?? 0  }}.00 </td>
            </tr>
            <tr>
              <td><strong> Total </strong></td>
              <td>&#8377;{{ number_format($sub_total) ?? 0  }}.00</td>
            </tr>
            <tr>
              <td><strong> Payment Status </strong></td>
              <td><?php if($status == 2 ){ echo "Paid"; }else{ echo "Pending"; } ?></td>
            </tr>
          </table>
          <?php if($status == 2 ){ }else{?>
            <a href="{{ url('/admin/orders/billing/'.$id.'/') }}" class="btn btn-success">Add Payment</a></strong>
            <?php } ?></p>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@section('stylesheets')
<style type="text/css">
    .order-box .order-details-box{
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        margin-bottom: 20px;
        -webkit-box-align: center;
        align-items: center;
      }
      .img-fluid {
            max-height: 3rem;
            object-fit: contain;
            object-position: center;
            margin-right: .5rem;
      }
</style>
@endsection