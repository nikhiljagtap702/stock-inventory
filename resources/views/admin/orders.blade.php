@extends('layouts/admin')

@section('title', 'Orders')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
            	<div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Orders</h6>
                </div>
                <div class="col-md-6 text-right">
                </div>
            </div>
        </div>
        
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                    <tr>
                        <th>OrderID</th>
                        <th>Client Name</th>
                        <th>Items</th>
                        <th>Total</th>
                        <th>Order </th>
                        <th>Payment </th>
                        <th>Shipping </th>
                        <th>Placed </th>
                        <th>Updated </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $val)
                    <tr>
                        <?php
                        //  Order status
                        if( $val->status == 0 ) {
                            $val->statusName = 'Draft';
                        }else if( $val->status == 1 ) {
                            $val->statusName = '<span class="text-warning">Placed</span>';
                        }else if( $val->status == 2 ) {
                            $val->statusName = '<span class="text-success">Success</span>';
                        }else if( $val->status == 3 ) {
                            $val->statusName = '<span class="text-danger">Failed</span>';
                        }else {
                            // $val->statusName = 'Unknown';
                            $val->statusName = '<span class="text-warning">Placed</span>';
                        }

                        //  Shipping status
                        if( $val->shipping_status == 0 ) {
                            $val->shippintStatusName = 'N/A';
                        }else if( $val->shipping_status == 1 ) {
                            $val->shippintStatusName = 'Pickup';
                        }else if( $val->shipping_status == 2 ) {
                            $val->shippintStatusName = 'In Transit';
                        }else if( $val->shipping_status == 3 ) {
                            $val->shippintStatusName = 'Out for Delivery';
                        }else if( $val->shipping_status == 4 ) {
                            $val->shippintStatusName = '<span class="text-success">delivered</span>';
                        }else {
                            $val->shippintStatusName = 'Unknown';
                        }

                        //  Payment status
                        if( $val->status == 0 || $val->status == 1 ) {
                            $val->paymentStatusName = '<span class="text-daner">Failed</span>';
                        }else if( $val->status == 2 ) {
                            $val->paymentStatusName = '<span class="text-success">Paid</span>';
                        }else {
                            $val->paymentStatusName = '<span class="text-warning">Pending</span>';
                        }
                            /*if($val->transactionID == null){
                                echo "Pending";
                            }else{ 
                                echo "Paid"; 
                            }*/
                        ?>
                        
                            <td class="align-middle">{{ $val->orderID }}</td>
                            <td class="align-middle" ><a href="{{ url( '/admin/orders/view/' . $val->orderID ) }}">{{ $val->title }}</a></td>
                            <td class="align-middle">{{ $val->items }} </td>
                            <td class="align-middle">₹{{ $val->amount }}</td>
                            <td class="align-middle"><?php echo $val->statusName; ?></td>
                            <td class="align-middle"><?php echo  $val->paymentStatusName; ?></td>
                            <td class="align-middle"><?php echo $val->shippintStatusName; ?></td>
                            <td class="align-middle"><?php echo date('d M Y',strtotime($val->created)); ?></td>
                            <td class="align-middle"><?php echo date('d M Y',strtotime($val->updated)); ?></td>
                            <!-- <td>
                                <form action="{{ url('/admin/orders/delete/'.$val->orderID.'/') }}"  method="post">
                                @csrf
                                @method('DELETE')
                                <a href="{{ url('/admin/orders/view/'.$val->orderID.'/') }}" class="btn btn-primary" ><i class="fa fa-eye"></i></a>
                                <button type="submit" class="btn btn-danger" ><i class="fa fa-trash"></i></button>
                                </form>
                            </td> -->
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : [],
          "pageLength": 25
        });
    });
</script>
@endsection