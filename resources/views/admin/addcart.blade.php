@extends('layouts/admin')

@section('title','Order page')

@section('content')
<form class="form" name="form" action="{{ url('/admin/store/cart/store') }}" method="POST">
    @csrf
    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-4">
                    <h6 class="m-0 font-weight-bold text-primary">Cart</h6>
                </div>
                <div class="col-4 mx-auto text-center">
                    ₹<span id="orderTotal">0</span>
                    <input type="hidden" name="orderTotal" id="orderTotal1">
                    <input type="hidden" name="storeID"  value="<?php echo Request::segment(5); ?>">
                </div>
                <div class="col-4 text-right">
                    <button type="submit" class="btn btn-sm btn-success" id="submit">
                        <i class="fas fa-save mr-1"></i>
                        <span>Confirm</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-border table-hover table-sm" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col">Container</th>
                            <th scope="col">Brand</th>                      
                            <th scope="col">Pieces/Box</th>
                            <th scope="col">Price</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $product)
                        <tr>
                            <td>
                                <img class="img-fluid mr-2" src="{{ $product->avatar ?? asset('public/media/default-product.png') }}" height="32" width="auto"  style="height: 1.5rem;width: auto;">
                                <input type="hidden" name="image[]" value="{{ $product->avatar ?? asset('public/media/default-product.png') }}">
                                    {{ $product->title ?? '' }}
                                <input type="hidden" name="title[]" value="{{ $product->title ?? '' }}">
                                <input type="hidden" name="product_id[]" value="{{ $product->productID ?? '' }}">
                            </td>
                            <td>{{ $product->box ?? '' }}</td>
                            <input type="hidden" name="box[]" value="{{$product->box ?? ''}}">
                            <td>{{ $product->brandName ?? ( $product->category ?? '' ) }}</td>
                            <input type="hidden" name="brandName[]" value=" {{ $product->brandName ?? ( $product->category ?? '' ) }}">
                            <input type="hidden" name="brand_id[]" value="{{ $product->brandID ?? '' }}">
                            <td>{{ $product->pieces ?? 0 }}</td>
                            <input type="hidden" name="pieces[]" value="{{ $product->pieces ?? 0 }}">
                            
                            <td>{{ $product->productstore ?? 0 }}</td>
                            <input type="hidden" name="Price[]" value=" {{ $product->productstore ?? 0 }}">
                            <td>
                                <?php if (isset($product->stock) && !empty($product->stock)) {
                                    echo $product->stock;
                                }else{
                                    echo "<span class='text-danger'> Out of Stock</span>";
                                } ?>
                                <input type="hidden" name="stock" class="stock" value="{{ $product->stock }}">  
                            </td>
                            <td class="qty">
                                <?php if ( isset($product->stock) && !empty($product->stock)) { ?>
                                    
                                    <button type="button" class="btn btn-sm btn-primary rounded-circle btn-qty minus" onclick="updateQuantity( 'remove', this, event )" data-product="{{ $product ?? '' }}">-</button>
                                    <span class="quantity-value">0</span>
                                    <input type="hidden" name="quantity[]" class="quantity-value1" value="0">                                    
                                    <button type="button" class="btn btn-sm btn-primary rounded-circle btn-qty plus" onclick="updateQuantity( 'add', this, event )" data-product="{{ $product ?? '' }}">+</button>

                                <?php }else{ ?>

                                    <button type="button" class="btn btn-sm btn-primary rounded-circle btn-qty minus"   onclick="javascript:return confirm('Stock not available');">-</button>
                                    <span class="quantity-value">0</span>
                                    <input type="hidden" name="quantity[]" class="quantity-value1" value="0">                                    
                                    <button type="button" class="btn btn-sm btn-primary rounded-circle btn-qty plus"  onclick="javascript:return confirm('Stock not available');">+</button>

                                <?php } ?>
                            </td>
                            <td class="total">0</td> 
                            <input type="hidden" name="total[]" class="total1">                   
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.card-body -->
    </div><!-- /.card -->
</form>  


@endsection
@section('stylesheets')
<style type="text/css">
    .btn-qty {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        height: 2rem;
        width: 2rem;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector( '#submit' ).disabled = true;
    });

   
    var order = {};
    order.quantity = 0;
    order.total = 0;
    order.products = [];

    function updateQuantity( action, el, ev ) {
        ev.preventDefault();
        var row = el.parentElement.parentElement;
        var product = JSON.parse( ev.target.dataset.product ) || {};
        var qty = parseInt( row.querySelector( '.quantity-value1' ).value ) || 0;
        if( action === 'remove' ) {
            if( qty > 0 ) { 
                qty--;
                order.quantity--;
                order.total -= parseFloat( product.productstore );
                if( order.total == 0){

                    document.querySelector( '#submit' ).disabled = true;
                    }else{

                        document.querySelector( '#submit' ).disabled = false ;

                    }
            }
        }else {

            var stock = parseInt( row.querySelector( '.stock' ).value ) || 0;
            if (qty >= stock ) {

            }else{
                qty++;
                order.quantity++;
                order.total += parseFloat( product.productstore );

                document.querySelector( '#submit' ).disabled = false ;
                if( order.total == 0){

                    document.querySelector( '#submit' ).disabled = true;
                }
            }

        }

        //  Update values
        row.querySelector( '.quantity-value' ).innerHTML = qty;
        row.querySelector( '.total' ).innerHTML = qty*product.productstore;
        row.querySelector( '.total1' ).value = qty*product.productstore;
        row.querySelector( '.quantity-value1' ).value = qty;        
        document.querySelector( '#orderTotal' ).innerHTML = order.total;
        document.querySelector( '#orderTotal1' ).value = order.total;
    }

</script>
@endsection

