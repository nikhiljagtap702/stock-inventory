<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;


class MailController extends Controller
{
    public function basic_email() {
      $data = array('name'=>"Supply Port");
   
      Mail::send(['text'=>'admin.mail'], $data, function($message) {
         $message->to('mangallatwal@gmail.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
         $message->from('donotreply@supplyport.com','Supply Port');
      });
      echo "Basic Email Sent. Check your inbox.";
   }
}
