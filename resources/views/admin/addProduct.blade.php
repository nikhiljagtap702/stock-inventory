@extends('layouts/admin')

@section('title','Product page')

@section('content')
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Create Product </h6>
      </div>
     <div class="card-body">
    <form method="post" action="{{ route('product.store') }}" enctype="multipart/form-data" >
       @csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Title </label>
      <input type="text" class="form-control"  name="title">
       @error('title')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
    <div class="form-group col-md-6"> 
      <label>Category</label>
      <input type="text" class="form-control"  name="category">
      @error('category')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
</div>
  <div class="form-row">
   <div class="form-group col-md-6">
      <label>Type </label>
      <input type="text" class="form-control"   name="type">
       @error('type')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
   <div class="form-group col-md-6">
     <label >Stock Keeping Unit</label>
       <input type="text" class="form-control"  name="sku">
      @error('sku')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
</div>

  <div class="form-row">
   <div class="form-group col-md-6">
      <label>Body </label>
      <textarea class="form-control"  name="body"></textarea>
       @error('body')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
     <div class="form-group col-md-6">
       <label>description</label>
    <textarea class="form-control"   name="description"></textarea> 
      @error('description')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
</div>
 <div class="form-row">
   <div class="form-group col-md-3">
      <label>Min_price </label>
      <input type="number" class="form-control"  name="min_price">
       @error('min_price')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
    <div class="form-group col-md-3">
      <label>Max_price </label>
      <input type="number" class="form-control"  name="Max_price">
       @error('box')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
       <div class="form-group col-md-3">
      <label>Weight </label>
      <input type="number" class="form-control"  name="weight">
       @error('min_price')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
     <div class="form-group col-md-3">
      <label>Unit </label>
      <input type="text" class="form-control"  name="unit">
       @error('unit')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
  </div>
 <div class="form-row">
  
   <div class="form-group col-md-3">
      <label>Tax </label>
      <input type="number" class="form-control"  name="tax">
       @error('tax')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
     <div class="form-group col-md-3">
      <label>Margin </label>
      <input type="number" class="form-control"  name="margin">
       @error('margin')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
     <div class="form-group col-md-3">
      <label>Discount </label>
      <input type="number" class="form-control"  name="discount">
       @error('discount')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
     <div class="form-group col-md-3">
       <label>Rating </label>
      <input type="number" class="form-control"  name="rating">
       @error('rating')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
     </div>
  </div>
  <div class="form-row">
  <div class="form-group col-md-3">
      <label> Stock Quantity </label>
      <input type="number" class="form-control"  name="Stock">
       @error('Stock')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
   <div class="form-group col-md-3">
       <label>Box </label>
      <input type="number" class="form-control"  name="box">
       @error('box')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
    <div class="form-group col-md-3">
      <label>Main Image</label>
      <div class="pt-2">
     <input type='file' name="fileToUpload" onchange="readURL(this);" alt="your image" />
      @error('fileToUpload')
      <span class="feedback" role="alert">
      <strong class="text-danger">{{ $message }}</strong>
      </span>
      @enderror
    </div>
    </div>
    <div class="form-group col-md-3">
      <img id="blah" src=""  />
    </div>
</div>


<button type="submit" class="btn btn-primary">ADD PRODUCT</button>
</form>
</div>
</div>
@endsection
<script type="text/javascript">
  function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
