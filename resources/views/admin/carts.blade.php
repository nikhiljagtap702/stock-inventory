@extends('layouts/admin')

@section('title','Order page')

@section('content')
    <div class="card shadow mb-4">
      <div class="card-header py-3">
          <div class="row">
              <div class="col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Orders</h6>
              </div>
               <div class="col-md-6 text-right">
                    <a href="{{ url('/admin/store/cart/add/'.Request::segment(4).'/') }}" class="btn btn-primary">+ADD ORDER</a>
                </div>
              <div class="col-md-6 text-right">
              </div>
            </div>
            </div>

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span>{{ $message }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>
           @endif

          <div class="card-body">
              <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                    <tr>
                     <th>#</th>
                      <th>Customer Name</th>
                      <th>No of Product</th>
                      
                      <!-- <th>Status</th> -->
                      <th>Total</th>
                      <th>Payment Status</th>
                      <!-- <th>Shipping Status</th> -->
                      <th>Date of Creation</th>
                      <th>Last Updated</th>
                      <th>Action</th>
                    </tr>
                   </thead>
                   <tbody>
                    <tr class="active"> 
                      @foreach($data as $val)
                        <td>{{ ++$i }}</td>
                        <td>{{$val->name }}</td>
                        <td>{{$val->items}}</td>
                         <td>{{ $val->amount }}</td>
                        <td>
                            <?php if( $val->status == 0 || $val->status == 1 ) {
                                echo '<span class="text-danger"> Failed</span>';
                            }else if( $val->status == 2 ) {
                                echo '<span class="text-success">Paid</span>';
                            }else {
                               echo  '<span class="text-warning">Pending</span>';
                            } ?>
                        </td>
                        <td><?php echo  date('d M Y',strtotime($val->created)); ?></td>
                        <td><?php echo  date('d M Y',strtotime($val->updated)); ?></td>
                        <td>
                            <form action="{{url('/admin/store/cart/delete/'.$val->orderID.'/') }}"  method="post">
                                @csrf
                                @method('DELETE') 
                                <a href="{{url('/admin/orders/view/'.$val->orderID.'/') }}" class="btn btn-info" ><i  class="bi bi-eye" title="View" ></i><span class="d-none d-md-block">View</span></a>

                                <button type="submit" class="btn btn-danger" ><i class="bi bi-trash"  onclick="javascript:return confirm('Are You Confirm Deletion');"  title="Delete"></i></i><span class="d-none d-md-block">Delete</span></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
  @endsection
  @section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : []
        });
    });
</script>
@endsection