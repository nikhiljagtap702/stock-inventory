<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\offer;
use App\file;
use App\store;
use App\order;
use App\product;
use App\order_product;
use Carbon\Carbon;
use Validator;
use App\Charts\UserChart;
use DB;


/**
 * @group Users
 *
 * APIs for managing users
 */
class dashboardController extends Controller {
    
     /** 
     * Get app dashboard
     * 
     * @queryParam userID required Unique user ID Example: 1
     * @queryParam storeID required Unique store ID Example: 1
     * @bodyParam start_date date optional Analytics start date Example: 2020-01-01
     * @bodyParam end_date date  optional  Analytics end date Example: 2020-03-31
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
     public function get_dashboad( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'userID'     => ['required'],
            'storeID'    => ['required'],
            'start_date' => [],
            'end_date'   => [],
        ], [
            'userID.required' => 'Please enter a user ID',
            'storeID.required' => 'Please enter a store ID',
        ]);

        if( !$valid->fails() ) {

            $response['status'] = 'success';

            $response['data']['offers'] = array();
            $response['data']['orders'] = array();
            $response['data']['analytics'] = array(); 
            $response['data']['visits'] = array();  

            
            $startDate  = Carbon::parse($request->start_date)->format('Y-m-d');
            $endDate  = Carbon::parse($request->end_date)->format('Y-m-d');

            $data = order::where('storeID',$request->storeID)->select( 'orderID','amount','created')
                     ->whereBetween('created', [ $startDate, $endDate ])
                    ->get();

                if( isset( $data ) && !empty( $data ) ) {  

                     $response['status'] = 'success';
                    
                    foreach( $data as $items ) {

                        $res['ID']        =  $items->orderID;
                        $res['title']     =  'Order #' . $items->orderID;
                        $res['value']     =  $items->amount;
                        $res['timestamp'] =  date( 'c', strtotime( $items->created ) );

                        $response['data']['analytics'][] = $res;
                     }   
                } 


            $orders = order::where('storeID',$request->storeID)->whereNotIn('status', [0])->count();
            $pending_orders = order::where('storeID',$request->storeID)->whereNotIn('status',[2])->count();
            $pending_deliveries = order::where('storeID',$request->storeID)->whereNotIn('shipping_status',[4])->count();

            $res = array(

                      array(
                          'title'       => 'Pending Deliveries',
                          'value'       =>  $pending_deliveries,
                          'description' => '',
                         
                      ),
                       array(
                          'title'       => 'Pending Dues',
                          'value'       => $pending_orders,
                          'description' => '',
                         
                      ),  );

            $response['data']['visits'] = $res;
            $offers = offer::latest()->leftjoin('files','offers.fileID','=','files.fileID')->select('offers.*','files.fileName','files.uri')->Where('offers.status', '=' , 1)->get();
          	if( isset( $offers ) && !empty( $offers ) ) {
                  foreach( $offers as $item) {
                      $res = array();
                      $res['offerID']=$item['offerID'];
                      $res['fileID']=$item['fileID'];
                      $res['title']=$item['title'];
                      $res['description']=$item['description'];
                      $res['body']=$item['body'];
                      // $res['url']=$item['uri'];
                      $res['status']=$item['status'];
                      // $res['created']=$item['created'];
                      // $res['updated']=$item['updated'];
                      if( !isset( $item['directory'] ) || empty( $item['directory'] )) {
                          $item['directory'] = 'public/uploads';
                      }
                       if (isset($item['uri']) && !empty($item['uri'])) {
                          $res['fileUrl']    = url( $item['directory'] . '/' . $item['uri'] );
                          // $res['avatar']     = url( $item['directory'] . '/thumbnail/' . $item['uri'] );
                          $res['avatar']     = url( $item['directory'] . '/' . $item['uri'] );
                      }else{
                          $res['fileUrl']  = url( $item['directory'] . '/default-product.png');
                          // $res['avatar']   = url( $item['directory'] . '/thumbnail/default-product.png');
                          $res['avatar']   = url( $item['directory'] . '/default-product.png');
                      }
                      
                      //  To be removed
                      $res['avatar'] = $res['fileUrl'];


                      $response['data']['offers'][] = $res;
                  }
              }

           $orders = order::latest()->where([ 
              ['storeID', '=', $request->storeID] ])->offset(0)->limit(5)->get();
           $response['data']['orders'] = $orders;
            if( isset(  $response['data']['orders'] ) && !empty( $response['data']['orders'] ) ) {
                $response['status'] = 'success';
                foreach ( $response['data']['orders'] as $value) {

                    if( $value['status'] == 0 ) {
                        $value['statusName'] = 'draft';
                    }else if( $value['status'] == 1 ) {
                        $value['statusName'] = 'placed';
                    }else if( $value['status'] == 2 ) {
                        $value['statusName'] = 'Success with payment';
                    }else if( $value['status'] == 3 ) {
                        $value['statusName'] = 'Failure';
                    }else if( $value['status'] == 4 ) {
                        $value['statusName'] = 'payment unpaid';
                    }else {
                        $value['statusName'] = 'Unknown';
                    } 

                    if( $value['shipping_status'] == 0 ) {
                        $value['shipping_statusName'] = 'N/A';
                    }else if( $value['shipping_status'] == 1 ) {
                        $value['shipping_statusName'] = 'Pickup';
                    }else if( $value['shipping_status'] == 2 ) {
                        $value['shipping_statusName'] = 'In Transit';
                    }else if( $value['shipping_status'] == 3 ) {
                        $value['shipping_statusName'] = 'Out for Delivery';
                    }else if( $value['shipping_status'] == 4 ) {
                        $value['shipping_statusName'] = 'Shipped';
                    }else {
                        $value['shipping_statusName'] = 'Unknown';
                    } 
                }
              }else {
                $response['status'] = 'error';
            }


            

            // $response['data']['analytics'][] = array( 'timestamp' => date( 'c', strtotime( '2019-11-01 00:00:00' ) ), 'value' => 90 );
            // $response['data']['analytics'][] = array( 'timestamp' => date( 'c', strtotime( '2019-12-01 00:00:00' ) ), 'value' => 120 );
            // $response['data']['analytics'][] = array( 'timestamp' => date( 'c', strtotime( '2020-01-01 00:00:00' ) ), 'value' => 80 );
            // $response['data']['analytics'][] = array( 'timestamp' => date( 'c', strtotime( '2020-02-01 00:00:00' ) ), 'value' => 50 );
            // $response['data']['analytics'][] = array( 'timestamp' => date( 'c', strtotime( '2020-03-01 00:00:00' ) ), 'value' => 40 );
            // $response['data']['analytics'][] = array( 'timestamp' => date( 'c', strtotime( '2020-04-01 00:00:00' ) ), 'value' => 10 );

             // $product = product::latest()->leftjoin('files','products.fileID','=','files.fileID')->select('products.*','files.uri')->where('products.status', '=' , 1)->offset(0)->limit(5)->get();

              $product = product::latest()->leftjoin('files','products.fileID','=','files.fileID')->leftjoin('store_products','products.productID', '=' ,'store_products.productID')->where('store_products.storeID', '=',$request->storeID)->where('products.stock','>',0)->select('products.*','files.uri','store_products.price as sprice')->offset(0)->limit(5)->get();
        
            if( isset( $product ) && !empty( $product ) ) {

                foreach( $product as $item) {
                    $res = array();
                    $res['productID']=$item['productID'];
                    $res['fileID']=$item['fileID'];
                    $res['brandID']=$item['brandID'];
                    $res['sku']=$item['sku'];
                    $res['category']=$item['category'];
                    $res['type']=$item['type'];
                    $res['title']=$item['title'];
                    $res['description']=$item['description'];
                    $res['body']=$item['body'];
                    $res['pieces']=$item['pieces'];
                    $res['box']=$item['box'];
                    $res['price']=$item['sprice'];
                    $res['max_price']=$item['max_price'];
                    $res['weight']=$item['weight'];
                    $res['unit']=$item['unit'];
                    $res['tax']=$item['tax'];
                    $res['tax']   = floatval( $item['tax'] );
                    $res['cess']  = floatval( $item['cess'] );
                    $res['hsn']   = $item['hsn'];
                    $res['margin']=$item['margin'];
                    
                    if ($item['max_price'] > 0) {
                      $res['discount']   = round(($item['max_price']-$item['sprice'])/$item['max_price']*100,0);
                       $res['saving']    = $item['max_price']* $res['discount'] /100;
                      $res['discountFormatted']=  $res['discount'].'% Off';
                    }else{
                      $res['discount'] = $item['discount'];
                      $res['discountFormatted'] =  $res['discount'].'% Off';
                      $res['saving']    = $item['discount'];

                    }
                    $res['rating']=$item['rating'];
                    $res['stock']=$item['stock'];
                    $res['sales']=$item['sales'];
                    $res['piecesFormatted']= 'Set of '.$item['pieces'];
                    $res['marginFormatted']= $item['margin'];

                    $res['status']=$item['status'];
                    $res['created']=$item['created'];
                    $res['updated']=$item['updated'];

                    if( !isset( $item['directory'] ) || empty( $item['directory'] )) {
                        $item['directory'] = 'public/uploads';
                    }
                    if (isset($item['uri']) && !empty($item['uri'])) {
                        $res['fileUrl']    = url( $item['directory'] . '/' . $item['uri'] );
                        $res['avatar']     = url( $item['directory'] . '/thumbnail/' . $item['uri'] );
                    }else{
                        $res['fileUrl'] = url( $item['directory'] . '/default-product.png');
                        $res['avatar']     = url( $item['directory'] . '/thumbnail/default-product.png');
                    }
                     //  To be removed
                    $res['avatar'] = $res['fileUrl'];


                    $response['data']['products'][] = $res;
                }
                // $response['data']['products'] = $product;
            }


        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
     

     public function get_analytics( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'start_date' => [],
            'end_date'   => [],
        ]);

        if( !$valid->fails() ) {

            $response['status'] = 'success';
            $response['data']['orders']   = array();  
            $response['data']['brands']   = array();  
            $response['data']['products'] = array();  

            
            $startDate  = Carbon::parse($request->start_date)->format('Y-m-d');
            $endDate  = Carbon::parse($request->end_date)->format('Y-m-d');

            $data = order::where('created','>=',$startDate)->where('created','<=',$endDate)->select( 'orderID','amount','created')->get();
            if( isset( $data ) && !empty( $data ) ) {
                $response['status'] = 'success';
                foreach( $data as $items ) {
                    $res['ID']        =  $items->orderID;
                    $res['title']     =  'Order #' . $items->orderID;
                    $res['value']     =  $items->amount;
                    // $res['timestamp'] =  date( 'c', strtotime( $items->created ) );

                    $response['data']['orders'][] = $res;
                 }   
            }
            $data = order_product::latest()
                            ->whereBetween('created', [ $startDate, $endDate ])
                            ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                            ->groupBy('title','productID')
                            ->get();


                            $title = array();
                            $total_val = array();
                            $total_amount = array();

                            foreach( $data as $item ) {

                              $title[]     =  $item['title'];
                              $total_val[]    =  $item['total_val'];
                              $total_amount[]    =  $item['total_amount'];
                              $response['data']['brands'][] = $item;
                            }
             $brand = order_product::latest()
                     ->whereYear('created', Carbon::now()->year)
                     ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                     ->groupBy('brandID','brand_name')
                     ->get();

                      $brand_name = array();
                      $total_value = array();
                     foreach( $brand as $item ) {

                      $brand_name[]     =  $item['brand_name'];
                      $total_value[]    =  $item['total_val'];
                      $response['data']['brands'][] = $item;
                    }

            // $data = order_product::latest()->whereBetween('created', [ $startDate, $endDate ])->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('brandID','brand_name')->get();
            // if( isset( $data ) && !empty( $data ) ) {
            //     $response['status'] = 'success';
            //     foreach( $data as $item ) {
            //         $res['ID']        =  $item->brandID;
            //         $res['title']     =  $item->brand_name;
            //         $res['value']     =  $item->total_val;
            //         $res['Amount']     =  $item->total_amount;
            //         $response['data']['brands'][] = $res;
            //      }   
            // }
            // $data = order_product::latest()->whereBetween('created', [ $startDate, $endDate ])->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('title','productID')->get();
            // if( isset( $data ) && !empty( $data ) ) {
            //     $response['status'] = 'success';
            //     foreach( $data as $item ) {
            //         $res['ID']        =  $item->productID;
            //         $res['title']     =  $item->title;
            //         $res['value']     =  $item->total_val;
            //         $res['amount']     =  $item->total_amount;
            //         // $res['timestamp'] =  date( 'c', strtotime( $item->created ) );

            //         $response['data']['products'][] = $res;
            //     }
            // }
             //products data
            $orderproductChart = new UserChart;
            $orderproductChart->labels($title);
            $orderproductChart->dataset('Order-Product Value Reporting', 'line',$total_val)
                            ->color("rgb(22,160,133, 0.2)")
                            ->backgroundcolor("rgb(34,198,246, 0.2)");
            $orderproductChart->dataset('Product-Amount', 'line',$total_amount )
                            ->color("rgb(255, 99, 132)")
                            ->backgroundcolor("rgb(255, 205, 86, 0.2)");  

             //brands data
             $brandChart = new UserChart;
             $brandChart->labels($brand_name);
             $brandChart->dataset('Order-Brand Reporting', 'line',$total_value)
                            ->color("rgb(255, 99, 132)")
                            ->backgroundcolor("rgb(255, 99, 132)")
                            ->fill(false)
                            ->linetension(0.1)
                            ->dashed([5]); 
            $response['data'][] = ([ 'orderproductChart' => $orderproductChart ,'brandChart' => $brandChart]); 


   


        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }      
}
