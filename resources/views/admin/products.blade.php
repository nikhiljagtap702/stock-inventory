@extends('layouts/admin')

@section('title','Products')

@section('content')

    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Products</h6>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('products.create') }}" class="btn btn-primary">+ADD PRODUCT</a>
                </div>
            </div>
        </div>
        
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Rate</th>
                            <th scope="col">M.R.P.</th>
                            <th scope="col">Stock Unit</th>
                            <th scope="col">Sold</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $value)
                    <tr>
                        <td scope="row">{{++$i}}</td>
                        <td>
                            <a href="{{ route('products.edit', $value->productID ) }}">
                                <?php if($value->fileID > 0){?>
                                <img class="img-fluid" src="{{ asset('public/uploads/thumbnail/'.$value->uri )}}" height="32" width="auto" style="height: 2rem;">
                            <?php }else{ ?>
                                 <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" height="32" width="auto" style="height: 2rem;">
                           <?php  } ?>
                           <span class="text-lowercase">{{ $value->title }}</span>
                            </a>
                            
                        
                        </td>
                        <td>₹{{ $value->price ?? 0 }}</td>
                        <td>₹{{ $value->max_price ?? 0 }}</td>
                        <td>
                            <?php if ($value->stock > 0) {
                                echo '<span class="text-success">'.$value->stock. '</span>';
                            }else{
                                echo '<span class="text-danger">'.$value->stock. '</span>';
                            }  ?>
                                
                        </td>
                        <td>{{ $value->sales ?? 0 }}</td>
                    </tr>
                    @endforeach                   
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.card-body -->
    </div><!-- /.card -->
    <!--  <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Products Inventory </h6>
                </div>
            </div>
        </div>
       

        <div class="card-body">
           {!! $productChart->container() !!}
        </div> --><!-- /.card-body -->
    </div><!-- /.card -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
  {!! $productChart->script() !!} -->
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
<style type="text/css">
    #page-admin-products .img-fluid {
        max-height: 8rem;
        object-fit: contain;
        object-position: center;
    }

   

</style>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">

    $('#myTable').dataTable({

columnDefs: [{
orderable: false,
className: 'select-checkbox',
targets: 0
}],
select: {
style: 'os',
selector: 'td:first-child'
}
});
</script>
@endsection