@extends('layouts/admin')

@section('title','Order page')

@section('content')
	<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"></h6>
      </div>
     <div class="card-body">
		<form method="post" action="{{ route('orders.update',$data->orderID ) }}" >
        @csrf
        @method('PUT')
      <div class="form-row">
        <div class="form-group col-md-4 mx-auto">
          <label>Status</label>
          <select class="form-control" name="status">
          	<option value="0" <?php if($data->status=='0') echo 'selected="selected"'; ?> >Draft</option>
          	<option value="1" <?php if($data->status=='1') echo 'selected="selected"'; ?> >place</option>
          	<option value="2" <?php if($data->status=='2') echo 'selected="selected"'; ?> >Success</option>
          	<option value="3" <?php if($data->status=='3') echo 'selected="selected"'; ?> >Failure</option>
          </select>
          </div>
        </div>
        <div class="form-group col-md-4 mx-auto">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
@endsection