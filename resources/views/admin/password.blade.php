@extends('layouts/admin')

@section('title','Change Password')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('change.update',$data->userID ) }}" >
        @csrf
        @method('PUT')
        <div class="form-row">
            <div class="form-group col-md-4 mx-auto">
                <label>Current Password</label>
                <input type="password" name="old_password"  class="form-control" required="required">
                @if(session('success'))
                <strong class="text-danger">{{session('success')}}</strong>
                @endif
                @error('old_password')
                <span class="feedback">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4 mx-auto">
                <label>New Password</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                @if(session('error'))
                <strong class="text-danger">{{session('error')}}</strong>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4 mx-auto">
                <label>Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                @error('confirmed')
                <span class="feedback">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group col-md-3 mx-auto">
            <button type="submit" class="btn btn-primary">Change Password</button>
        </div>
    </form>
</div>
</div>
@endsection