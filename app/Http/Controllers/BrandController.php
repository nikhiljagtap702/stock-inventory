<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use App\brand;
use App\order_product;
use Carbon\Carbon;
use App\file;
use App\Charts\UserChart;
use Helpers;

class BrandController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = order_product::whereYear('created', Carbon::now()->year)
                 ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                 ->groupBy('brandID','brand_name')
                 ->get();

         $brand_name = array();
         $total_value = array();

         foreach( $brand as $item ) {
            $brand_name[]     =  $item['brand_name'];
            $total_value[]    =  $item['total_val'];
         }

        //brands data
         $brandChart = new UserChart;
         $brandChart->labels($brand_name);
         $brandChart->dataset('Order-Brand Reporting', 'bar',$total_value)
                        ->color("rgb(255, 99, 132)")
                        ->backgroundcolor("rgb(255, 99, 132)")
                        ->fill(false)
                        ->linetension(0.1)
                        ->dashed([5]);
         $data=brand::latest()->leftjoin('files','brands.fileID','=','files.fileID')->select('brands.*','files.uri')->get();
        return view("admin.brands",[ 'brandChart' => $brandChart ],compact('data'))->with('i');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/brand-manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = brand::find($id);
        echo "<pre>";
        print_r($data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=brand::where('brandID',$id)->leftjoin('files','brands.fileID','=','files.fileID')->select('brands.*','files.uri')->first();
        return view("admin.brand-manage",compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         $brand_data=array(
            'title' => $request['title'],
            'description' => $request['description'],
            'body' => $request['Body'],
            'status' => 1,
        ); 

        //  Check for featured image
        if( $request->fileToUpload ) {
            
           $brand_data['fileID'] = Helpers::post_file($request->fileToUpload);
           
        }

        if( $id > 0 ) {
            $brand = brand::find( $id );
            $brand->update( $brand_data );
            $msg = 'Brand updated successfully';
        }else {

            $brands = brand::latest()->get();
            foreach ($brands as $value) {
                if (strtolower($request['title']) == strtolower($value->title)) {

                    return redirect()->back()->with('error', 'The brand has already exist.');
                   
                }
                
            }

            $brand = brand::create( $brand_data );
            $msg = 'Brand created successfully';
        }
        return redirect( '/admin/brands' )->with('success', $msg );

    }

    public function createThumbnail($path,$filename, $width, $height)
    {
    $img = Image::make($path)->resize($width, $height, function ($constraint) {
        $constraint->aspectRatio();
    });
    $img->save(public_path('uploads/thumbnail/'.$filename));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = brand::find($id);
        $brand->delete();
        return redirect("/admin/brands")->with('success','Brand Deleted successfully');
    }
}
