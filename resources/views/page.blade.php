@extends('layouts.app')

@section('title', ( $page->title ?? 'Page | ' ) )

@section('content')
<div class="container">
   
    <div class="row">
        <div class="col-md-10 mx-auto">
        	 <h1>{{ $title ?? '' }}</h1>
           <p> {{ $body ?? '' }}</p>
        </div>
    </div>
</div>
@endsection
