@extends('layouts/admin')

@section('title','Categories')

@section('content')
    <style type="text/css">
        #page-admin-products .img-fluid {
            max-height: 8rem;
            object-fit: contain;
            object-position: center;
        }
    </style>
    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Categories</h6>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('categories.create') }}" class="btn btn-primary">+ADD CATEGORY</a>
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <div class="card-body">
            <div class="table-responsive">
               <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Created</th>
                        </tr>
                    </thead>
                
                    <tbody>
                        <tr> 
                            @foreach($data as $value )
                           <td scope="row">{{ ++$i }}</td>
                           <td>
                            <a href="{{ route('categories.edit',$value->categoryID)}}">
                            <?php if($value->fileID){?>
                                <img class="img-fluid" src="{{ asset('public/uploads/thumbnail/'.$value->uri )}}" height="32" width="auto" style="height: 2rem;">
                                <?php }else{ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" height="32" width="auto" style="height: 2rem;">
                                <?php } ?>
                                    <?php echo ucfirst($value->title); ?>
                                </a>
                                </td>
                            <td><?php echo  date('d M Y',strtotime($value->created)); ?></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.card-body -->
    </div><!-- /.card -->
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : [],
          "pageLength": 25
        });
    });
</script>
@endsection