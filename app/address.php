<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class address extends Model
{
     const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'addressID';

    
     protected $fillable = [
     	'addressID',
		'userID',
		'name',
		'email',
		'phone',
		'address',
		'street',
		'region',
		'city',
		'state',
		'locality',
		'postal_code',
		'country',
		'status',
	];
}
