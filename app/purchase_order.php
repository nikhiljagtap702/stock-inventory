<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase_order extends Model
{
	const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';

	protected $primaryKey = 'orderID';
    
    protected $fillable = [
   		'userID',
      'storeID',
   		'referenceID',
   		'items',
   		'products',
   		'discount',
   		'tax',
   		'amount',
      'status'
   	];
}
