<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
    
    protected $primaryKey = 'categoryID ';

    public $table = 'categories';

    protected $fillable = [
    	'userID',
        'fileID',
        'module',
        'slug',
        'title',
        'description',
        'keywords',
        'weight',
        'useage',
        'status',
    ];
}
