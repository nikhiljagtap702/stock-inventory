<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    const CREATED_AT = 'created';
   // const UPDATED_AT = 'updated';

	protected $primaryKey = 'logID';
    
   protected $fillable = [
   		'userID',
        'subject',
   		'data',
   		
   ];
}
