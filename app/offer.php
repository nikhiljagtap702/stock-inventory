<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offer extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'offerID';
    protected $fillable = [
        'offerID',
        'fileID',
        'title',
        'description',
        'stores',
        'body',
        'status',
        'uri'
    ];
}
