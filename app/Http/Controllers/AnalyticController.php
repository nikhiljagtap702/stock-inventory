<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\order;
use Carbon\Carbon;
use App\User;
use App\store;
use App\team;
use App\order_product;
use DB;
use App\Charts\UserChart;
use App\product;
use App\brand;

class AnalyticController extends Controller
{
    public function index(Request $request)
    {
        // echo $request;
        // die();
        $current_month = date('M');
        $current_year  = date('Y');
        $last_month    = date('M',strtotime("-2 month"));
        $today         = Carbon::now();
        $yesterday     = Carbon::now()->subDays(1);
        $last_7_day    = Carbon::now()->subDays(7);
        $last_30_day   = Carbon::now()->subDays(30);
        $lastmonths    = Carbon::today()->startOfMonth()->subMonth();

        $a = Carbon::now();
        $a->month($a->month-3);
        $firstOfQuarter = $a->quarter;
        //2nd quarter of the year
        $dt = \Carbon\Carbon::now(); 
        $secondOfQuarter = $dt->quarter;

        $current_month_order = order::whereYear('created', Carbon::now()->year)
                                    ->whereMonth('created', Carbon::now()->month)
                                    ->count();
        $last_month_order    = order::whereMonth('created', Carbon::now()->subMonth(2))
                                    ->count(); 
        $last_to_last_month_order = order::whereYear('created', Carbon::now()->year)
                                    ->count();

        if (isset($request->compare) && !empty($request->compare)) {
            if ($request->compare == 'last_year') {

                $data = order_product::select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->whereYear('created', date('Y', strtotime('-1 year')))->groupBy('title','productID')->get();

                $brand = order_product::select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('brandID','brand_name')->get();
                
            }elseif ($request->compare == 'firstOfQuarter') {

                $data = order_product::where(DB::raw('QUARTER(created)'), $firstOfQuarter)
                        ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                        ->groupBy('title','productID')
                        ->get();

                $brand = order_product::where(DB::raw('QUARTER(created)'), $firstOfQuarter)->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('brandID','brand_name')->get();

            }elseif ($request->compare == 'secondOfQuarter') {
                $data = order_product::where(DB::raw('QUARTER(created)'), $secondOfQuarter)
                        ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                        ->groupBy('title','productID')
                        ->get();

                $brand = order_product::where(DB::raw('QUARTER(created)'), $secondOfQuarter)->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('brandID','brand_name')->get();
                }else{
                    $data = order_product::where('created',$request->compare)
                    ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                    ->groupBy('title','productID')
                    ->get();

                    $brand = order_product::where('created',$request->compare)->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('brandID','brand_name')->get();

                }
            }elseif (isset($request->start_date) && !empty($request->start_date)) {
                $start = $request->start_date;
                $end   = $request->end_date;

                $data = order_product::latest()
                ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                ->groupBy('title','productID')
                ->get();

                $brand = order_product::latest()->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('brandID','brand_name')->get();

            }else{
                $data = order_product::select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                      ->groupBy('title','productID')->whereDate('created', '=', Carbon::today())
                      ->get();
                $brand = order_product::select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))->groupBy('brandID','brand_name')->whereDate('created', '=', Carbon::today())->get();

               
            }

            $title        = array();
            $total_val    = array();
            $total_amount = array();

            foreach( $data as $item ) {

                $title[]     =  $item['title'];
                $total_val[] =  $item['total_val'];
                $total_amount[] =  $item['total_amount'];
            }
            $brand_name = array();
            $total_value = array();

            foreach( $brand as $item ) {
                $brand_name[]     =  $item['brand_name'];
                $total_value[]    =  $item['total_val'];
            }

            $order =order::latest()->join('users','orders.userID','=','users.userID')->select('orders.*','users.name')->take(10)->get();

            //brands data
            $brandChart = new UserChart;
            $brandChart->labels($brand_name);
            $brandChart->dataset('Order-Brand Reporting', 'line',$total_value)
                       ->color("rgb(255, 99, 132)")
                       ->backgroundcolor("rgb(255, 99, 132)")
                       ->fill(false)
                       ->linetension(0.1)
                       ->dashed([5]);
            $borderColors = [
                "rgba(255, 99, 132, 1.0)",
                "rgba(22,160,133, 1.0)",
                "rgba(255, 205, 86, 1.0)",
                "rgba(51,105,232, 1.0)",
                "rgba(244,67,54, 1.0)",
                "rgba(34,198,246, 1.0)",
                "rgba(153, 102, 255, 1.0)",
                "rgba(255, 159, 64, 1.0)",
                "rgba(233,30,99, 1.0)",
                "rgba(205,220,57, 1.0)"
            ];
            $fillColors = [
                "rgba(255, 99, 132, 0.2)",
                "rgba(22,160,133, 0.2)",
                "rgba(255, 205, 86, 0.2)",
                "rgba(51,105,232, 0.2)",
                "rgba(244,67,54, 0.2)",
                "rgba(34,198,246, 0.2)",
                "rgba(153, 102, 255, 0.2)",
                "rgba(255, 159, 64, 0.2)",
                "rgba(233,30,99, 0.2)",
                "rgba(205,220,57, 0.2)"

            ];

            $orderChart = new UserChart;
            $orderChart->labels([$current_month, $last_month, $current_year]);
            $orderChart->dataset('Order Reporting', 'doughnut', [$current_month_order, $last_month_order, $last_to_last_month_order])
                        ->color($borderColors)
                        ->backgroundcolor($fillColors);


            $orderproductChart = new UserChart;
            $orderproductChart->labels($title);
            $orderproductChart->dataset('Order-Product Values ', 'bar', $total_val)
                                ->color($borderColors)
                                ->backgroundcolor($fillColors);
            $orderproductChart->dataset('Order-Product Amounts ', 'bar', $total_amount)
                                ->color($borderColors)
                                ->backgroundcolor($fillColors);

        return view('admin.analytics',[ 'orderChart' => $orderChart ],[ 'orderproductChart' => $orderproductChart ,'brandChart' => $brandChart])->with(compact('today','yesterday','last_7_day','last_30_day','lastmonths'));
    }
}
