<?php

use App\file;
use App\notification;
use App\session;
use Illuminate\Support\Facades\DB;

class Helpers{
    public static function post_log( $request = array(), $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() )){
        $data = array(
                'userID'  => $request['userID'],
                'subject' => 'order transactions',
                'data'    => json_encode($request,true),
                );
       
        return  DB::table('logs')->insert( $data);
    }
    public static function post_file( $file, $request = array(), $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $request['extension'] = $file->extension();

        $request['fileName'] = $file->getClientOriginalName();

        $request['uri'] = md5( microtime(true) ) . '.' . $request['extension'];

        $request['directory'] = 'public/uploads';

        $request['fileType'] = filetype($file);
        $request['fileMime'] = $file->getMimeType();;
        $request['fileSize'] = filesize($file);

        // $request['module'] = '';
        // $request['description'] = '';
        // $request['keywords'] = '';        
        // $request['bgPrimary'] = '';
        // $request['bgSecondary'] = '';
        // $request['location'] = '';
        
        if( !isset( $request['useage'] ) || ( $request['useage'] == '' ) ) {
            $request['useage'] = 1;
        }

        if( !isset( $request['status'] ) || ( $request['status'] == '' ) ) {
            $request['status'] = 1;
        }

        if( !isset( $request['weight'] ) || ( $request['weight'] == '' ) ) {
            $request['weight'] = 1;
        }
      
        list( $request['width'], $request['height'], $request['type'], $request['attr'] ) = getimagesize( $file );

        $file->move( public_path('uploads'), $request['uri'] );

        $fileThumbnail = public_path('uploads/' . $request['uri'] );

        $img = Image::make( $fileThumbnail )->resize(150, 96, function ( $constraint ) {
            $constraint->aspectRatio();
        });

        $img->save( public_path('uploads/thumbnail/' . $request['uri'] ) );

        $response['data']['fileID'] = 0;
        if( $fileUpload = file::create( $request ) ) {
            $response['status'] = 'success';
            $response['data'] = $request;
            $response['data']['fileID'] = $fileUpload->fileID;            
        }

        return $response['data']['fileID'];
        // return $response;
    }


}

define('FIREBASE_SERVER_KEY', 'AAAAh7y-qLs:APA91bHfAPM1yu-vRaAUK0YnojE1AmlbamBFW9rpDrm-1JbUQBKcTp_bqRMuDVz37GcgYuQ35ag-I4b1mJooi5ze4UpE2rBZjffyKkfbNLh-6WcrpqGC8jSAkuNWYxdDyNjmOH3cKSq9');
define('FIREBASE_SENDER_ID', '582987196603');
define('FIREBASE_PROJECT_ID', 'supply-port');
define('FIREBASE_WEB_API_KEY', 'AIzaSyDRBD2nXry6a3b8U3z7ADGM2D4pHg_XhD0');
define('FIREBASE_WEB_ID', 'd1e7675c96cd96aa85219e' );
define('FIREBASE_MEASUREMENT_ID', 'G-HN8BC1ZKXM' );

function post_notification( $request = array(), $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

    if( !isset($request['userID']) || empty($request['userID']) ) {
            $response['errors']['userID'] = 'Please enter a user ID';
        }

        if( !isset($request['senderID']) || empty($request['senderID']) ) {
            $response['errors']['senderID'] = 'Please enter a sender ID';
        }
        
        if( !isset($request['title']) || empty($request['title']) ) {
            $response['errors']['title'] = 'Please enter a title';
        }  

        if( !isset($request['message']) || empty($request['message']) ) {
            $response['errors']['message'] = 'Please enter a message';
        }

        if( !isset($request['body']) || empty($request['body']) ) {
            $response['errors']['body'] = 'Please enter a body';
        }  

        if( !isset($request['module']) || empty($request['module']) ) {
            $response['errors']['module'] = 'Please specify a module';
        }
        
        if( !isset($request['badge']) || empty($request['badge']) ) {
            $request['badge'] = 1;
        }

        if( !isset( $request['status'] ) || ( $request['status'] == '' ) ) {
            $request['status'] = 1;
        }
            
        if( isset( $response['errors'] ) && !empty( $response['errors'] ) ) {
            $response['values'] = $request;
            return $response;
        }

        $request['firebase'] = false;

        //Store notification
        if( $notification = notification::create( $request ) ) {
            $response['status'] = 'success';
            $response['data'] = $request;
            $response['data']['noteID'] = $notification->noteID;            
            $response['notification'] = $notification;

            $request['firebase'] = true;
        }    

        //  Send Firebase Notification
        if( isset( $request['firebase'] ) && ( $request['firebase'] === true ) ) {

            $debug = false;
            if( isset( $request['debug'] ) && ( $request['debug'] === true ) ) {
                $debug = true;
            }

            //  Get user devices        
            //  device = android
            //  fcmToken != ''
            $sessions = session::where('userID', $request['userID'] )->select('fcmToken')->get();

            $device = array();
            foreach( $sessions as $session ) {

                    $fields = array();
                    $fields['to'] = $session['fcmToken'];

                    $fields['priority'] = 'high';

                    $fields['notification'] = $response['data'];

                    $ch = curl_init ();
                    curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                    curl_setopt ( $ch, CURLOPT_POST, true );
                    curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
                        'Authorization: key=' . FIREBASE_SERVER_KEY,
                        'Content-Type: application/json'
                    ));
                    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                    curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
                    
                    if( $debug ) {
                        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
                    }
                    
                    $result = curl_exec ( $ch );

                    if ( $result ) {
                        $response['status'] = 'success';
                    }else {
                        $response['errors'][] = 'Unable to notify user. Error: ' . curl_error( $ch );
                    }

                    if( $debug ) {
                        $response['debug'] = curl_getinfo( $ch );
                    }

                    curl_close ( $ch );

                    $response['firebase'] = json_decode( $result, true );
                
            }
        }

        return $response;
}



?>