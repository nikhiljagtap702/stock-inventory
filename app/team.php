<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class team extends Model {
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $primaryKey = 'teamID';

    public $table = 'team';
    
    protected $fillable = [
		'storeID',
		'userID',
		'role',
	];
}
