@extends('layouts/admin')

@section('title','Offers')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Offers</h6>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('offers.create') }}" class="btn btn-primary">+ADD OFFER</a>
                </div>
            </div>
        </div>
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>StoreID</th>
                            <th>Description</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($data as $value )
                        <tr> 
                            <td scope="row">{{ ++$i }}</td>
                            <td> 
                                <a href="{{ route('offers.edit', $value->offerID ) }}">
                                    <?php if($value->fileID > 0){ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/thumbnail/'.$value->uri )}}" height="32" width="auto" style="height: 2rem;">
                                <?php }else{ ?>
                                          <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" height="32" width="auto" style="height: 2rem;">
                               <?php  } ?>
                                </a>
                                <a href="{{ route('offers.edit', $value->offerID ) }}">{{ $value->title }}</a>
                            </td>
                            <td>{{ $value->stores }}</td>
        				 	<td><?php echo substr($value->description,0,20); ?></td>
        				 	<!-- <td>
    				 		 <form action="{{ route('offers.destroy', $value->offerID )}}"  method="post">
                                <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are You Confirm Deletion');" title="Delete" >
                                    <i class="fa fa-trash"></i>
                                </button>
                                @csrf
                                @method('DELETE')
              
    				 	      </form>
					       </td> -->
				        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.card-body -->
    </div><!-- /.card -->
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : [],
          "pageLength": 25
        });
    });
</script>
@endsection