#!/bin/sh

#   Issue SSL Certificates
#
#   Usage:  
#   sh /var/www/ssl/issue.sh supplyport.in

domain="supplyport.in"
acme=~/.acme.sh/acme.sh

if [ -z "$domain" ]; then
    echo 'Please enter a domain name'
    exit 0
fi

if [ ! -d "/etc/nginx/ssl/$domain" ]; then
    echo 'Creating certificates directory...'
    sudo mkdir -p /etc/nginx/ssl/$domain
fi

echo 'Issuing certificate...'
$acme --issue -d $domain -d *.$domain --dns dns_gd

echo 'Installing certificate...'
$acme --install-cert -d $domain \
--key-file       /etc/nginx/ssl/$domain/key.rsa.pem  \
--fullchain-file /etc/nginx/ssl/$domain/cert.rsa.pem \
--reloadcmd     "sudo systemctl force-reload nginx"

echo 'Issuing ecc certificate...'
$acme --issue -d $domain -d *.$domain --dns dns_gd --keylength ec-384

echo 'Installing ecc certificate...'
$acme --install-cert -d $domain --ecc \
--key-file       /etc/nginx/ssl/$domain/key.ecc.pem  \
--fullchain-file /etc/nginx/ssl/$domain/cert.ecc.pem \
--reloadcmd     "sudo systemctl force-reload nginx"
