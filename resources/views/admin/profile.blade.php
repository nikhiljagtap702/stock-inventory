@extends('layouts/admin')

@section('title','Profile-'.$data->name)

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Profile</h6>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php if($data->fileID > 0){?>
                <img class="card-img-bottom" src="{{ asset('public/uploads/'.$file->uri)}}"  width="100%" >
            <?php }else{ ?>
                <img class="card-img-bottom" src="{{ asset('public/uploads/user.jpg')}}" width="auto" >
            <?php } ?>
            <table cellpadding="10">
                <?php if(Auth::check() && Auth::User()->role ==1 ){ ?>
                    <tr>
                        <td>
                            <a href="{{ route('users.edit', $data->userID ) }}" class="btn btn-primary">
                                <i class="bi bi-pencil-square"></i>
                                <span>Edit</span>
                            </a>
                        </td>
                        <td style="text-align: right;">
                            <a class="btn btn-outline-secondary" href="{{ url('/admin/employees') }}" role="button" title="Go back">
                                <i class="bi bi-caret-left"></i>
                                <span>Back</span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a href="{{ route('change.edit', $data->userID )}}" class="btn btn-info">
                               <i class="bi bi-key"></i> <span> Change Password </span>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        </div>
        <div class="col-md-4">
            <table cellpadding="10">
                <tbody>
                    <tr>
                        <td><strong>Name :</strong></td>
                        <td>{{ $data->name}}</td>
                    </tr>
                    <tr>
                        <td><strong>Email :</strong></td>
                        <td> {{$data->email}}</td>
                    </tr>
                    <tr>
                        <td> <strong>Phone :</strong></td>
                        <td> {{$data->mobile}}</td>
                    </tr>
                    <tr>
                        <td><strong>DOB:</strong></td>
                        <td> {{$data->dob}}</td>
                    </tr>
                    <tr>
                        <td><strong>Company:</strong></td>
                        <td> {{$data->company}}</td>
                    </tr>
                    <tr>
                        <td><strong>Position:</strong> </td>
                        <td>{{$data->position}}</td>
                    </tr>
                    <tr>
                        <td> <strong>Website:</strong></td>
                        <td> {{$data->website}}</td>
                    </tr>
                    <tr>
                        <td><strong>No of  Order  Placed :</strong></td>
                        <td>{{ $order}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
             <table class="table table-bordered">
                <tr>
                    <th>StoreID</th>
                    <th>Role</th>
                </tr>
                <?php $i = 0; ?>
                @foreach($store as $value)
                <tr>
                    <td>{{ $value->storeID }}</td>
                    <td><?php if ($value->storeID == 1) {
                      echo 'admin';
                    }else{
                      echo 'Manager';
                    } ?></td>
                    @endforeach
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection