@extends('layouts/admin')

@section('title','add product store page')

@section('content')
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Create Store Product </h6>
          </div>
     <div class="card-body">
		<form method="post"  action="{{ route('Product.store') }}">
      @csrf
  <div class="form-row">
     <div class="form-group col-md-6">
      <label >Store</label>
      <select id="inputState" class="form-control" name="store">
         @foreach($data as $value)
        <option value="{{ $value->storeID }}" >{{ $value->title }}</option>
       @endforeach
      </select>
    </div>
    <div class="form-group col-md-6">
      <label>Product</label>
     <select id="inputState" class="form-control" name="product">
         @foreach($product as $value)
        <option value="{{ $value->productID }}" >{{ $value->category }}</option>
      @endforeach
    </select>
    </div>
</div>
<div class="form-row">
<div class="form-group col-md-6">
      <label>price</label>
    <input type="text" name="value" class="form-control">
    </div>
  </div>

 
<button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>

@endsection