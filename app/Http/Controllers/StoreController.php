<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\store;
use App\file;
use App\User;
use App\address;
use App\order;
use App\team;
use Auth;
use DB;
use Helpers;
use Carbon\Carbon;
use App\order_product;
use App\store_products;
use App\Charts\UserChart;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() && Auth::User()->role == 2) {

            $data = team::latest()->join('stores','team.storeID', '=' , 'stores.storeID')->leftjoin('users','stores.userID','=','users.userID')->leftjoin('files','stores.fileID','=','files.fileID')->where('team.userID',Auth::User()->userID)->whereNotIn('stores.status',[ 2 ])->select('stores.*','files.uri','users.name','users.userID')->get();
            foreach ($data as $key => $value) {
                $value->products = store_products::where('storeID',$value->storeID)->count();
                $value->members = team::where('storeID',$value->storeID)->count();
                $value->orders = order::where('storeID',$value->storeID)->count();
            }
            return view('admin/stores',compact('data'))->with('i');

        }else{

            $data = store::latest()->leftjoin('files','stores.fileID','=','files.fileID')->leftjoin('users','stores.userID','=','users.userID')->select('stores.*','files.uri','users.name','users.userID')->whereNotIn('stores.status',[ 2 ])->get();
            foreach ($data as $key => $value) {
                $value->products = store_products::where('storeID',$value->storeID)->count();
                $value->members = team::where('storeID',$value->storeID)->count();
                $value->orders = order::where('storeID',$value->storeID)->count();
            }
            return view('admin/stores',compact('data'))->with('i');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       
        // $user = User::where('role',3)->get();
       
        return view('admin.add-store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (User::where('mobile', '=', $request->mobile)->exists() || User::where('mobile', '=', $request->mobile)->exists()) {
            $user = User::where('mobile', '=', $request->mobile)->first();
            return view('admin.store-manage',compact('user'));
        }else{
            return redirect()->back()->with('error', 'this number doesnt exist in our records. Kindly create user before proceeding.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id){
        // echo $id;
        // echo $request->id;
        // die();

        $current_month = date('M');
        $current_year = date('Y');
        $last_month = date('M',strtotime("-2 month"));
        $today =Carbon::now();
        $yesterday = Carbon::now()->subDays(1);
        $last_7_day = Carbon::now()->subDays(7);
        $last_30_day = Carbon::now()->subDays(30);
        $lastmonths = Carbon::today()->startOfMonth()->subMonth();
     // $last_month = date('M',strtotime("-1 month"));
     $last_to_last_month = date('M',strtotime("-2 month"));

     $store = store::find($id);

   


     $data = order_product::whereYear('created', Carbon::now()->year)
                        ->whereMonth('created', Carbon::now()->month)
                        ->where('storeID',$id)
                        ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                        ->groupBy('title','productID')
                        ->get();

                $title = array();
                $total_val = array();
                foreach( $data as $item ) {
                   
                    $title[]     =  $item['title'];
                    $total_val[] =  $item['total_val'];
    
                } 

           //products data
            $orderproductChart = new UserChart;
            $orderproductChart->labels($title);
            $orderproductChart->dataset('Order-Product Reporting', 'line',$total_val )
                    ->color("rgb(255, 99, 132)")
                    ->backgroundcolor("rgb(255, 99, 132)")
                    ->fill(false)
                    ->linetension(0.1)
                    ->dashed([5]);  


     $brand = order_product::whereYear('created', Carbon::now()->year)
                         ->whereMonth('created', Carbon::now()->month)
                         ->where('storeID',$id)
                         ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                         ->groupBy('brandID','brand_name')
                         ->get();
                         
                $brand_name = array();
                $total_value = array();
                 foreach( $brand as $item ) {
                   
                    $brand_name[]     =  $item['brand_name'];
                    $total_value[]    =  $item['total_val'];
    
                }

             //brands data
            $brandChart = new UserChart;
            $brandChart->labels($brand_name);
            $brandChart->dataset('Order-Brand Reporting', 'line',$total_value)
                    ->color("rgb(255, 99, 132)")
                    ->backgroundcolor("rgb(255, 99, 132)")
                    ->fill(false)
                    ->linetension(0.1)
                    ->dashed([5]);

        return view('admin.store-charts',[ 'orderproductChart' => $orderproductChart ,'brandChart' => $brandChart])->with(compact('store','today','yesterday','last_7_day','last_30_day','lastmonths'));
       
    }
    public function view(Request $request){
        
        // echo $request->compare;
        // die();
        $id = $request['id'];

        $current_month = date('M');
        $current_year = date('Y');
        $last_month = date('M',strtotime("-2 month"));
        $today =Carbon::now();
        $yesterday = Carbon::now()->subDays(1);
        $last_7_day = Carbon::now()->subDays(7);
        $last_30_day = Carbon::now()->subDays(30);
        $lastmonths = Carbon::today()->startOfMonth()->subMonth();
         // $last_month = date('M',strtotime("-1 month"));
         $last_to_last_month = date('M',strtotime("-2 month"));
         $a = Carbon::now();
        $a->month($a->month-3);
        $firstOfQuarter = $a->quarter;
        //2nd quarter of the year
        $dt = \Carbon\Carbon::now(); 
        $secondOfQuarter = $dt->quarter;

        $store = store::find($id);
        if ($request->compare == 'last_year') {

            // $order = order::whereYear('created',date('Y', strtotime('-1 year')))
            //                         ->where('storeID',$id)
            //                         ->count();

            $data = order_product::whereYear('created',date('Y', strtotime('-1 year')))
                        ->where('storeID',$id)
                        ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                        ->groupBy('title','productID')
                        ->get();

            $brand = order_product::whereYear('created',date('Y', strtotime('-1 year')))
                         ->where('storeID',$id)
                         ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                         ->groupBy('brandID','brand_name')
                         ->get();
            
            
        }elseif ($request->compare == 'firstOfQuarter') {

            // $order = order::where(DB::raw('QUARTER(created)'), $firstOfQuarter)
            //                         ->where('storeID',$id)
            //                         ->count();

            $data = order_product::where(DB::raw('QUARTER(created)'), $firstOfQuarter)
                        ->where('storeID',$id)
                        ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                        ->groupBy('title','productID')
                        ->get();

            $brand = order_product::where(DB::raw('QUARTER(created)'), $firstOfQuarter)
                         ->where('storeID',$id)
                         ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                         ->groupBy('brandID','brand_name')
                         ->get();

        }elseif ($request->compare == 'secondOfQuarter') {

             $data = order_product::where(DB::raw('QUARTER(created)'), $secondOfQuarter)
                    ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                    ->groupBy('title','productID')
                    ->get();

            // $order = order::where(DB::raw('QUARTER(created)'), $secondOfQuarter)
            //                         ->where('storeID',$id)
            //                         ->count();


            $data = order_product::where(DB::raw('QUARTER(created)'), $secondOfQuarter)
                        ->where('storeID',$id)
                        ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                        ->groupBy('title','productID')
                        ->get();

            $brand = order_product::where(DB::raw('QUARTER(created)'), $secondOfQuarter)
                         ->where('storeID',$id)
                         ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                         ->groupBy('brandID','brand_name')
                         ->get();
        }else{
            // $order = order::where('storeID',$id,'created',$request->compare)->count();

        
            $data = order_product::where('created',$request->compare)
                        ->where('storeID',$id)
                        ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                        ->groupBy('title','productID')
                        ->get();

            $brand = order_product::where('created',$request->compare)
                         ->where('storeID',$id)
                         ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
                         ->groupBy('brandID','brand_name')
                         ->get();
        }

        

        $title = array();
        $total_val = array();
        foreach( $data as $item ) {
           
            $title[]     =  $item['title'];
            $total_val[] =  $item['total_val'];

        } 

       //products data
        $orderproductChart = new UserChart;
        $orderproductChart->labels($title);
        $orderproductChart->dataset('Order-Product Reporting', 'line',$total_val )
                ->color("rgb(255, 99, 132)")
                ->backgroundcolor("rgb(255, 99, 132)")
                ->fill(false)
                ->linetension(0.1)
                ->dashed([5]);  


    
                         
        $brand_name = array();
        $total_value = array();
         foreach( $brand as $item ) {
           
            $brand_name[]     =  $item['brand_name'];
            $total_value[]    =  $item['total_val'];

        }

         //brands data
        $brandChart = new UserChart;
        $brandChart->labels($brand_name);
        $brandChart->dataset('Order-Brand Reporting', 'line',$total_value)
                ->color("rgb(255, 99, 132)")
                ->backgroundcolor("rgb(255, 99, 132)")
                ->fill(false)
                ->linetension(0.1)
                ->dashed([5]);

        return view('admin.store-charts',[ 'orderproductChart' => $orderproductChart ,'brandChart' => $brandChart])->with(compact('store','today','yesterday','last_7_day','last_30_day','lastmonths'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = store::find($id);
        $user = User::find($data->userID);
        $file = file::find($data->fileID);
        $billing =address::find($data->billing_address);
        $shipping =address::find($data->shipping_address);
        return view('admin.store-manage')->with(compact('data','billing','shipping','file','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){


        $store_data=array(

            'title'           => $request['title'],
            'restaurant_name' => $request['restaurant_name'],
            'tagline'         => $request['tagline'],
            'domain'          => $request['domain'],
            'favicon'         => $request['favicon'],
            'gstin'           => $request['gst'],
            'description'     => $request['description'],
            'keywords'        => $request['keyword'],
            'payment_status'  => $request['payment'],
            'order_limit'     => $request['order_limit'],
            'credit_period'   => $request['credit_period'],
            'status'          => 1,
           
        );

        if($id > 0 ) {
             
        }else{
             $store_data['userID'] =  $request['owner'];
        }
        //store admin
        $team_data = array(
            'userID'  => $request['owner'],
            'role'    => 1,
        );

        //billing data

        $Add_data =  array(
             // 'userID' => $user,
             'name' => $request['name'],
             'email' => $request['email'],
             'phone' => $request['mob'],
             'address' => $request['address'],
             'street' => $request['street'],
             'region' => $request['region'],
             'city' => $request['city'],
             'state' => $request['state'],
             'postal_code' => $request['postal_code'],
             'country' => $request['country'],
             'locality' => $request['locality'],
             'status' => 1,
       );

        if($id > 0 ) {
             
        }else{
             $Add_data['userID'] =  $request['owner'];
        }

        //shipping data
         $ship_data =  array(
             // 'userID' => $user,
             'name' => $request['s_name'],
             'email' => $request['s_email'],
             'phone' => $request['s_mob'],
             'address' => $request['s_address'],
             'street' => $request['s_street'],
             'region' => $request['s_region'],
             'city' => $request['s_city'],
             'state' => $request['s_state'],
             'postal_code' => $request['s_postal_code'],
             'country' => $request['s_country'],
             'locality' => $request['s_locality'],
             'status' => 1,
       );

         if($id > 0 ) {
             
        }else{
             $Add_data['userID'] =  $request['owner'];
        }

            //store billing address 
            $add = address::create($Add_data);
            $store_data['billing_address'] = $add->addressID;

            //store shipping address
         if($request['s_address'] == "no"){

                $ship = address::create($ship_data);
                 $store_data['shipping_address'] = $ship->addressID;

             }else{
                $store_data['shipping_address'] = $add->addressID;

            }

        //  Check for featured image
        if( $request->fileToUpload ) {

            $store_data['fileID'] = Helpers::post_file($request->fileToUpload);
 
        }

        if( $id > 0 ) {
            $store = store::find( $id );
            $store->update( $store_data );
            $msg = 'store updated successfully';
        }else {
            $store = store::create( $store_data );
            $team_data['storeID'] = $store->storeID;
            team::create( $team_data);
            $msg = 'store created successfully';
        }

        return redirect( '/admin/stores' )->with('success', $msg );

   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $store=store::find($id);
        $store->update( ['status' => 2 ,] );
        $team = team::where('storeID',$id);
        $team->delete();
         return redirect("/admin/stores")
     ->with('success','Store Deleted successfully');
    }
}


