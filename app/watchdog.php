<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class watchdog extends Model
{
    const CREATED_AT = 'created';
    public $timestamps = false;

    protected $primaryKey = 'logID';

    public $table = 'watchdog';

     protected $fillable = [
		'userID',
		'referenceID',
		'storeID',
		'module',
		'message',
		'status',
	];
}
