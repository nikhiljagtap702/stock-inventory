<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_product extends Model {
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
    
	public $timestamps = false;

	protected $primaryKey = 'orderpID';
    
    protected $fillable = [
   		'orderID',
   		'productID',
      'storeID',
   		'brandID',
   		'title',
   		'quantity',
      'brand_name',
   		'price',
      'tax',
      'cess',
      'hsn',
      'image',
   		'amount',
   	];
}
