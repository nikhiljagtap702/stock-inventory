@extends('layouts/admin')

@section('title','Order page')

@section('content')
<style type="text/css">
	.btn-qty {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        height: 2rem;
        width: 2rem;
    }
</style>
    <form class="form" name="form" action="{{url('/admin/store/cart/update/'.$data->orderID.'/') }}" method="POST">
        @csrf
         @method('PUT')

        <div class="card shadow mb-4">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-4">
                        <h6 class="m-0 font-weight-bold text-primary">Edit Cart</h6>
                    </div>
                    <div class="col-4 mx-auto text-center">
                        ₹<span id="orderTotal"><?php echo $data->amount; ?></span>
                        <input type="hidden" name="orderTotal" id="orderTotal1" value="<?php echo $data->amount; ?>">
                    </div>
                    <div class="col-4 text-right">
                        <button type="submit" class="btn btn-sm btn-success">
                            <i class="fas fa-save mr-1"></i>
                            <span>Confirm</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-border table-hover table-sm" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col">Product</th>
                                <!-- <th scope="col">Container</th> -->
                                <th scope="col">Brand</th>                      
                                <th scope="col">Pieces/Box</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($products as $pro)
                            <tr>

                             <td>
                                <input type="hidden" name="storeID" value="{{ $pro['storeID'] }}">
                                <img class="img-fluid mr-2" src="{{ $pro['image'] }}" height="32" width="auto"  style="height: 1.5rem;width: auto;">
                                <input type="hidden" name="image[]" value="{{ $pro['image'] }}">
                                {{ $pro['title'] ?? '' }}
                                <input type="hidden" name="title[]" value="{{ $pro['title'] }}">
                                <input type="hidden" name="product_id[]" value="{{ $pro['productID'] }}">
                            </td>


                             <td>{{ $pro['brand_name'] ?? ( $pro['category'] ?? '' ) }}</td>
                                <input type="hidden" name="brandName[]" value=" {{ $pro['brand_name'] ?? ( $pro['category'] ?? '' ) }}">
                                <input type="hidden" name="brand_id[]" value=" {{ $pro['brandID'] }}">

                             <td>{{ $pro['quantity'] ?? 0 }}</td>
                                <input type="hidden" name="pieces[]" value="{{ $pro['quantity'] ?? 0 }}">

                            <td>{{ $pro['price'] ?? 0 }}</td>
                                <input type="hidden" name="Price[]" value=" {{ $pro['price'] ?? 0 }}">

                            <td class="qty">
                                    <button type="button" class="btn btn-sm btn-primary rounded-circle btn-qty minus" onclick="updateQuantity( 'remove', this, event )" data-product="{{ json_encode($pro,true) ?? '' }}">-</button>
                                    <span class="quantity-value">{{ $pro['quantity']}}</span>
                                    <input type="hidden" name="quantity[]" class="quantity-value1" value="{{ $pro['quantity']}}">                                    
                                    <button type="button" class="btn btn-sm btn-primary rounded-circle btn-qty plus" onclick="updateQuantity( 'add', this, event )" data-product="{{ json_encode($pro,true) ?? '' }}">+</button>
                            </td>

                            <td class="total">{{ $pro['amount']}}</td> 
                                <input type="hidden" name="total[]" class="total1" value="{{ $pro['amount']}}"> 

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </form>    
<script type="text/javascript">
    var order = {};
    order.quantity = 0;
    order.total = <?php echo $data->amount; ?>;
    order.products = [];

    function updateQuantity( action, el, ev ) {
        ev.preventDefault();
        var row = el.parentElement.parentElement;
        var product = JSON.parse( ev.target.dataset.product ) || {};
        var qty = parseInt( row.querySelector( '.quantity-value1' ).value ) || 0;
        if( action === 'remove' ) {
            if( qty > 0 ) { 
                qty--;
                order.quantity--;
                order.total -= parseFloat( product.price );
            }
        }else {
            qty++;
            order.quantity++;
            order.total += parseFloat( product.price );
        }

        //  Update values
        row.querySelector( '.quantity-value' ).innerHTML = qty;
        row.querySelector( '.total' ).innerHTML = qty*product.price;
        row.querySelector( '.total1' ).value = qty*product.price;
        row.querySelector( '.quantity-value1' ).value = qty;        
        document.querySelector( '#orderTotal' ).innerHTML = order.total;
        document.querySelector( '#orderTotal1' ).value = order.total;
    }
</script>
@endsection
