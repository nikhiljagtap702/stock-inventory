@extends('layouts/admin')

@section('title','Dashboard')

@section('content')
<!-- main content start-->
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<h1 class="h2">Dashboard</h1>
</div>
<?php if (Auth::check() && Auth::User()->role == 2) { ?>
    <div class="row">
        <div class="col-md-4">
            <a  href="{{ url('/admin/orders/1')}}">
                <div class="card-counter order">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="count-numbers">{{ $new_order }}</span>
                    <span class="count-name">New Orders</span>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a  href="{{ url('/admin/orders/2')}}">
                <div class="card-counter primary">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="count-numbers">{{ $total_order }}</span>
                    <span class="count-name">Total Orders</span>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a  href="{{ url('/admin/stores') }}">
                <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers">{{ $total_clients }}</span>
                    <span class="count-name">Stores</span>
                </div>
            </a>
        </div>
    </div>
  <?php }else{ ?> 
<div class="row">
	<div class="col-md-4">
    	<a  href="{{ url('/admin/orders/1')}}">
    		<div class="card-counter order">
    			<i class="bi bi-cart4"></i>
    			<span class="count-numbers">{{ $new_order }}</span>
    			<span class="count-name">New Orders</span>
    		</div>
    	</a>
    </div>
    <div class="col-md-4">
    	 <?php if (Auth::User()->role == 1) { ?>
                <a  href="{{ url('/admin/orders')}}">
            <?php }else{ ?>
                <a  href="{{ url('/admin/orders/2')}}">
            <?php } ?>
    		<div class="card-counter primary">
          <i class="bi bi-cart4"></i>
    			<span class="count-numbers">{{ $total_order }}</span>
    			<span class="count-name">Total Orders</span>
    		</div>
    	</a>
    </div>
    <div class="col-md-4">
    	<a  href="{{ url('/admin/products') }}">
    		<div class="card-counter danger">
    			<i class="bi bi-box-seam"></i>
    			<span class="count-numbers">{{ $total_products }}</span>
    			<span class="count-name">Products</span>
    		</div>
		</a>
     </div>
 </div>
 <div class="row">
     <div class="col-md-4">
     	<a  href="{{ url('/admin/brands')}}">
     		<div class="card-counter success">
     			 <i class="bi bi-tags"></i>
     			<span class="count-numbers">{{ $total_brands }}</span>
     			<span class="count-name">Brands</span>
     		</div>
		</a>
    </div>
    <div class="col-md-4">
    	<a  href="{{ url('/admin/supplier') }}">
    		<div class="card-counter suppliers">
    			<i class="bi bi-truck"></i>
    			<span class="count-numbers">{{ $total_suppliers }}</span>
    			<span class="count-name">Suppliers</span>
    		</div>
    	</a>
    </div>
    <div class="col-md-4">
    	<a  href="{{ url('/admin/stores') }}">
    		<div class="card-counter info">
    			<i class="bi bi-people"></i>
    			<span class="count-numbers">{{ $total_clients }}</span>
    			<span class="count-name">Stores</span>
    		</div>
    	</a>
    </div>
</div>
<?php  } ?>
@endsection
@section('stylesheets')
<style type="text/css">
	.card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
    position: relative;
  }

  .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .card-counter.primary{
    background-color: #007bff;
    color: #FFF;
  }

  .card-counter.order{
    background-color: #a400ffe3;
    color: #FFF;
  }

  .card-counter.danger{
    background-color: #ef5350;
    color: #FFF;
  }  

  .card-counter.success{
    background-color: #66bb6a;
    color: #FFF;
  }  

  .card-counter.info{
    background-color: #26c6da;
    color: #FFF;
  }

  .card-counter.suppliers{
    background-color: #dab826;
    color: #FFF;
  }  

  .card-counter i{
    font-size: 5rem;
    opacity: 0.2;
    position: absolute;
    left: .8rem;
    top: -.9rem;
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }
</style>
@endsection
