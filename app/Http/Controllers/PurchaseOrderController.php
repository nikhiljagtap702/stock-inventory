<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\purchase_order;
use App\product;
use App\purchase_product;
use App\cart;


class PurchaseOrderController extends Controller
{
    public function index($id)
    {
        $supplier = $id;
        $data = purchase_order::latest()->where('referenceID',$id)->join('users','purchase_orders.referenceID','=','users.userID')->select('purchase_orders.*','users.name')->get();
        return view('admin.purchaseOrders',compact('data','supplier') );
        
    }

    
    public function create($id)
    {
    	$supplier = $id;
        $cart = cart::where(['userID' => Auth::User()->userID, 'status' => '0'])->count();
    	$data = product::latest()->leftjoin('brands','products.brandID', '=', 'brands.brandID')->leftjoin('files','products.fileID', '=' , 'files.fileID')->select('products.*','brands.title as brandName','files.uri')->get();
        return view('admin.create-order', compact('data','supplier','cart'));
    }


    public function add_cart(Request $request)
    {
        $data = array('userID' => Auth::User()->userID,
                       'productID' => $request->productID,
                       'supplierID' => $request->supplierID,
                       'status'  => '0'
                   );
        cart::create($data);

        $cart = cart::where(['userID' => Auth::User()->userID, 'status' => '0'])->count();
        $supplier = $request->supplierID;
        $products = cart::leftjoin('products','carts.productID','=', 'products.productID')->leftjoin('files','products.fileID', '=' , 'files.fileID')->where(['carts.userID' => Auth::User()->userID, 'carts.status' => '0'])->select('products.*','carts.cartID','files.uri')->get();

        return view('admin.manage-cart', compact('products','supplier','cart'));


    
    }

    public function carts($id)
    {
        $supplier = $id;
        $cart = cart::where(['userID' => Auth::User()->userID, 'status' => '0'])->count();
        $products = cart::leftjoin('products','carts.productID','=', 'products.productID')->leftjoin('files','products.fileID', '=' , 'files.fileID')->where(['carts.userID' => Auth::User()->userID, 'carts.status' => '0'])->select('products.*','carts.cartID','files.uri')->get();
        return view('admin.manage-cart', compact('products','supplier','cart'));


    
    }

   
    public function store(Request $request)
    {
        // echo $supplier =  $request['supplier'];
        // die();
        $response = [];
        $quantity = 0;
        $total_price = 0;
        $ip= 0;
        $store = array(
            'referenceID' => $request['supplier'],
            'userID'      => \Auth::user()->userID,
            //'products' =>json_encode($response),
        );
        $order = purchase_order::create($store);
        for ($i=0; $i < count($request->get('title')) ; $i++) {
                    $res['title']      =  $request['title'][$i];
                    $res['product_id'] =  $request['productID'][$i];
                    $res['image']      =  $request['image'][$i];
                    $res['hsn']        =  $request['hsn'][$i];
                    $res['brandID']   =  $request['brandID'][$i];
                    $res['price']      =  $request['Price'][$i];
                    $res['quantity']   =  $request['quantity'][$i];
                    // $res['total']      =  $request['total'][$i];
                    array_push($response, $res);
                    $quantity += $res['quantity'];
                    $total_price += $res['price']*$res['quantity'] ;
                    //store product data;
                    $product_data = array(
                        'orderID'    => $order->orderID,
                        'title'      => $res['title'],
                        'productID'  => $res['product_id'], 
                        'brandID'    => $res['brandID'],
                        'quantity'   => $res['quantity'],
                        'price'      => $res['price'],
                        'hsn'        => $res['hsn'],
                        'image'      => $res['image'],
                        'amount'     => $res['price'] * $res['quantity'],
                        'status'     => '1',
                    );
                    $order_product =  purchase_product::create($product_data);
                    $product = product::find($res['product_id']);
                    $product->update(array('max_price' => $res['price'],
                                            'price'    => $res['price'],
                                            'stock'    => $product->stock + $res['quantity'], ));
                    
                }
                // echo $total_price;
                // die(); 
                $order = purchase_order::find($order->orderID);
                $order_data= array(
                    'items' => $quantity,
                    'amount' => $total_price,
                    // 'products' =>json_encode($response),
                    'status' => '1'
                );
                $order->update($order_data);
                return redirect('admin/purchase/orders/'.$store['referenceID']. '/');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $carts = cart::find($id);
        $supplier = $carts->supplierID;
        $carts->update(array('status' => '4', ));
        $cart = cart::where(['userID' => Auth::User()->userID, 'status' => '0'])->count();
        $products = cart::leftjoin('products','carts.productID','=', 'products.productID')->leftjoin('files','products.fileID', '=' , 'files.fileID')->where(['carts.userID' => Auth::User()->userID, 'carts.status' => '0'])->select('products.*','carts.cartID','files.uri')->get();
         return view('admin.manage-cart', compact('products','supplier','cart'));

    }
}
