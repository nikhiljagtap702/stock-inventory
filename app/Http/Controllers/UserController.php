<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Image;
use App\file;
use App\store;
use App\order;
use App\team;
use Helpers;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

     $data = User::latest()->leftjoin('files','users.fileID','=','files.fileID')->select('users.*','files.uri')->whereNotIn('users.role',[5,3])->get();
      return view('admin.users', compact('data'))
        ->with('i');
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $user = collect([
            'userID' => 0,
            'name' => '',
            'full_name' => '',
            'gender' => 0,
        ]);
         $in_states = url(asset('/public/data/state.json'));
         $states =  json_decode(file_get_contents($in_states), true);

        
        
        return view( 'admin.user-manage', compact('user','states') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
     {
        $this->validate($request, [
                'email'  => 'required|unique:users',
                'mobile' => 'required|digits:10|unique:users',
                'password_confirmation' => 'required_with:password|same:password',
                'dob'    =>'required|date|date_format:Y-m-d|before:tomorrow'
        ]);

        $user_data = array(
            'name'      => $request['name'],
            'full_name' => $request['full_name'],
            'mobile'    => $request['mobile'],
            'dob'       => $request['dob'],
            'company'   => $request['company'],
            'position'  => $request['position'],
            'email'     => $request['email'],
            'website'   => $request['website'],
            'address'   => $request['address'],
            'state'     => $request['state'],
            'city'      => $request['city'],
            'gender'    => $request['gender'],
            'zipcode'   => $request['zipcode'],
            'password' => Hash::make($request['password']),
            'role'      => $request['role'],
            'status'    => 1,
        );

        $User = User::create( $user_data );
        $msg = 'user created successfully';

        return redirect( '/admin/users' )->with('success', $msg );
        
    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $userID )
    {
        $data = User::find($userID);
        $file = file::find($data->fileID);
     

         $store = team::latest('team.created')
                ->join( 'stores AS s', 'team.storeID', '=', 's.storeID' )
                ->select( 's.storeID', 's.fileID', 's.slug', 's.title', 's.description' )
                ->where( 'team.userID', '=', $userID )
                ->orderBy( 's.title', 'ASC')
                ->get();
        
        $order = order::where('userID',$data->userID)->count();


        return view( 'admin.profile',compact('data','file','store','order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $userID ) {
         
         $user = User::find($userID);
        
         $file = file::where('fileID',$user->fileID)->first();

        $store = team::latest('team.created')
                ->join( 'stores AS s', 'team.storeID', '=', 's.storeID' )
                ->where( 'team.userID', '=', $userID )
                ->count();

         $in_states = url(asset('/public/data/state.json'));
         $states =  json_decode(file_get_contents($in_states), true);

        
        return view( 'admin.user-manage' , compact('user','file','states','store') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $userID ) {  

        $this->validate($request, [
            'dob'      => 'required|date|date_format:Y-m-d|before:tomorrow',
            'mobile'   => 'required|digits:10'

        ]);
        

        $user_data = array(
            'name'      => $request['name'],
            'full_name' => $request['full_name'],
            'mobile'    => $request['mobile'],
            'dob'       => $request['dob'],
            'company'   => $request['company'],
            'position'  => $request['position'],
            'email'     => $request['email'],
            'website'   => $request['website'],
            'address'   => $request['address'],
            'state'     => $request['state'],
            'city'      => $request['city'],
            'gender'    => $request['gender'],
            'zipcode'   => $request['zipcode'],
            // 'password' => Hash::make($request['password']),
            'role'      => $request['role'],
            'status'    => 1,
        );

        if (User::where('email', $request['email'])->whereNotIn('userID',[$userID])->exists()) {

            return redirect()->back()->with( 'error', 'Email ID already exists' );
        }
        if (User::where('mobile', $request['mobile'])->whereNotIn('userID',[$userID])->exists()) {

            return redirect()->back()->with( 'error', 'Mobile number already exists' );
        
        }

        //  Check for featured image
        if( $request->fileToUpload ) {

            $user_data['fileID'] = Helpers::post_file($request->fileToUpload); 
        }
        $User = User::find($userID);
        $User->update( $user_data );
        $msg = 'User updated successfully';
       

        return redirect( '/admin/users' )->with('success', $msg );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $userID ) {
        $user = user::find( $userID );
        $user->delete();
        return redirect('/admin/users')->with( 'success', 'User deleted successfully' );
    }

  
}
