<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\categories;
use App\file;
use Helpers;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=categories::latest()->leftjoin('files','categories.fileID','=','files.fileID')->select('categories.*','files.uri')->get();
        return view("admin.categories",compact('data'))->with('i');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/category-manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=categories::where('categoryID',$id)->leftjoin('files','categories.fileID','=','files.fileID')->select('categories.*','files.uri')->first();
        return view("admin.category-manage",compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
        $category_data=array(
            'title' => $request['title'],
            'description' => $request['description'],
            'status' => 1,
        ); 

        //  Check for featured image
        if( $request->fileToUpload ) {
            
           $category_data['fileID'] = Helpers::post_file($request->fileToUpload);
           
        }

        if( $id > 0 ) {

            $categories = DB::table('categories')->where('categories.categoryID', $id)->update($category_data);
            $msg = 'Category updated successfully';
        }else {
            $categories = categories::latest()->get();
            foreach ($categories as $value) {
                if (strtolower($request['title']) == strtolower($value->title)) {

                    return redirect()->back()->with('error', 'The categorie has already exist.');
                   
                }
                
            }
             categories::create( $category_data );
            $msg = 'Category created successfully';
        }
        return redirect( '/admin/categories' )->with('success', $msg );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = DB::table('categories')->where('categories.categoryID', $id)->delete();
        return redirect("/admin/categories")->with('success','Category Deleted successfully');
    }
}
