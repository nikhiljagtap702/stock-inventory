<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\brand;
use App\categories;
use App\file;
use App\store_products;
use Validator;
use DB;
use Helpers;



/**
 * @group Products
 *
 * APIs for managing products
 */
class ProductController extends Controller {
  //  Default API status code
 // public $apiStatusCode = 200;
    
    /** 
     * Get products data 
     * 
     * @queryParam storeID int optional Unique store ID Example: 1
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_products( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'show_all'    => [],  
             'storeID'    => [],
             'page'       => [],
             'limit'      => [],

        ]);
        
        if( !$valid->fails() ) {

            $page  = $request->page * 10;
            $limit  = $request->limit;

            if(isset($request['storeID']) && !empty($request['storeID'])) {

                $products = product::latest()->leftjoin('store_products','products.productID', '=' ,'store_products.productID')->where('store_products.storeID', '=',$request['storeID'])->where('products.max_price','>', 0)->select('products.*','store_products.price as sprice','store_products.storeID')->get();
        
            }elseif ($request->page) {
              
                $products = product::latest()->where('max_price','>', 0)->paginate($page);
            }else{

                $products = product::latest()->where('max_price','>', 0)->paginate($limit);
            }

            if( isset( $products ) && !empty( $products ) ) {            
                $response['status'] = 'success';
                
                foreach( $products as $item ) {
                    $res = array();
                    // $res['storeID'] = $item['storeID'];
                    $res['productID']  = $item['productID'];
                    $res['brandID']    = $item['brandID'];
                    $res['categoryID'] = $item['category'];
                    $res['sku']        = $item['sku'];               
                    $res['title']      = $item['title'];
                    $res['description']= $item['description'];
                    $res['box']        = $item['box'];
                    $res['pieces']     = intval( $item['pieces'] );
                    

                    if(isset($item['sprice']) && !empty($item['sprice'])){
                        $res['price']      = floatval($item['sprice']);
                    }else{
                        $res['price']      = floatval( $item['price']);
                    }
                    $res['max_price']  = floatval( $item['max_price'] );
                    

                    if ($item['max_price'] > 0) {
                      $res['discount']   = round(($item['max_price']-$item['price'])/$item['max_price']*100);
                       $res['saving']    = $item['max_price']* $res['discount'] /100;
                      $res['discountFormatted']=  $res['discount'].'% Off';
                    }else{
                      $res['discount'] = $item['discount'];
                      $res['discountFormatted'] =  $res['discount'].'% Off';
                      $res['saving']    = $item['discount'];

                    }
                    $res['tax']        = floatval( $item['tax'] );
                    $res['cess']       = floatval( $item['cess'] );
                    $tax =  ( $res['tax'] +  $res['cess'] );
                    $res['taxAmount']  = ($item['price']  * $tax  ) / 100;

                    $res['weight']     = floatval( $item['weight'] );
                    $res['unit']       = $item['unit'];

                    $res['rating']     = $item['rating'];
                    $res['hsn']        = $item['hsn'];
                    $res['fileID']     = $item['fileID'];
                    $res['piecesFormatted']= 'Set of '.$item['pieces'];
                    $res['marginFormatted']= $item['margin'];
                    $res['max_boxes']  = 0;
                    if( isset( $item['stock'] ) && !empty( $item['stock'] ) && isset( $item['pieces'] ) && !empty( $item['pieces'] ) ) {
                        $res['max_boxes'] = $item['stock']/$item['pieces']*0.75;
                    }
                    
                    if( $res['max_boxes'] > 0 ){
                        $res['in_stock'] = 'true';
                    }else{
                        $res['in_stock'] ='false';
                    }
                    // $res['fileName']   = $item['fileName'];

                    $file = file::find($item['fileID']);

                   if( !isset( $item['directory'] ) || empty( $item['directory'] )) {
                        $item['directory'] = 'public/uploads';
                    }

                    if(isset( $file ) || !empty( $file )) {
                        $res['fileUrl']    = url( $item['directory'] . '/' . $file['uri'] );
                        // $res['avatar']     = url( $item['directory'] . '/thumbnail/' . $file['uri'] );
                        $res['avatar']     = url( $item['directory'] . '/' . $file['uri'] );
                    }else{
                        $res['fileUrl'] = url( $item['directory'] . '/default-product.png');
                        // $res['avatar']     = url( $item['directory'] . '/thumbnail/default-product.png');
                        $res['avatar']     = url( $item['directory'] . '/default-product.png');
                    }
                    /*$res['file'] = array(
                        'fileID'    => $item['fileID'],
                        'title'     => $item['fileName'],
                        'uri'       => $item['uri'],
                        'url'       => url( $item['directory'] . '/' . $item['uri'] ),
                        'thumbnail' => url( $item['directory'] . '/thumbnail/' . $item['uri'] ),
                    );*/

                    $res['brandName'] = '';
                    $res['brandAvatar'] = url( 'public/media/default-product.png' );
                    if( isset( $item['brandID'] ) && !empty( $item['brandID'] )) {
                        $res['brandName'] = $item['category'];
                        $res['brandAvatar'] = url( 'public/media/default-product.png' );
                    }

                    // $res['categories'] = array();
                    // $res['categories'][] = array( 'categoryID' => 0, 'title' => $item['type'] );

                    // $res['category']   = $item['category'];
                    // $res['type']       = $item['type'];
                    // $res['body']=$item['body'];
                    // $res['margin']=$item['margin'];
                    // $res['discount']=$item['discount'];
                    // $res['stock']=$item['stock'];
                    // $res['sales']=$item['sales'];
                    // $res['status']=$item['status'];                
                    // $res['created']=$item['created'];
                    // $res['updated']=$item['updated'];
                    // array_push($val,$res);
                    ksort( $res );
                   
                    $response['data'][] = $res;
                }
            }
        }
              
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Get product details
     * 
     * @response {
     *      "data": [],
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_product( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'productID' => ['required'],
        ], [
            'productID.required' => 'Please enter a product ID'
        ]);
        
        if( !$valid->fails() ) {

            $product = product::find($request->productID );
            if( $product ) {
                $tax = floatval( $product['tax'] +  $product['cess'] );
                $product->taxAmount     =  ( $product->price  * $tax  ) / 100;
                if($product->fileID > 0) {

                    $file = file::find($product->fileID);
                    $product['fileUrl'] = url( 'public/media/'.$product->fileID );

                }else{

                    $product->fileUrl   =  url( 'public/media/default-product.png');
                    $product->avatar    =  url('public/media/thumbnail/default-product.png');

                }

                $response['data'] = $product;
                $response['status'] = 'success';
                $response['messages'][] = array( 'message' => 'Product does exists', 'type' => 'success' );
            }else{
                $response['messages'][] = array( 'message' => 'Product does not exist', 'type' => 'error' );
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
     /** 
     * Add a product
     * 
     * @bodyParam userID int required Unique user ID Example: 1234
     * @bodyParam brandID int required brand ID Example: 12
     * @bodyParam title string required  title Example: xyz
     * @bodyParam sku string optional sku  Example: xyz
     * @bodyParam category string optional category  Example: xyz
     * @bodyParam category string optional category  Example: xyz
     * @bodyParam description string optional description  Example: xyz
     * @bodyParam body string optional body  Example: xyz
     * @bodyParam min_price string optional min_price  Example: xyz
     * @bodyParam max_price string optional max_price  Example: xyz
     * @bodyParam weight string optional weight  Example: xyz
     * @bodyParam unit string optional unit  Example: xyz
     * @bodyParam box string optional box  Example: xyz
     * @bodyParam tax int optional tax  Example: 10
     * @bodyParam cess int optional cess  Example: 5
     * @bodyParam margin int optional margin  Example: 50
     * @bodyParam discount int optional discount  Example: 150
     * @bodyParam quantity int optional quantity  Example: 50
     * @bodyParam rating int optional rating  Example: 1
     * @bodyParam stock int optional stock  Example: 150
     * @bodyParam sales int optional sales  Example: 150
     * @bodyParam fileToUpload  optional fileToUpload  Example: xyz.jpg
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     *
     */
    
    public function post_product( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'userID'        => ['required'],
            'brandID'       => ['required'],
            'title'         => ['required'],
            'sku'           => [],
            'category'      => [],
            'type'          => [],
            'description'   => [],
            'body'          => [],            
            'min_price'     => [],
            'max_price'     => [],
            'weight'        => [],
            'unit'          => [],
            'box'           => [],
            'tax'           => [],
            'cess'          => [],
            'margin'        => [],
            'discount'      => [],
            'quantity'      => [],
            'rating'        => [],
            'stock'         => [],
            'sales'         => [],
            'fileToUpload'  => [],
        ], [
            'title.required' => 'Please enter product title',
            'brandID.required' => 'Please enter a brand ID'
        ]);

        if( !$valid->fails() ) {

            $Product = product::create([
                'sku'           => $request->input('sku'),
                'category'      => $request->input('category'),
                'type'          => $request->input('type'),
                'title'         => $request->input('title'),
                'description'   => $request->input('description'),
                'body'          => $request->input('body'),
                'box'           => $request->input('box'),
                'min_price'     => $request->input('min_price'),
                'max_price'     => $request->input('max_price'),
                'weight'        => $request->input('weight'),
                'unit'          => $request->input('unit'),
                'tax'           => $request->input('tax'),
                'cess'          => $request->input('cess'),
                'margin'        => $request->input('margin'),
                'discount'      => $request->input('discount'),
                'quantity'      => $request->input('quantity'),
                'rating'        => $request->input('rating'),
                'stock'         => $request->input('stock'),
                'sales'         => $request->input('sales'), 
            ]);

            $uploadsDirectory = 'public/uploads';

            if( $Product ) {

                if ($request->has('fileToUpload')) {

                    $imgUrl = $request->get('fileToUpload');
                    //$fileName = array_pop(explode('.', $imgUrl));
                    $type = substr($imgUrl, 5, strpos($imgUrl, ';')-5);
                    $type = explode('/',$type);
                    $type = $type[1];
                    $fileName = time().".".$type;
                    $image = file_get_contents($imgUrl);

                    $destinationPath = base_path() . '/' . $uploadsDirectory . '/' . $fileName;

                    file_put_contents($destinationPath, $image);
                   // $attributes['image'] = $fileName;
                }

                $form_data =array(
                    'fileName'  => $fileName,
                    'directory' => $uploadsDirectory,
                    'userID'    => $request->userID,
                );

                if($file=file::create($form_data)){
                    $Product= Product::find($Product->productID);
                    $Product->fileID = $file->id;
                    $Product->save();

                    $response['data'] = $Product;
                    $response['status'] = 'success';
                    $response['messages'][] = array( 'message' => 'Product created successfully', 'type' => 'success' );
                }else{
                     $response['messages'][] = array( 'message' => 'Product does not created', 'type' => 'error' );
                }
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Update product data 
     * 
     * @bodyParam productID int required product ID Example: 1234
     * @bodyParam brandID int required brand ID Example: 12
     * @bodyParam title string required  title Example: xyz
     * @bodyParam sku string optional sku  Example: xyz
     * @bodyParam category string optional category  Example: xyz
     * @bodyParam category string optional category  Example: xyz
     * @bodyParam description string optional description  Example: xyz
     * @bodyParam body string optional body  Example: xyz
     * @bodyParam min_price string optional min_price  Example: xyz
     * @bodyParam max_price string optional max_price  Example: xyz
     * @bodyParam weight string optional weight  Example: xyz
     * @bodyParam unit string optional unit  Example: xyz
     * @bodyParam box string optional box  Example: xyz
     * @bodyParam tax int optional tax  Example: 10
     * @bodyParam cess int optional cess  Example: 5
     * @bodyParam margin int optional margin  Example: 50
     * @bodyParam discount int optional discount  Example: 150
     * @bodyParam quantity int optional quantity  Example: 50
     * @bodyParam rating int optional rating  Example: 1
     * @bodyParam stock int optional stock  Example: 150
     * @bodyParam sales int optional sales  Example: 150
     * @bodyParam fileToUpload  optional fileToUpload  Example: xyz.jpg
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     */
    public function put_product( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'productID'     => ['required'],
            'brandID'       => ['required'],
            'title'         => ['required'],
            'sku'           => [],
            'category'      => [],
            'type'          => [],
            'description'   => [],
            'body'          => [],            
            'min_price'     => [],
            'max_price'     => [],
            'weight'        => [],
            'unit'          => [],
            'box'           => [],
            'tax'           => [],
            'margin'        => [],
            'discount'      => [],
            'quantity'      => [],
            'rating'        => [],
            'stock'         => [],
            'sales'         => [],
            'fileToUpload'  => [],
        ], [
            'productID.required' => 'Please enter a product ID'
        ]);

        if( !$valid->fails() ) {

            $Pro = product::find($request->productID);
            //echo $Pro->fileID;
            $fileID = $Pro->fileID;

            //echo $fileID;

            if ($request->has('fileToUpload')) {

                $imgUrl = $request->get('fileToUpload');
                //$fileName = array_pop(explode('.', $imgUrl));
                $type = substr($imgUrl, 5, strpos($imgUrl, ';')-5);
                $type = explode("/",$type);
                $type = $type[1];
                $fileName = time().".".$type;
                $image = file_get_contents($imgUrl);

                $destinationPath = base_path() . '/public/uploads/' . $fileName;

                file_put_contents($destinationPath, $image);
                // $attributes['image'] = $fileName;
            }

            $file= file::where('fileID',$fileID);
            $data = ['fileName' => $fileName];
            $file->update($data);
            // $file->save();

            $Product = product::where( 'productID', $request->productID );
            $data = array(
                'sku'         => $request->sku,
                'category'    => $request->category,
                'type'        => $request->type,
                'title'       => $request->title,
                'description' => $request->description,
                'body'        => $request->body,
                'box'         => $request->box,
                'min_price'   => $request->min_price,
                'max_price'   => $request->max_price,
                'weight'      => $request->weight,
                'unit'        => $request->unit,
                'tax'         => $request->tax,
                'cess'        => $request->cess,
                'margin'      => $request->margin,
                'discount'    => $request->discount,
                'quantity'    => $request->quantity,
                'rating'      => $request->rating,
                'stock'       => $request->stock,
                'sales'       => $request->sales,
            );                    

            $Product->update($data);

            $response['data'] = $Product;
            $response['status'] = 'success';
            $response['messages'][] = array( 'message' => 'Product updated successfully', 'type' => 'success' );
         }else{
            $response['messages'][] = array( 'message' => 'Unable to update product', 'type' => 'error' );
         }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
     /** 
     * Delete product 
     * 
     * @bodyParam productID int required product ID Example: 1234
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     */

    public function delete_product( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'productID' => ['required'],
        ], [
            'productID.required' => 'Please enter a product ID'
        ]);

        if( !$valid->fails() ) {
            if( $product = product::find( $request->productID ) ) {
                $response['status'] = 'success';
                $product->delete();

                $response['messages'][] = array( 'message' => 'Product Deleted successfull', 'type' => 'success' );
            }else{
                $response['messages'][] = array( 'message' => 'Product ID  does not exist', 'type' => 'success' );
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Filter products 
     * 
     * @bodyParam userID int optional user ID Example: 1234
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     */

    public function filter_products( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'userID' => [],
        ]);

        if( !$valid->fails() ) {
            $response['status'] = 'success';

            //  Filter by brands
            $response['data']['brands'] = array();
            $response['data']['brands']['title'] = 'Brands';
            $response['data']['brands']['slug']  = 'brands';
            $response['data']['brands']['options'] = array();

            //  Get brands
            $brands = brand::leftjoin('files','brands.fileID','=','files.fileID')->where('brands.status',1)->select('brands.brandID','brands.fileID','brands.title','files.uri')->get();
            if( isset( $brands ) && !empty( $brands ) ) {
                foreach( $brands as $item) {
                    $res = array();
                    $res['brandID'] = $item['brandID'];
                    $res['fileID']  = $item['fileID'];
                    $res['title']   = $item['title'];
                    if( !isset( $item['directory'] ) || empty( $item['directory'] )) {
                        $item['directory'] = 'public/uploads';
                    }
                    if (isset($item['uri']) && !empty($item['uri'])) {
                        $res['fileUrl']    = url( $item['directory'] . '/' . $item['uri'] );
                        $res['avatar']     = url( $item['directory'] . '/thumbnail/' . $item['uri'] );
                    }else{
                          $res['fileUrl'] = url( $item['directory'] . '/default-product.png');
                          $res['avatar']  = url( $item['directory'] . '/thumbnail/default-product.png');
                    }
                    $response['data']['brands']['options'][] = $res;
                }
                
            }

            //  Filter by category
            $response['data']['categories']['title'] = 'Category';
            $response['data']['categories']['slug']  = 'categories';

            $categories = categories::leftjoin('files','categories.fileID','=','files.fileID')->where('categories.status',1)->select('categories.categoryID','categories.title','categories.fileID','files.uri')->get();
            if( isset( $categories ) && !empty( $categories ) ) {
                foreach( $categories as $item) {
                    $res = array();
                    $res['categoryID'] = $item['categoryID'];
                    $res['fileID']  = $item['fileID'];
                    $res['title']   = $item['title'];
                    if( !isset( $item['directory'] ) || empty( $item['directory'] )) {
                        $item['directory'] = 'public/uploads';
                    }
                    if (isset($item['uri']) && !empty($item['uri'])) {
                        $res['fileUrl']    = url( $item['directory'] . '/' . $item['uri'] );
                        // $res['avatar']     = url( $item['directory'] . '/thumbnail/' . $item['uri'] );
                        $res['avatar']     = url( $item['directory'] . '/' . $item['uri'] );
                    }else{
                          $res['fileUrl'] = url( $item['directory'] . '/default-product.png');
                          // $res['avatar']  = url( $item['directory'] . '/thumbnail/default-product.png');
                          $res['avatar']  = url( $item['directory'] . '/default-product.png');
                    }
                    $response['data']['categories']['options'][] = $res;
                }
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }
        return $response;
    }

     /** 
     * Post file 
     * 
     * @bodyParam fileToUpload int required file To Upload  Example: xyz.jpj
     * 
     * @response {
     *      "data":{},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function file_upload( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'fileToUpload'    => ['required']
        
        ]);
        
        if( !$valid->fails() ) {
            $response['status'] = 'success';
            $response['data']   = Helpers::post_file($request->fileToUpload);
            $response['messages'][] = array( 'message' => 'file has been uploaded ', 'type' => 'success' );
           
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
}

  