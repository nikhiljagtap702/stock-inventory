@extends('layouts/admin')
<?php if( isset( $sproduct->ID ) && !empty( $sproduct->ID ) ) :?>
    @section('title','Manage Product Price') 
<?php else :?>
    @section('title','Add Product') 
<?php endif;?>
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            <?php if( isset( $sproduct->ID ) && !empty( $sproduct->ID ) ) :?>
                Edit Product Price
            <?php else :?>
                Add a Product
            <?php endif;?>
        </h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <?php if( isset( $sproduct->ID ) && !empty( $sproduct->ID ) ) :?>
                <form method="post" action="{{url('/admin/store/product/update/'.$sproduct->ID.'/') }}" > 
                    @csrf
                    @method('PUT')
                <?php else :?>
                <form method="post"  action="{{ url('/admin/store/product/store') }}">
                    @csrf
                <?php endif;?>
                <input type="hidden" name="storeID"  value="<?php echo Request::segment(5); ?>">
                <input type="hidden" name="store"  value="{{ $sproduct->storeID ?? '' }}">
                <input type="hidden" name="max_price" value="{{ $sproduct->max_price ?? ''}}" >
                <?php if( !isset($sproduct->ID) || empty($sproduct->ID)): ?>
                <div class="form-group">
                    <label>Product<span class="text-danger">*</span></label><br/>
                    <select class="form-select" multiple aria-label="multiple select example" name="product[]" onchange="updateProduct( this );" required="required">
                        <option value="" readonly disabled selected>-- Select  Product --</option>
                        @foreach($product as $k => $value)
                        <option value="{{ $value->productID ?? '' }}">{{ $value->title ?? '' }}</option>
                        @endforeach
                    </select>
                    @error('product')
                        <span class="text-danger" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <?php endif; ?>
               <!--  <div class="form-group">
                    <label>Price</label>
                    <input type="number" name="value" class="form-control" value="{{ old('value') ?? $sproduct->price ?? '' }}" required>
                    @error('value')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @if(session('error'))
                    <strong class="text-danger">{{session('error')}}</strong>
                    @endif
                </div> -->
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                        <i class="bi bi-file-earmark-plus"></i>
                        <span>Save</span>
                    </button>
                    <?php if( isset( $sproduct->ID ) && !empty( $sproduct->ID ) ) :?>
                    <a class="btn btn-outline-secondary" href="{{ url('/admin/store/product/'.$sproduct->storeID) }}" role="button" title="Go back">
                        <i class="bi bi-caret-left"></i>
                        <span>Back</span>
                    </a>
                    <?php else :?>
                    <a class="btn btn-outline-secondary" href="{{ url('/admin/store/product/'.Request::segment(5)) }}" role="button" title="Go back">
                        <i class="bi bi-caret-left"></i>
                        <span>Back</span>
                    </a>  
                    <?php endif;?>
                </div>
            </form>
        </div>
       <!--  <div class="col-md-6">
            <h6>Product Details</h6>
                <table class="table table-sm table-hover table-bordered">
                    <tr>
                        <th width="120">Title</th>
                        <td id="product-title">{{$sproduct->title ?? ''}}</td>
                    </tr>
                    <tr>
                        <th width="120">Category</th>
                        <td id="product-category">{{$sproduct->category ?? ''}}</td>
                    </tr>
                    <tr>
                        <th width="120">Retail Price</th>
                        <td id="product-max_price">{{$sproduct->max_price ?? ''}}</td>
                    </tr>
                    <tr>
                        <th width="120">Sale Price</th>
                        <td id="product-price">{{$sproduct->p_price ?? ''}}</td>
                    </tr>
                </table>
            </div>
        </div> -->
    </div>
    <?php if( isset( $sproduct->ID ) && !empty( $sproduct->ID ) ) :?>
    <div class="card-body pt-0">
        <form action="{{url('/admin/store/product/delete/'.$sproduct->ID.'/') }}" method="post">
            <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are you sure you want to delete this Store-Product?');" title="Delete">
                <i class="bi bi-trash"></i>
                <span>Delete this store Product</span>
            </button>
            @csrf
            @method('DELETE')
        </form>          
    </div>
    <?php endif;?>
</div>
<?php if( !isset($sproduct->ID) || empty($sproduct->ID)): ?>
<script type="text/javascript">
function updateProduct( el, ev ) {
    // ev.preventDefault();
    var product = JSON.parse( el[parseInt(el.value)].dataset.product ) || {};
    document.getElementById( 'product-title').innerHTML = product.title;
    document.getElementById( 'product-category').innerHTML = product.category;
    document.getElementById( 'product-max_price').innerHTML = product.max_price;
    document.getElementById( 'product-price').innerHTML = product.price;
}    
</script>
<?php endif;?>
@endsection
