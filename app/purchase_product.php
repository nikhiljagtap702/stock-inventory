<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase_product extends Model
{
	const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

	protected $primaryKey = 'purchaseID';
    
    protected $fillable = [
   		'orderID',
   		'productID',
   		'brandID',
   		'title',
   		'quantity',
   		'price',
        'hsn',
        'image',
   		'amount',
   		'status'
   	];
}
