@extends('layouts/admin')

@section('title', 'Store Products')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row align-items-center">
            	<div class="col-md-6">
                     <h6 class="m-0 font-weight-bold text-primary">PRODUCTS</h6>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ url('/admin/store/product/add/'.Request::segment(4).'/') }}" class="btn btn-primary"><i class="bi bi-file-earmark-plus"></i> ADD PRODUCT</a>
                    <a href="{{ url('/admin/store/products/edit/'.Request::segment(4).'/') }}" class="btn btn-primary"><i class="bi bi-pencil-square"></i> EDIT PRODUCTS</a>
                </div>
            </div>
        </div>
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
       @endif
       <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">M.R.P</th>
                            <th scope="col">Sale Price</th>
                            <th scope="col">Store Price</th>
                            <th scope="col">Stock Unit</th>
                            <th scope="col">Sold</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $store)
                        <tr class="active"> 
                            <td scope="row">{{ ++$i }}</td> 
                            <td>
                                <?php if($store->fileID > 0){?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/'.$store->uri )}}" height="32" width="auto" style="height: 2rem;">
                                <?php }else{ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}" height="32" width="auto" style="height: 2rem;">
                                <?php  } ?>
                                {{ $store->producttitle }}
                            </td> 
                            <td>{{ $store->max_price ?? '' }}</td>
                            <td>{{ $store->sale_price ?? '' }}</td>
                            <td>{{ $store->price  ?? ''}}</td>
                            <td>{{ $store->stock ?? '' }}</td>
                            <td>{{ $store->sales ?? '' }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : []
        });
    });
</script>
@endsection