@extends('layouts/admin')

@section('title','Notification')

@section('content')
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Notification</h6>
      </div>
     <div class="card-body">
      <div class="form-row">
         <div class="form-group col-md-4 mx-auto">
         	<table>
            <tr>
              <td><strong>storeID</strong></td><td>{{ $data->storeID}}</td>
            </tr>
            <tr>
              <td><strong>userID</strong></td><td>{{ $data->userID}}</td>
            </tr>
         		<tr>
         			<td><strong>Module</strong></td><td>{{ $data->module}}</td>
         		</tr>
         		<tr>
         			<td><strong>Message</strong></td><td>{{$data->message}}</td>
         		</tr>
         	</table>
         </div>
       </div>
     </form>
   </div>
 </div>
@endsection