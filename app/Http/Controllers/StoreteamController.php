<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\team;
use App\store;
use App\User;

class StoreteamController extends Controller{
    
    public function show($id){
        $data = team::latest()->join('users','team.userID', '=', 'users.userID')->join('stores','team.storeID', '=' ,'stores.storeID')->select('users.name','team.*','stores.title','stores.userID as owner')->where('team.storeID', '=' ,$id)->get();
         return view('admin.teams',compact('data'))
         ->with('i');

        }


    public function add($id){
        
        
        // $user = user::where('role','=',2)
        //     ->whereNotExists( function ($query) {
        //     $query->select('team.storeID')
        //     ->from('team')
        //     ->whereRaw('users.userID = team.userID')
        //     ->where('team.storeID', '=' ,request()->segment(4) );
        //   })->get();
       return view('admin.add-team');
    }



    public function store(Request $request ){
        if($request->userID){
            $team_data=array(
                'userID' => $request['userID'],
                'storeID' => $request['storeID'],
                'role' => $request['role'],
             ); 
            $data = team::create($team_data);
            $msg = 'Team Member Added Successfully';
            $id =  $request['storeID'];
            return redirect( '/admin/store/team/'.$id )->with('success', $msg );
        }else{
            if (User::where('mobile', '=', $request->mobile)->exists() || User::where('mobile', '=', $request->mobile)->exists()) {
                $user = User::where('mobile', '=', $request->mobile)->first();
                $storeID = $request->storeID;
                if (team::where(['userID' => $user->userID,'storeID' => $storeID] )->exists()) {
                   // $data =  team::where('storeID',$storeID)->where('userID',$user->userID)->first();
                    return redirect()->back()->with('error', 'Your are  already associated.');
                }else{
                    // echo "user doesn't exist";
                    return view('admin.team-manage',compact('storeID','user'));
                }
                
            }else{
                return redirect()->back()->with('error', 'Your mobile Number does not matches. Please try again.');
            }
        }
    }

    public function edit($id){
        $data = team::find($id);
        $user = User::find($data->userID);
       return view('admin.team-manage',compact('data','user'));
    }


    public function update(Request $request,$id){

		$team_data=array(
            'storeID' => $request['store'],
            'role' => $request['role'],
             ); 
		
		$team = team::find($id);
       	$team->update($team_data);
         $msg = 'Team Updated successfully ';

        $id =  $request['store'];
        return redirect( '/admin/store/team/'.$id )->with('success', $msg );
    }

    public function destroy($id){
        
        $team = team::find($id);
        $team->delete();
        $store = $team->storeID;


        return redirect('/admin/store/team/'.$store)
        ->with('success','Team Deleted successfully');
    }

}
