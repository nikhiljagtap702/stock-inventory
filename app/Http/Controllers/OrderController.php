<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\User;
use App\team;
use Helpers;
use PDF;
use Auth;
use DB;
use Mail;
use Carbon\Carbon;
use App\watchdog;
use App\address;
use App\brand;
use App\store;
use App\product;
use App\session;
use Notifications;
use App\store_products;
use App\order_payment;
use App\order_product;

if ( !class_exists('NumbersToWords') ){
    /**
    * NumbersToWords
    */
    class NumbersToWords {
        public static $hyphen      = '-';
        public static $conjunction = ' and ';
        public static $separator   = ', ';
        public static $negative    = 'negative ';
        public static $decimal     = ' point ';
        public static $dictionary  = array(
          0                   => 'zero',
          1                   => 'one',
          2                   => 'two',
          3                   => 'three',
          4                   => 'four',
          5                   => 'five',
          6                   => 'six',
          7                   => 'seven',
          8                   => 'eight',
          9                   => 'nine',
          10                  => 'ten',
          11                  => 'eleven',
          12                  => 'twelve',
          13                  => 'thirteen',
          14                  => 'fourteen',
          15                  => 'fifteen',
          16                  => 'sixteen',
          17                  => 'seventeen',
          18                  => 'eighteen',
          19                  => 'nineteen',
          20                  => 'twenty',
          30                  => 'thirty',
          40                  => 'fourty',
          50                  => 'fifty',
          60                  => 'sixty',
          70                  => 'seventy',
          80                  => 'eighty',
          90                  => 'ninety',
          100                 => 'hundred',
          1000                => 'thousand',
          1000000             => 'million',
          1000000000          => 'billion',
          1000000000000       => 'trillion',
          1000000000000000    => 'quadrillion',
          1000000000000000000 => 'quintillion'
        );

        public static function convert($number){
          if (!is_numeric($number) ) return false;
          $string = '';
          switch (true) {
            case $number < 21:
                $tens   = ((int) ($number / 1)) * 1;
                $units  = $number % 1;
                $string = self::$dictionary[$tens];
                if ($units) {
                    $string .= self::$hyphen . self::$dictionary[$units];
                }
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = self::$dictionary[$tens];
                if ($units) {
                    $string .= self::$hyphen . self::$dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = self::$dictionary[$hundreds] . ' ' . self::$dictionary[100];
                if ($remainder) {
                    $string .= self::$conjunction . self::convert($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = self::convert($numBaseUnits) . ' ' . self::$dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? self::$conjunction : self::$separator;
                    $string .= self::convert($remainder);
                }
                break;
          }
          return $string;
        }
    }
}

class OrderController extends Controller {

    public function generatePDF($id) {

        $data = order::find($id);
        $store = store::find($data->storeID);
        if (isset($store->billing_address) && !empty($store->billing_address)) {
            $store_add = address::find($store->billing_address);
        }else{
            $store_add = '';
        }
        
        $watchdog = watchdog::where('referenceID',$id)->select('status')->get();
        $watchd = [];

        foreach ($watchdog as $wd) {
            array_push($watchd,$wd->status);
        }

        $order_product = order_product::where('orderID',$id)->get();
        if (isset($data->shipping_address) && !empty($data->shipping_address)) {
            $ship = address::find($data->shipping_address);
        }else{
            $ship = '';

        }

        if (isset($data->billing_address) && !empty($data->billing_address)) {
            $bill = address::find($data->billing_address);
        }else{
            $bill = '';

        }
        
        // $bill = address::find($data->billing_address);
        $data['taxes'] = $data['tax'] + $data['cess'];
        $total = $data['amount'];

        $data['com'] = ( $data['amount']*$data['taxes'] / 100 );

        $data['sub_total'] = $data['amount'];
        $data['amountInWords'] = ucwords( NumbersToWords::convert( $data['sub_total'] ) );
        // echo IntToEnglish::Int2Eng($sub_total);

        // return view('admin.order-pdf',['data' => $data,'ship' => $ship,'bill' => $bill,'watchd' => $watchd,'order_product' => $order_product,'store' => $store,'store_add' => $store_add ]);
        $pdf = PDF::loadView('admin.order-pdf',['data' => $data,'ship' => $ship,'bill' => $bill,'watchd' => $watchd,'order_product' => $order_product,'store' => $store,'store_add' => $store_add ]);
        return $pdf->download( $data->orderID . '.pdf' );
    }

    public function show($id){
        if (Auth::check() && Auth::User()->role == 2) {
            $data = order::latest()->join('users', 'orders.userID','=','users.userID')->where(['orders.storeID' => $id,'orders.userID' => Auth::User()->userID ])->select('orders.*','users.name')->paginate(25);
        }else{
            $data = order::latest()->join('users', 'orders.userID','=','users.userID')->where('orders.storeID',$id)->select('orders.*','users.name')->paginate(25);

        }
        return view("admin.carts",compact('data'))->with('i', (request()->input('page',1) -1)*25);
    }

    public function add($id){
        $data = store_products::where('storeID',$id)->join('products', 'store_products.productID', '=', 'products.productID')->leftjoin('brands','products.brandID', '=', 'brands.brandID')->select('products.*','brands.title as brandName', 'store_products.price as productstore')->get();
        return view('admin.addcart',compact('data'));
    }

    public function transactions(){
        $data = order_payment::latest()->get();
        return view("admin.transactions",compact('data'))->with('i');
    }
    public function transaction_update(){
        // $data = order_payment::latest()->get();
        // return view("admin.transactions",compact('data'))->with('i');
        echo "success";
    }


    public function store(Request $request){
        //one month old pending payment orders
        // $seoScoreLastMonth = order::where('created', '>=', Carbon::now()->subMonth())->where('status','4')->count();

        $response = []; 
        $ip= 0;
        $orderTotal = 0;
        $total_tax = 0;
        $store = $request['storeID'];
        $detail = store::find($store);
        $product_order = [];
        $store=array(
            'storeID' => $request['storeID'],
            'userID' => \Auth::user()->userID,
            'billing_address' => $detail->billing_address,
            'shipping_address' => $detail->shipping_address,
            // 'products' =>json_encode($response),
            // 'items' => $ip,
            // 'amount' => $orderTotal,
        );
        $order = order::create($store);
        for ($i=0; $i < count($request->get('title')) ; $i++) {
            if(isset($request['quantity'][$i]) && $request['quantity'][$i] > 0):
            //print_r($request['product_id']);
                
                $product = product::find($request['product_id'][$i]);
                $tax =  $product['tax'] + $product['cess'];
                if($product['stock'] >= $request['quantity'][$i]){
                    $res['title'] =  $request['title'][$i];
                    $res['product_id'] =  $request['product_id'][$i];
                    $res['image'] =  $request['image'][$i];
                    $res['box'] =  $request['box'][$i];
                    $res['brandName'] =  $request['brandName'][$i];
                    $res['brand_id'] =  $request['brand_id'][$i];
                    $res['pieces'] =  $request['pieces'][$i];
                    
                    $res['quantity'] =  $request['quantity'][$i];
                    $res['tax']      =  $product['tax'];
                    $res['cess']     =  $product['cess'];
                    $res['hsn']      =  $product['hsn'];
                    $res['price']    =  $request['Price'][$i];
                    $res['product_taxt']    =  ($request['total'][$i])*$product['tax'] / 100;
                    $res['total']    =  $request['total'][$i];
                    array_push($response, $res);
                    $orderTotal += $res['total'];
                    //store product data;
                    $product_data = array(
                        'orderID'    => $order->orderID,
                        'storeID'    => $request->storeID,
                        'title'      => $res['title'],
                        'productID'  => $res['product_id'], 
                        'brandID'    => $res['brand_id'],
                        'brand_name' => $res['brandName'],
                        'quantity'   => $res['quantity'],
                        'price'      => $res['price']/('1.'.$tax ),
                        'tax'        => $res['tax'],
                        'cess'       => $res['cess'],
                        'hsn'        => $res['hsn'],
                        'image'      => $res['image'],
                        'amount'     => $res['total'], 
                    );
                    // echo $res['tax'];
                    // die();
                    $order_product =  order_product::create($product_data);
                    $product_order = order_product::where('orderID',$order_product->orderID)->get();
                    $data = array(
                        'stock' => $product['stock']- $request['quantity'][$i],
                        'sales' => $product['sales'] + $request['quantity'][$i],
                    );
                    $product->update($data);
                    $total_tax += $tax;

                   // // add brand sale quantity
                   // $brand = brand::find($request['brand_id'][$i]);

                   //    $brand_data = array(
                   //     'sales' => $brand['sales'] + $request['quantity'][$i],
                   // );
                   //    $brand->update($brand_data);
                }
            endif;
            $ip+=$request['quantity'][$i];
        }

        $order =order::find($order->orderID);
        $order_data=array(
            'items' => $ip,
            'amount' => $orderTotal,
            'tax'    => $total_tax,
        );
        $order->update($order_data);
        return view('admin.revieworder',compact('order','orderTotal','store','product_order'));
    }

    public function edit($id){
        $data = order::find($id);
        $products = order_product::where('orderID',$id)->get(); 
        return view('admin.editcart',compact('data','products'));
    }

    public function update(Request $request,$id){
        $response = [];
        $orderTotal = 0;
        $ip= 0;
        $total_tax = 0;
        $product_order = [];
        order_product::where('orderID',$id)->delete();
        for ($i=0; $i < count($request->get('title')) ; $i++) { 
            if(isset($request['quantity'][$i]) && $request['quantity'][$i] > 0):
                $product = product::find($request['product_id'][$i]);
                $tax =  $product['tax'] + $product['cess'];
                if($product['stock'] >= $request['quantity'][$i]){
                    $res['title'] =  $request['title'][$i];
                    $res['product_id'] =  $request['product_id'][$i];
                    $res['image'] =  $request['image'][$i];
                    $res['box'] =  $request['box'][$i];
                    $res['brandName'] =  $request['brandName'][$i];
                    $res['brand_id'] =  $request['brand_id'][$i];
                    $res['pieces'] =  $request['pieces'][$i];
                    $res['quantity'] =  $request['quantity'][$i];
                    $res['tax']      =  $product['tax'];
                    $res['cess']     =  $product['cess'];
                    $res['hsn']      =  $product['hsn'];
                    $res['price']    =  $request['Price'][$i];
                    $res['product_taxt']    =  ($request['total'][$i])*$product['tax'] / 100;
                    $res['total']    =  $request['total'][$i];
                    array_push($response, $res);

                    $product_data = array(
                        'orderID'    => $id,
                        'storeID'    => $request->storeID,
                        'title'      => $res['title'],
                        'productID'  => $res['product_id'], 
                        'brandID'    => $res['brand_id'],
                        'brand_name' => $res['brandName'],
                        'quantity'   => $res['quantity'],
                        'price'      => $res['price']/('1.'.$tax ),
                        'tax'        => $res['tax'],
                        'cess'       => $res['cess'],
                        'hsn'        => $res['hsn'],
                        'image'      => $res['image'],
                        'amount'     => $res['total'], 
                    );
                    $order_product =  order_product::create($product_data);
                    $product_order = order_product::where('orderID',$id)->get();
                    $data = array(
                        'stock' => $product['stock']- $request['quantity'][$i],
                        'sales' =>$product['sales'] + $request['quantity'][$i],
                    );
                    $product->update($data);
                     // add brand sale quantity
                    $brand = brand::find($request['brand_id'][$i]);
                    $brand_data = array(
                        'sales' => $brand['sales'] + $request['quantity'][$i],
                    );
                    $brand->update($brand_data);
                    $orderTotal += $res['total'];
                    $total_tax += $tax;
                }
            endif;
            $ip+=$request['quantity'][$i];
        }
        $order = order::find($id);
        $order_data=array(
        // 'products' =>json_encode($response),
            'items' => $ip,
            'amount' => $orderTotal,
            'tax'    => $total_tax,
        );
        $order->update($order_data);
        $order = order::find($id);
        return view('admin.revieworder')->with(compact('order','orderTotal','product_order'));
    }

    public function place(Request $request,$id){
        $order_data =array(
            'status'=> 4,
        );
        $order = order::find($id );
        $order->update($order_data);
        $store = $order->storeID;
        $device = array();
        $user = User::find($order->userID);

        //send notification user
        $devices['userID']      = $order->userID;
        $devices['storeID']     = $order->storeID;
        $devices['referenceID'] = $order->orderID ;
        $devices['senderID']    = $order->userID;
        $devices['title']       = "order #".$id;
        $devices['message']     = $user->name. " has generated a new order #".$id;
        $devices['body']        = $user->name. " has generated a new order #".$id;
        $devices['module']      = "orders";
        post_notification($devices);


        // //send notification  Admin
        // $users = User::latest()->where('role',1);

        // foreach ($users as $value) {
        //     $devices['userID']      = $value->userID;
        //     $devices['storeID']     = $order->storeID;
        //     $devices['referenceID'] = $order->orderID ;
        //     $devices['senderID']    = $order->userID;
        //     $devices['title']       = "order #".$id;
        //     $devices['message']     = $user->name." has generated a new order #".$id;
        //     $devices['body']        = $user->name." has generated a new order #".$id;
        //     $devices['module']      = "orders";
        //     post_notification($devices);
     
        // }

        

        // $email= $user['email'];
        // $sms = 'xyz';
        // $to_name = 'Supplyport';
        // $to_email = $email;
        // $data = array('name'=>"Cloudways (sender_name)", "body" => "tast mail");
        // Mail::send('admin.mail', $data, function($message) use ($to_name, $to_email) {
        //     $message->to($to_email, $to_name)
        //     ->subject('');
        //     $message->from('donotreply@supplyport.com','Supply Port');
        // });
        // foreach ($users as $mail) {
        //     $data = array('name'=>"Cloudways (sender_name)", "body" => "tast mail");
        //     Mail::send('admin.mail', $data, function($message) use ($to_name, $mail) {
        //          $message->to($mail['email'], $to_name)
        //          ->subject('Supplyport order');
        //          $message->from('donotreply@supplyport.com','Supply Port');
        //    });

        //     // echo $mail['email'];

      
        //send mails 

      //   $data = array('name'=>"Supply Port");
      //   Mail::send(['text'=>'admin.mail'], $data, function($message) {
      //   $message->to( $email , 'Tutorials Point')->subject
      //       ('Laravel Basic Testing Mail');
      //   $message->from('donotreply@supplyport.com','Supply Port');
      // });
        return redirect( '/admin/store/cart/'.$store )->with('success','Order Placed successfully');
    }

    public function destroy($id){
        $order = order::find($id );
        $order->delete();
        order_product::where('orderID',$id)->delete();
        $store = $order->storeID;
        return redirect( '/admin/store/cart/'.$store )->with('success','Order Deleted successfully');
    }
}
