<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';

	protected $primaryKey = 'productID';

	protected $fillable = [
		'sku',
		'originalTitle',
		'fileID',
		'brandID',
		'category',
		'type',
		'title',
		'description',
		'body',
		'box',
		'price',
		'max_price',
		'weight',
		'unit',
		'tax',
		'margin',
		'hsn',
		'cess',
		'discount',
		'quantity',
		'rating',
		'stock',
		'pieces',
		'sales'
   ];
}
