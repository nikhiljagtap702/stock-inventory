<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\offer;
use Image;
use App\file;
use App\store;
use Helpers;

class OfferController extends Controller
{
    
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data=offer::latest()->leftjoin('files','offers.fileID','=','files.fileID')->select('offers.*','files.uri')->get();
        return view("admin.offers",compact('data'))->with('i');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = store::latest()->get();
        return view('admin/offer-manage',compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = offer::find($id);
        echo "<pre>";
        print_r($data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=offer::where('offerID',$id)->leftjoin('files','offers.fileID','=','files.fileID')->select('offers.*','files.uri')->first();
        return view("admin.offer-manage",compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         $offer_data=array(
            'title' => $request['title'],
            'description' => $request['description'],
            'body' => $request['Body'],
            'status' => 1,
        ); 

        //  Check for featured image
        if( $request->fileToUpload ) {

            $offer_data['fileID'] = Helpers::post_file($request->fileToUpload);

        }

        if( $id > 0 ) {
            $offer = offer::find( $id );
            $offer->update( $offer_data );
            $msg = 'Offer updated successfully';
        }else {

            $store = $request->store;
            for ($i=0; $i < count($store) ; $i++) {
                $offer_data['stores'] =  $store[$i];
                $Offer = offer::create( $offer_data );
            }

            $msg = 'Offer created successfully';
        }

        return redirect( '/admin/offers' )->with('success', $msg );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = offer::find($id);
        $offer->delete();
    return redirect("/admin/offers")
     ->with('success','offer Deleted successfully');
    }

}
