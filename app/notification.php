<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notification extends Model
{
   const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';


	protected $primaryKey = 'noteID';
    
    protected $fillable = [
   		'userID',
   		'senderID',
   		'message',
   		'referenceID',
   		'storeID',
   		'module',
      'title',
      'body',
      'status'
   	];
}
