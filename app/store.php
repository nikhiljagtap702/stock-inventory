<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class store extends Model {
   	
   const CREATED_AT = 'created';
   const UPDATED_AT = 'updated';

	protected $primaryKey = 'storeID';
    
   protected $fillable = [
   		'userID',
         'storeID',
   		'fileID',
   		'slug',
   		'title',
   		'description',
   		'tagline',
   		'keywords',
   		'image',
   		'domain',
         'gstin',
         'restaurant_name',
   		'favicon',
         'billing_address',
         'shipping_address',
         'payment_status',
         'credit_period',
         'order_limit',
         'status',
   ];
}
