<?php

use Illuminate\Support\Facades\Route;
// use App\order_product;
// use App\order;
use App\product;
use App\User;
use App\brand;
use App\store;
use App\address;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
// Route::get('/test', function () {
//       Mail::to('mangallatwal@gmail.com')->send(new TestAmazonSes('It works!'));
// });
Route::get('contact', 'feedbackController@index');
Route::get( 'terms', 'PageController@terms' );
Route::get( 'privacy', 'PageController@privacy' );
Route::resource('/admin/notifications','NotificationController');

Route::get('sendbasicemail','MailController@basic_email');

 Route::get('/admin/feedbacks','FeedbackController@index');
 Route::post('/admin/feedbacks/store','FeedbackController@store');
 Route::get('/admin/feedback/edit','FeedbackController@edit');
 Route::get('/admin/feedback/show/{id}','FeedbackController@show');
 Route::get('/admin/feedbacks/{type?}','FeedbackController@index');
Route::resource('admin/contact/registers', 'ContactController');

//  Public pages
Auth::routes();
Route::get('home/sendemail', 'HomeController@sendemail');

Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
Route::group(['middleware' => 'admin'], function() {
    Route::resource('/admin/users','UserController');
    Route::resource('/admin/users','UserController');
    
    Route::resource('/admin/brands','BrandController');
    Route::resource('/admin/offers','OfferController');
    Route::get('/admin/analytics','AnalyticController@index');
    Route::get('/admin/analytics/{compare}', 'AnalyticController@index');
    Route::resource('/admin/products','ProductController');
    Route::resource('/admin/store/owner','StoreownerController');
    // Route::resource('/admin/store/team','TeamController');
    Route::get('/form','PageController@form');
});
Route::resource('/admin/user/password/change','PasswordController');
Route::resource('/admin/profile','ProfileController');
Route::resource('/admin/stores','StoreController');
Route::resource('/admin/categories','CategoryController');
//cart
Route::get('/admin/store/cart/{id}','OrderController@show');
Route::get('/admin/transactions','OrderController@transactions');
Route::get('/admin/store/cart/add/{id}','OrderController@add');
Route::post('/admin/store/cart/store','OrderController@store');
Route::get('/admin/store/cart/edit/{id}','OrderController@edit');
Route::put('/admin/store/cart/update/{id}','OrderController@update');
Route::get('/admin/store/cart/update/place/{id}','OrderController@place');
Route::delete('/admin/store/cart/delete/{id}','OrderController@destroy');
//Team
Route::get('/admin/store/team/{id}','StoreteamController@show');
Route::get('/admin/store/add/{id}','StoreteamController@add');
Route::post('/admin/store/store','StoreteamController@store');
Route::get('/admin/store/edit/{id}','StoreteamController@edit');
Route::put('/admin/store/update/{id}','StoreteamController@update');
Route::delete('/admin/store/delete/{id}','StoreteamController@destroy');
//Store Product
Route::get('/admin/store/product/{id}','StoreproductController@show');
Route::get('/admin/store/product/add/{id}','StoreproductController@add');
Route::post('/admin/store/product/store','StoreproductController@store');
Route::get('/admin/store/product/edit/{id}','StoreproductController@edit');
Route::put('/admin/store/product/update/{id}','StoreproductController@update');
Route::delete('/admin/store/product/delete/{id}','StoreproductController@destroy');
Route::get('/admin/store/products/edit/{id}','StoreproductController@edit_products');
//Order
Route::get('/admin/orders/','ShippingController@view');
Route::get('/admin/orders/{id}','ShippingController@view');
Route::get('/admin/orders/view/{id}','ShippingController@show');
Route::get('/admin/orders/shipping/view/{id}','ShippingController@show');
Route::get('/admin/orders/create/{id}','ShippingController@create');
Route::get('/admin/orders/billing/{id}','ShippingController@billing');
Route::put('/admin/orders/updatebilling/{id}','ShippingController@billingupdated');
Route::post('/admin/orders/store','ShippingController@store');
Route::delete('/admin/orders/delete/{id}','ShippingController@destroy');

//purchase orders
Route::get('/admin/purchase/orders/{id}','PurchaseOrderController@index');
Route::get('/admin/purchase/order/products/{id}','PurchaseOrderController@create');
Route::post('/admin/purchase/cart/','PurchaseOrderController@add_cart');
Route::get('/admin/purchase/carts/{id}','PurchaseOrderController@carts');
Route::get('/admin/purchase/delete/{id}','PurchaseOrderController@destroy');
Route::post('/admin/purchase/order/store','PurchaseOrderController@store');

Route::get('admin/order/pdf/{id}','OrderController@generatePDF');



Route::get('/admin/stores/{compare}/{id}','StoreController@view');
Route::post('/home', 'HomeController@index');

Route::resource('/admin/supplier','SupplierController');
Route::resource('/admin/employees','EmploEmployeeController');
Route::resource('/admin/clients','ClientController');




Route::get('admin/import', function() {

    $filepath =  url( asset( '/public/data/productss.csv' ) );
    $file = fopen( $filepath, 'r' );
    $importData_arr = array();
    $i = 0;
    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
        $num = count($filedata );
        // Skip first row (Remove below comment if you want to skip the first row)
        if($i == 0){
            $i++;
            continue;
        }
        for ($c=0; $c < $num; $c++) {
            $importData_arr[$i][] = $filedata [$c];
        }
        $i++;
    }
    fclose($file);

    // Insert to MySQL database
    foreach($importData_arr as $importData){


         $insertData = array(
            'fileID'    => $importData[1],
            'brandID'   => $importData[2],
            'category'  => $importData[4],
            'title'     => $importData[7],
            'pieces'    => $importData[10],
            'brandID'   => $importData[2],
            'box'       => $importData[11],
            'price'     => $importData[12],
            'weight'    => $importData[14],
            'unit'      => $importData[15],
            'hsn'       => $importData[16],
            'tax'       => $importData[17],
            'cess'      => $importData[18],
            'stock'     => $importData[22],
            'status'    => 1,
        );
         if ($importData[13] == 0) {
               $insertData['max_price'] = $importData[12];
            }else{
               $insertData['max_price'] = $importData[13];
            }
        // insert products data

        product::create($insertData);
       

        // Redirect to index
        // print_r($insertData);
    }
    
});
Route::get('admin/orders/insert', function() {
    //  TRUNCATE `orders`;TRUNCATE `order_products`;
    $page = 1;
    if( isset( $_REQUEST['page'] ) && !empty( $_REQUEST['page'] ) ) {
        $page = intval( $_REQUEST['page'] );
    }
    $limit = 2000;
    $offset = ($page - 1) * $limit;

    $ordersJson =  url(asset('/public/data/orders.json'));

    $orders = file_get_contents( $ordersJson, true );
    if( isset( $orders ) && !empty( $orders ) ) {
        $orders = json_decode( $orders, true );
        if( count( $orders ) > $limit ) {
            $orders = array_splice( $orders, $offset, $limit );
        }
        foreach( $orders as $key => $order ) {
            $order['orderID']  = ( $key + 1 );
            $order['products'] = json_encode( $order['products'] );

            //  Generate order
            // $order['orderID'] = DB::table( 'orders' )->insertGetId( $order );

            if( isset( $order['products'] ) && !empty( $order['products'] ) ) {
                $order['products'] = json_decode( $order['products'], true );
                foreach ( $order['products'] as $k => $product ) {
                    // $product['orderID'] = $order['orderID'];
                    // DB::table( 'order_products' )->insertGetId( $product );
                }
            }

            print_r( 'Inserted order #' . $order['orderID'] . PHP_EOL );
        }
    }
    exit;
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

if(!Auth::user())
{
return redirect('/login'); // add your desire URL in redirect function
}
