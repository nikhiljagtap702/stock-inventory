@extends('layouts/admin')

@section('title','Feedback page')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Feedback</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-4 mx-auto">
                <table>
             		<tr>
             			<td><strong>Name: </strong></td><td>{{ $data->name}}</td>
             		</tr>
             		<tr>
             			<td><strong>Email: </strong></td><td>{{$data->email}}</td>
             		</tr>
                    <tr>
                        <td><strong>Mobile: </strong></td><td>{{$data->mobile}}</td>
                    </tr>
                    <tr>
                        <td><strong>Store Name: </strong></td><td>{{$data->company_name}}</td>
                    </tr>
             		<tr>
             			<td><strong>Subject:</strong></td><td>{{$data->subject}}</td>
             		</tr>
             		<tr>
             			<td><strong>Message:</strong></td><td>{{$data->message}}</td>
             		</tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection