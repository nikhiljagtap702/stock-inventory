@extends('layouts/admin')

@section('title','Suppliers')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h6 class="m-0 font-weight-bold text-primary">Suppliers</h6>
                </div>
                <div class="col-md-6 text-right">
                    <!-- <a href="" class="btn btn-primary">+ADD USER</a> -->
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <span>{{ $message }}</span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped table-bordered table-hover" width="100%" cellspacing="0" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <!-- <th scope="col">Email</th> -->
                            <th scope="col">Joined</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $value)
                        <tr>
                            <td scope="row">{{ $value->userID }}</td>
                            <td>
                                <a href="{{ route('users.show', $value->userID ) }}">
                                <?php if($value->fileID > 0){ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/'.$value->uri )}}" height="32" width="auto" style="height: 2rem;">
                                <?php }else{ ?>
                                    <img class="img-fluid" src="{{ asset('public/uploads/user.jpg')}}" height="32" width="auto" style="height: 2rem;">
                               <?php  } ?>
                               <?php echo ucfirst($value->name); ?></a>
                            </td>
                            <!-- <td>{{ $value->email }}</td> -->
                            <td><?php echo  date('d M Y',strtotime($value->created)); ?></td>
                            <td>
                                 <a href="{{ url('admin/purchase/orders/'.$value->userID .'/') }}" class="btn btn-primary"><i class="bi bi-bag"></i> Orders</a>
                                <!-- <a href="{{ url('/admin/purchase/order/products/'.$value->userID .'/') }}" class="btn btn-primary"><i class="bi bi-cart-plus"></i> Orders</a> -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.card-body -->
    </div><!-- /.card -->
@endsection
@section('stylesheets')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable({
          'order' : [],
          "pageLength": 25
        });
    });
</script>
@endsection