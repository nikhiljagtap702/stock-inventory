<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use SujalPatel\IntToEnglish\IntToEnglish;
use PDF;
use App\order;
use App\watchdog;
use App\address;
use App\order_product;
use App\store;

if ( !class_exists('NumbersToWords') ){
    /**
    * NumbersToWords
    */
    class NumbersToWords {
        public static $hyphen      = '-';
        public static $conjunction = ' and ';
        public static $separator   = ', ';
        public static $negative    = 'negative ';
        public static $decimal     = ' point ';
        public static $dictionary  = array(
          0                   => 'zero',
          1                   => 'one',
          2                   => 'two',
          3                   => 'three',
          4                   => 'four',
          5                   => 'five',
          6                   => 'six',
          7                   => 'seven',
          8                   => 'eight',
          9                   => 'nine',
          10                  => 'ten',
          11                  => 'eleven',
          12                  => 'twelve',
          13                  => 'thirteen',
          14                  => 'fourteen',
          15                  => 'fifteen',
          16                  => 'sixteen',
          17                  => 'seventeen',
          18                  => 'eighteen',
          19                  => 'nineteen',
          20                  => 'twenty',
          30                  => 'thirty',
          40                  => 'fourty',
          50                  => 'fifty',
          60                  => 'sixty',
          70                  => 'seventy',
          80                  => 'eighty',
          90                  => 'ninety',
          100                 => 'hundred',
          1000                => 'thousand',
          1000000             => 'million',
          1000000000          => 'billion',
          1000000000000       => 'trillion',
          1000000000000000    => 'quadrillion',
          1000000000000000000 => 'quintillion'
        );

        public static function convert($number){
          if (!is_numeric($number) ) return false;
          $string = '';
          switch (true) {
            case $number < 21:
                $string = self::$dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = self::$dictionary[$tens];
                if ($units) {
                    $string .= self::$hyphen . self::$dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = self::$dictionary[$hundreds] . ' ' . self::$dictionary[100];
                if ($remainder) {
                    $string .= self::$conjunction . self::convert($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = self::convert($numBaseUnits) . ' ' . self::$dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? self::$conjunction : self::$separator;
                    $string .= self::convert($remainder);
                }
                break;
          }
          return $string;
        }
    }
}

class PDFController extends Controller {
    // public function generatePDF($id) {
    // 	$data = order::find($id);
    //     $store = store::find($data->storeID);
    //     $store_add = address::find($store->billing_address);
	   //  $watchdog = watchdog::where('referenceID',$id)->select('status')->get();
	   //  $watchd = [];
	   //  foreach ($watchdog as $wd) {
	   //    array_push($watchd,$wd->status);
	   //  }
	   //  $order_product = order_product::where('orderID',$id)->get();
	   //  $ship = address::find($data->shipping_address);
	   //  $bill = address::find($data->billing_address);
    //     $data['taxes'] = $data['tax'] + $data['cess'];
    //     $total = $data['amount']*$data['taxes']/100;

    //     $data['com'] = ( $data['amount']*$data['taxes'] / 100 );

    //     $data['sub_total'] = $total+$data['amount'];
    //     $data['amountInWords'] = ucwords( NumbersToWords::convert( $data['sub_total'] ) );
    //     // echo IntToEnglish::Int2Eng($sub_total);

    //     // return view('admin.order-pdf',compact('data','ship','bill','watchd','order_product','store','store_add'));
    //     $pdf = PDF::loadView('admin.order-pdf',['data' => $data,'ship' => $ship,'bill' => $bill,'watchd' => $watchd,'order_product' => $order_product,'store' => $store,'store_add' => $store_add ]);
    //     return $pdf->download( $data->orderID . '.pdf' );
    // }

    public function store(Request $request, $response = array( 'status' => 'error', 'message' => '' ) ){
     


            $file =  url(asset('/public/data/city.csv'));
            
            // File Details 
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();
             // Valid File Extensions
            $valid_extension = array('csv');
            // 2MB in Bytes
            $maxFileSize = 2097152; 
            
            // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){
                // Check file size
                if( $fileSize <= $maxFileSize ) {
                    // File upload location
                    $location = 'public/uploads';
                    // Upload file
                    $file->move($location,$filename);
                    // Import CSV to Database
                    $filepath = $location . '/' . basename($filename);
                    // $filepath = public_path($location."/".$filename);
                    // Reading file
                    $file = fopen( $filepath, 'r' );
                    $importData_arr = array();
                    $i = 0;
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata );
                        // Skip first row (Remove below comment if you want to skip the first row)
                        if($i == 0){
                            $i++;
                            continue;
                        }
                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata [$c];
                        }
                        $i++;
                    }
                    fclose($file);

                    // Insert to MySQL database
                    foreach($importData_arr as $importData){

                    	// print_r($importData[2]);
                    	// die();

                        $insertData = array(
                            "title"     => $importData[0],
                            "quantity"  => $importData[1],
                            "amount"    => $importData[2],
                            "orderID"   => $importData[3],
                        );
                        order_product::create($insertData);
                        // insert log data
                        
                        // Redirect to index
                    }
                    $response['status'] = 'success';
                    $response['message']= 'Import Successful';
                }else{
                    $response['message'] = 'File too large. File must be less than 2MB.';
                }
            }else{
                $response['message'] = 'Invalid File Extension';
            }
        

        return redirect()->action('BanklogController@index')->with( $response['status'], $response['message'] );
    }
}
