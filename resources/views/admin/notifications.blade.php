@extends('layouts/admin')

@section('title','Notification')

@section('content')
<h1 class="text-primary">Notifications</h1>

<div class="list-group list-group-flush">
    @foreach($data as $value )
    <a class="list-group-item list-group-item-action p-1" href="{{ url( '/admin/' . $value->module . '/view/' . $value->referenceID ) }}">        
        <div class="d-flex align-items-center justify-content-between w-100">
            <div class="d-flex align-items-center">
               
                <h6 class="mb-1">{{ $value->title }}</h6>
            </div>
            <small><?php echo date( 'D, jS M Y g:i a', strtotime( $value->created ) );?></small>
        </div>
        <small class="mb-1">{{ $value->message }} </small>
    </a>
    @endforeach
</div>
<div class="row pt-5"> 
    {{ $data->links() }}

</div>
@endsection