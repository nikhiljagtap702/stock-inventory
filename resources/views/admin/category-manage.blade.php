@extends('layouts/admin')
<?php if( isset( $data->categoryID  ) && !empty( $data->categoryID  ) ) :?>
    @section('title','Manage Category') 
<?php else :?>
    @section('title','Add Category') 
<?php endif;?>

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            <?php if( isset( $data->categoryID  ) && !empty( $data->categoryID  ) ) :?>
                Edit Category 
            <?php else :?>
                Add a Category
            <?php endif;?>
        </h6>
    </div>
    <div class="card-body">
		<?php if( isset( $data->categoryID  ) && !empty( $data->categoryID  ) ) :?>
        <form method="post" action="{{ route('categories.update', $data->categoryID  ) }}" enctype="multipart/form-data" > 
        <?php else :?>
        <form method="post" action="{{ route('categories.update', 0 ) }}" enctype="multipart/form-data" >
        <?php endif;?>
        @csrf
        @method('PUT')
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Title<span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="title" value="{{ old('title') ?? $data->title ?? '' }}"  required="required"  >
                 @error('title')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                @if(session('error'))
                <strong class="text-danger">{{session('error')}}</strong>
                @endif
            </div>
            <div class="form-group col-md-6">
                <label>Description </label>
                <textarea class="form-control"  name="description">{{ old('description') ?? $data->description ?? '' }}</textarea>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Featured Image</label>
                <div class="pt-3">
                    <input type='file' name="fileToUpload" onchange="readURL(this);" alt="your image" />
                    @error('fileToUpload')
                    <span class="feedback" role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <?php if( isset( $data->uri ) && !empty( $data->uri ) ){ ?>
                <div class="form-group col-md-3">
                    <img id="blah"  class="img-fluid" src="{{asset('public/uploads/'.$data->uri )}}" />
                </div>
            <?php }else{ ?>
                <div class="form-group col-md-3">
                    <img class="img-fluid" src="{{ asset('public/uploads/default-product.png')}}">
                </div>
            <?php } ?>
        </div>
        <button type="submit" class="btn btn-success">
            <i class="bi bi-file-earmark-plus"></i>
            <span>Save</span>
        </button>
        <a class="btn btn-outline-secondary" href="{{ url('/admin/categories') }}" role="button" title="Go back">
            <i class="bi bi-caret-left"></i>
            <span>Back</span>
        </a>
    </form>
    <br/>
    <?php if( isset( $data->categoryID  ) && !empty( $data->categoryID  ) ) :?>
    <form action="{{ route('categories.destroy', $data->categoryID  )}}" method="post">
        <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are you sure you want to delete this Category?');" title="Delete">
            <i class="bi bi-trash"></i>
            <span>Delete this Category</span>
        </button>
        @csrf
        @method('DELETE')
    </form>
    <?php endif;?>
</div>
@endsection
@section('stylesheets')
<style type="text/css">
    .img-fluid {
        max-height: 10rem;
        object-fit: contain;
        object-position: center;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection 