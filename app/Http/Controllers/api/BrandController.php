<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\brand;
use App\file;
use Validator;

/**
 * @group Brands
 *
 * APIs for managing brands
 */

class BrandController extends Controller{
    
    /** 
     * Get all brands
     * 
     * @queryParam page Page number Example: 1
     * @queryParam limit number of items per request Example: 10
     *
     * @response {
     *      "data": [{
     *         "brandID": 1,
     *         "fileID": 0,
     *         "title": "Brand name",
     *         "description": "Brief description",
     *         "body": "Exteded description about the brand",
     *         "fileUrl": "",
     *         "avatar": ""
     *      }],
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_brands( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

         $valid = Validator::make( $request->all() , [
            'page' => [],
            'limit' => [],

        ]);

        if( !$valid->fails() ) {

        $page  = $request->page * 10;
        $limit  = $request->limit;
        if($request->limit){

        $brands = brand::latest()->paginate($limit);

        }elseif ($request->page) {

         $brands = brand::latest()->paginate($page);
            
        }

        if( isset( $brands ) && !empty( $brands ) ) {            
            $response['status'] = 'success';
            
            foreach( $brands as $row ) {
                $row['fileUrl'] = url( 'public/uploads/default-product.png' );
                $row['avatar']  = url( 'public/uploads/thumbnail/default-product.png' );
                if( isset( $row['fileID'] ) && !empty( $row['fileID'] ) ) {

                    $file = file::find( $row['fileID'] );
                    if( !isset( $file['directory'] ) || empty( $file['directory'] ) ) {
                        $file['directory'] = 'public/uploads';
                    }
                    $row['fileUrl'] = url( $file['directory'] . '/' . $file['uri'] );
                    // $row['avatar']  = url( $file['directory'] . '/thumbnail/' . $file['uri'] );
                    $row['avatar']  = url( $file['directory'] . '/' . $file['uri'] );

                }

                unset( $row['status'] );
                unset( $row['created'] );
                unset( $row['updated'] );
                unset( $row['products'] );

                $response['data'][] = $row;
            }
          }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Get brands details
     * 
     * @queryParam brandID required Unique brand ID Example: 1
     *
     * @response {
     *      "data": {
     *         "brandID": 1,
     *         "fileID": 0,
     *         "title": "Brand name",
     *         "description": "Brief description",
     *         "body": "Exteded description about the brand",
     *         "fileUrl": "",
     *         "avatar": ""
     *      },
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_brand( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'brandID' => ['required'],
        ], [
            'brandID.required' => 'Please enter a brand ID',
        ]);

        if( !$valid->fails() ) {

            $row = brand::find( $request->brandID );

            if( isset( $row ) && !empty( $row ) ) {                

                $row['fileUrl'] = url( 'public/uploads/default-product.png' );
                $row['avatar']  = url( 'public/uploads/thumbnail/default-product.png' );
                if( isset( $row['fileID'] ) && !empty( $row['fileID'] ) ) {

                    $file = file::find( $row['fileID'] );
                    if( !isset( $file['directory'] ) || empty( $file['directory'] ) ) {
                        $file['directory'] = 'public/uploads';
                    }
                    $row['fileUrl'] = url( $file['directory'] . '/' . $file['uri'] );
                    // $row['avatar']  = url( $file['directory'] . '/thumbnail/' . $file['uri'] );
                    $row['avatar']  = url( $file['directory'] . '/' . $file['uri'] );
                }

                unset( $row['status'] );
                unset( $row['created'] );
                unset( $row['updated'] );
                unset( $row['products'] );

                $response['data'] = $row;
                $response['status'] = 'success';
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
}
