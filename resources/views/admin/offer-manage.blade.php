@extends('layouts/admin')
<?php if( isset( $data->offerID ) && !empty( $data->offerID ) ) :?>
    @section('title','Manage Offer') 
<?php else :?>
    @section('title','Add Offer') 
<?php endif;?>

@section('content')
<style type="text/css">
    .img-fluid {
        max-height: 10rem;
        object-fit: contain;
        object-position: center;
    }
</style>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            <?php if( isset( $data->offerID ) && !empty( $data->offerID ) ) :?>
                Edit Offer
            <?php else :?>
                Add a Offer
            <?php endif;?>
        </h6>
    </div>
    <div class="card-body">
        <?php if( isset( $data->offerID ) && !empty( $data->offerID ) ) :?>
        <form method="post" action="{{ route('offers.update', $data->offerID ) }}" enctype="multipart/form-data" >
            <?php else :?>
                <form method="post" action="{{ route('offers.update', 0 ) }}" enctype="multipart/form-data" >
                <?php endif;?>
                @csrf
                @method('PUT')
                <div class="form-row">
                    <?php if( isset( $data->offerID ) && !empty( $data->offerID ) ) :?>
                    <div class="form-group col-md-12">
                        <label>Title</label>
                        <input type="text" class="form-control" value="{{ $data->title ?? '' }}" name="title"  placeholder="Offer Title" required="required">
                    </div>
                    
                    <?php else :?>
                    <div class="form-group col-md-6">
                        <label>Title</label>
                        <input type="text" class="form-control" value="{{ $data->title ?? '' }}" name="title"  placeholder="Offer Title" required="required">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Stores </label>
                        <select  name="store[]" class="form-control" multiple>
                            @foreach($stores as $store )
                            <option value="{{ $store->storeID }}">{{ $store->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <?php endif;?>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Description </label>
                        <textarea class="form-control"  name="description">{{ $data->description ?? '' }}</textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Body </label>
                        <textarea class="form-control"  placeholder="full description" name="Body">{{ $data->body ?? '' }}</textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label>Featured Image</label>
                        <div class="pt-3">
                            <input type='file' name="fileToUpload" onchange="readURL(this);" alt="your image" />
                            @error('fileToUpload')
                            <span class="feedback" role="alert">
                                <strong class="text-danger">{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <?php if( isset( $data->uri ) && !empty( $data->uri ) ){ ?>
                        <div class="form-group col-md-3">
                            <img id="blah"  class="img-fluid" src="{{asset('public/uploads/'.$data->uri )}}" />
                        </div>
                    <?php }else{ ?>
                        <div class="form-group col-md-3">
                            <img id="blah" class="img-fluid" src="{{asset('public/uploads/default-product.png')}}" />
                        </div>
                    <?php } ?>
                </div>
                <button type="submit" class="btn btn-success">
                    <i class="bi bi-file-earmark-plus"></i>
                    <span>Save</span>
                </button>
                <a class="btn btn-outline-secondary" href="{{ url('/admin/offers') }}" role="button" title="Go back">
                    <i class="bi bi-caret-left"></i>
                    <span>Back</span>
                </a>
            </form>
            <?php if( isset( $data->offerID ) && !empty( $data->offerID ) ) :?>
            <form action="{{ route('offers.destroy', $data->offerID )}}" method="post">
                <button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are you sure you want to delete this offer?');" title="Delete">
                    <i class="bi bi-trash"></i>
                    <span>Delete this offer</span>
                </button>
                @csrf
                @method('DELETE')
            </form>
        <?php endif;?>
    </div>
</div>
@endsection
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah')
                .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>