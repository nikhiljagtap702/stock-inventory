@extends('layouts/admin')

@section('title','stores page')

@section('content')

 <!-- Page Heading -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
          	<div class="row">
          		<div class="col-md-6">
            <h6 class="m-0 font-weight-bold text-primary">Stores Table </h6>
        </div><div class="col-md-6 text-right">
            <a href="{{ route('stores.create') }}" class="btn btn-primary">+ADD STORE</a>
        </div>
    </div>
       </div>
       @if ($message = Session::get('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
      
      <p>{{ $message }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      @endif
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>slug </th>
                  <th>tagline</th>
                  <th>domain</th>
                  <th>Action</th>
                </tr>
              </thead>
            
              <tbody>
				  @foreach($data as $value)
          <tr class="active"> 
          <td scope="row">{{ ++$i }}</td> 
          <td>{{ $value->title }}</td> 
          <td>{{ $value->slug }}</td> 
          <td>{{ $value->tagline }}</td>
          <td>{{ $value->domain }}</td>
				 	

				 	<td>
				 		 <form action="{{ route('stores.destroy',$value->storeID )}}"  method="post">

				 			<a href="{{ route('stores.show',$value->storeID ) }}" class="btn btn-info" ><i class="fa fa-eye" ></i></a>

              <a href="{{ route('stores.edit', $value->storeID ) }}" class="btn btn-success" ><i class="fa fa-edit" ></i></a>

				 		<button type="submit" class="btn btn-danger" onclick="javascript:return confirm('Are You Confirm Deletion');" title="Delete" ><i class="fa fa-trash"></i></button>
				 	@csrf
          @method('DELETE')
          

					 </form>
					</td>
				 </tr>
			
  @endforeach
                  </tbody>
                </table>
              {{ $data->links() }}
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      </div>
@endsection