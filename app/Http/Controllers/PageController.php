<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){

    	return view('admin.index');
    }

    public function view(){

    	return view('admin.viewuser');
    }

    public function terms() {
        $page = collect();
        $title = 'Terms and Conditions';
        $body = '';
        return view( 'page' )->with(compact('title','body'));
    }

    public function privacy() {
        $page = collect();
        $title = 'Privacy Policy';
        $body = '';
        return view( 'page' )->with(compact('title','body'));
    }

}
