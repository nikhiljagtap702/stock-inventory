@extends('layouts/admin')
@section('title','Add Member') 
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">
            Add a Member
        </h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ url('/admin/store/store/') }}" >
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6 mx-auto">
                    <label>Mobile No<span class="text-danger">*</span></label>
                    <input type="text" id="txtPhoneNo"  onkeypress="return isNumber(event)" name="mobile" value="{{ old('mobile')}}" placeholder="Enter Mobile Number" maxlength="10"  class="form-control" required="required"> 
                    @error('mobile')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    @if(session('error'))
                    <strong class="text-danger">{{session('error')}}</strong>
                    @endif
                </div>
            </div>
            <input type="hidden" name="storeID"  value="<?php echo Request::segment(4); ?>">
            <div class="form-group col-md-6 mx-auto">
                <button type="submit" class="btn btn-success" onclick="ValidateNo();">
                    <i class="bi bi-file-earmark-plus"></i><span class="pl-2">Add</span>
                </button>
                <a class="btn btn-outline-secondary" href="{{ url('/admin/store/team/'.Request::segment(4)) }}" role="button" title="Go back">
                    <i class="bi bi-caret-left"></i>
                    <span>Back</span>
                </a>
                <a href="{{ route('users.create')}}"><strong>Create User</strong></a>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            alert("Please enter only Numbers.");
            return false;
          }

          return true;
    }

    function ValidateNo() {
          var phoneNo = document.getElementById('txtPhoneNo');

          if (phoneNo.value == "" || phoneNo.value == null) {
            alert("Please enter your Mobile No.");
            return false;
          }
          if (phoneNo.value.length < 10 || phoneNo.value.length > 10) {
            alert("Mobile No. is not valid, Please Enter 10 Digit Mobile No.");
            return false;
          }

          // alert("Success ");
          // return true;
    }
</script>
@endsection
