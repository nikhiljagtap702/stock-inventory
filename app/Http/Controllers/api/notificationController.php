<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\notification;
use App\order;
use Validator;

/**
 * @group Notifications
 *
 * APIs for managing notifications
 */
class notificationController extends Controller {
    /** 
     * Fetch notifications
     *
     * Fetch notifications paginated by 10 per page
     *
     * @queryParam page Page number Example: 1
     * @queryParam limit Number of items per request Example: 10
     * @queryParam userID int required Unique user ID Example: 1
     * @queryParam storeID int required Unique store ID Example: 1
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_notifications( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        $valid = Validator::make( $request->all() , [
            'userID' => ['required'],
            'storeID' => [],
        ], [
            'userID.required' => 'Please enter a user ID'
        ]);
        
        if( !$valid->fails() ) {

            $data = notification::latest()->whereIn('userID',[1,$request->userID])->get();

              if( isset( $data ) && !empty( $data ) ) {            
                $response['status'] = 'success';
                
                foreach( $data as $item ) {
                    $res = array();
                    
                    $res['id']      = $item['noteID'];
                    $res['title']   = $item['title'];
                    $res['message'] = $item['message'];
                    $res['storeID'] = $item['storeID'];
                    $res['orderID'] = $item['referenceID'];
                    $res['date']    = date( 'c',strtotime( $item->created ) );

                    $order = order::find($res['orderID']);
                    if (isset($order) && !empty($order )) {
                        if( $order['shipping_status'] == 0 ) {
                            $res['shipping_statusName'] = 'N/A';
                        }else if( $order['shipping_status'] == 1 ) {
                            $res['shipping_statusName'] = 'Pickup';
                        }else if( $order['shipping_status'] == 2 ) {
                            $res['shipping_statusName'] = 'In Transit';
                        }else if( $order['shipping_status'] == 3 ) {
                            $res['shipping_statusName'] = 'Out for Delivery';
                        }else if( $order['shipping_status'] == 4 ) {
                            $res['shipping_statusName'] = 'Shipped';
                        }else {
                            $res['shipping_statusName'] = 'Unknown';
                        } 
                    }

                     ksort( $res );
                   
                    $response['data'][] = $res;

                }
            }
        }

        
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Fetch notification
     *
     * Fetch notification details
     *
     * @queryParam notificationID required Unique notification ID Example: 1
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_notification( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        $valid = Validator::make( $request->all() , [
            'notificationID' => ['required'],
        ], [
            'notificationID.required' => 'Please enter a notification ID'
        ]);
        
        if( !$valid->fails() ) {

        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
}
?>