<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\offer;
use App\file;
use Validator;

/**
 * @group Offers
 *
 * APIs for managing Offers
 */
class OfferController extends Controller{
	 /** 
     * Get offers details
     * 
     * @queryParam offerID required Unique offer ID Example: 1
     *
     * @response {
     *      "data": {
     *         "offerID": 1,
     *         "fileID": 0,
     *         "title": "offer name",
     *         "description": "Brief description",
     *         "body": "Exteded description about the offer",
     *         "fileUrl": "",
     *         "avatar": ""
     *      },
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_offer( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'offerID' => ['required'],
        ], [
            'offerID.required' => 'Please enter a Offer ID',
        ]);

         if( !$valid->fails() ) {

             $row = offer::find($request->offerID);
            if( isset( $row ) && !empty( $row ) ) {                

                $row['fileUrl'] = url( 'public/uploads/default-product.png' );
                $row['avatar']  = url( 'public/uploads/thumbnail/default-product.png' );
                if( isset( $row['fileID'] ) && !empty( $row['fileID'] ) ) {

                    $file = file::find( $row['fileID'] );
                    if( !isset( $file['directory'] ) || empty( $file['directory'] ) ) {
                        $file['directory'] = 'public/uploads';
                    }
                    $row['fileUrl'] = url( $file['directory'] . '/' . $file['uri'] );
                    $row['avatar']  = url( $file['directory'] . '/thumbnail/' . $file['uri'] );
                }

                unset( $row['status'] );
                unset( $row['created'] );
                unset( $row['updated'] );
                unset( $row['products'] );

                $response['data'] = $row;
                $response['status'] = 'success';
            }
            
       	 }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
     }  

     /** 
     * Get all offers
     * 
     * @queryParam page Page number Example: 1
     * @queryParam limit number of items per request Example: 10
     *
     * @response {
     *      "data": [{
     *         "offerID": 1,
     *         "fileID": 0,
     *         "title": "offer name",
     *         "description": "Brief description",
     *         "body": "Exteded description about the offer",
     *         "fileUrl": "",
     *         "avatar": ""
     *      }],
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */


     public function get_offers( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'page' => [],
            'limit' => [],

        ]);

        if( !$valid->fails() ) {

        $page  = $request->page * 10;
        $limit  = $request->limit;

        if($request->limit){

         $offer = offer::where('status',1)->paginate($limit);

         }elseif ($request->page) {
         
          $offer = offer::where('status',1)->paginate($page);
         }

          if( isset( $offer ) && !empty( $offer ) ) {            
            $response['status'] = 'success';
            
            foreach( $offer as $row ) {
                $row['fileUrl'] = url( 'public/uploads/default-product.png' );
                $row['avatar']  = url( 'public/uploads/thumbnail/default-product.png' );
                if( isset( $row['fileID'] ) && !empty( $row['fileID'] ) ) {

                    $file = file::find( $row['fileID'] );
                    if( !isset( $file['directory'] ) || empty( $file['directory'] ) ) {
                        $file['directory'] = 'public/uploads';
                    }
                    $row['fileUrl'] = url( $file['directory'] . '/' . $file['uri'] );
                    $row['avatar']  = url( $file['directory'] . '/thumbnail/' . $file['uri'] );
                }

                unset( $row['status'] );
                unset( $row['created'] );
                unset( $row['updated'] );
                unset( $row['products'] );

                $response['data'][] = $row;
            }
         }
     }
     if( $response['status'] == 'error' ) {
        $response['values'] = $request->all();
        $response['errors'] = $valid->errors();
    }

    return $response;
    }  

    /** 
     * create offer 
     * 
     * @bodyParam fileID int required Unique file ID Example: 1234
     * @bodyParam title string required Unique title Example: xyz
     * @bodyParam body string optional Unique body  Example: xyz
     * @bodyParam description string  optional description Example: xyz
     * @bodyParam storeID int  optional store ID Example: 133
     *
     * @response {
     *      "data": [
     *          {
     *
     *          }
     *      ],
     *      "status": "success"
     * }
     */
    public function post_offer( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
             'fileID'        => ['required'],
             'storeID'       => [],
             'title'         => ['required'],
             'body'          => [],
             'description'   => [],
            
        ]);

        if( !$valid->fails() ) {
            $response['status'] = 'success';

            $data = array(
                'fileID'           => $request->fileID,
                'storeID'          => $request->storeID,
                'title'            => $store->title,
                'body'             => $store->body,
                'description'      => $request->description,
            );

            $offer = offer::create($data);

            $response['data'] = $offer;

            $response['messages'][] = array( 'message' => 'Offer has been created successfully', 'type' => 'success' );            
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }
}
