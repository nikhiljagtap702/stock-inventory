<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\store;
use App\order;
use App\brand;
use App\product;
use App\team;
use App\order_brand;
use App\order_product;
use Carbon\Carbon;
use Validator;
use DB;

/**
 * @group Stores
 *
 * APIs for managing stores
 */
class storeController extends Controller {
    /** 
     * Fetch stores
     *
     * Fetch stores paginated by 10 per page
     *
     * @bodyParam storeID int required  store ID Example: 1
     *
     * @queryParam start_date required  start date Example: 2
     *
     * @queryParam end_date required  end date Example: 3
     *
     * @bodyParam page int Page number Example: 1
     * @bodyParam limit int Number of items per request Example: 10
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_stores( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        
        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Fetch store
     *
     * Fetch stores details
     *
     * @bodyParam storeID int required Unique store ID Example: 1
     *
     * @response {
     *      "data": {},
     *      "messaegs": [],
     *      "status": "success"
     * }
     *
     */
    public function get_store( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {
        $valid = Validator::make( $request->all() , [
            'storeID' => ['required'],
        ], [
            'storeID.required' => 'Please enter a store ID'
        ]);
        
        if( !$valid->fails() ) {

        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Get store analytics
     * 
     * @return \Illuminate\Http\Response 
     */
    /*public function get_store_analytics( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'storeID' => ['required'],
        ], [
            'storeID.required' => 'Please enter a store ID'
        ]);
        
        if( !$valid->fails() ) {

         }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }*/

    /** 
     * Get store brand analytics
     * 
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam start_date date optional Analytics start date Example: 2020-01-01
     * @bodyParam end_date date optional Analytics end date Example: 2020-03-31
     *
     * @response {
     *      "data": [],
     *      "messaegs": [],
     *      "status": "success"
     * }
     */
    public function get_analytics_brands( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

     $valid = Validator::make( $request->all() , [
            'storeID'   => ['required'],
            'start_date'=> [],
            'end_date'  => [],
        ], [
            'storeID.required' => 'Please enter a store ID'
        ]);
       
        if( !$valid->fails() ) {

              $startDate  = Carbon::parse($request->start_date)->format('Y-m-d');
              $endDate    = Carbon::parse($request->end_date)->format('Y-m-d');
    
             $data = order_product::where('storeID',$request->storeID)
              ->whereBetween('created', [ $startDate, $endDate ])
             ->select('brandID','brand_name',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
             ->groupBy('brandID','brand_name')
            ->get();

            if( isset( $data ) && !empty( $data ) ) {

                $response['status'] = 'success';

                foreach( $data as $item ) {
                    $res['ID']        =  $item->brandID;
                    $res['title']     =  $item->brand_name;
                    $res['value']     =  $item->total_val;
                    $res['Amount']     =  $item->total_amount;
                    // $res['timestamp'] = date( 'c', strtotime( $item->created ) );

                    $response['data'][] = $res;
                 }   
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Get store orders analytics
     * 
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam start_date date optional Analytics start date Example: 2020-01-01
     * @bodyParam end_date date  optional  Analytics end date Example: 2020-03-31
     *
     * @response {
     *      "data": [],
     *      "messaegs": [],
     *      "status": "success"
     * }
     */
    public function get_analytics_orders( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'storeID'   => ['required'],
            'start_date'=> [],
            'end_date'  => [],
        ], [
            'storeID.required' => 'Please enter a store ID'
        ]);
        
        if( !$valid->fails() ) {

            $startDate  = Carbon::parse($request->start_date)->format('Y-m-d');
            $endDate  = Carbon::parse($request->end_date)->format('Y-m-d');

            $data = order::where('storeID',$request->storeID)->select( 'orderID','amount','created')
                 ->whereBetween('created', [ $startDate, $endDate ])
                ->get();

            if( isset( $data ) && !empty( $data ) ) {  

                $response['status'] = 'success';
                
                foreach( $data as $item ) {

                    $res['ID']        =  $item->orderID;
                    $res['title']     =  'Order #' . $item->orderID;
                    $res['value']     =  $item->amount;
                    $res['timestamp'] =  date( 'c', strtotime( $item->created ) );

                    $response['data'][] = $res;
                 }   
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;
    }

    /** 
     * Get store products analytics
     * 
     * @bodyParam storeID int required Unique store ID Example: 1234
     * @bodyParam start_date date optional Analytics start date Example: 2020-01-01
     * @bodyParam end_date date  optional Analytics end date Example: 2020-03-31
     *
     * @response {
     *      "data": [],
     *      "messaegs": [],
     *      "status": "success"
     * }
     */
    public function get_analytics_products( Request $request, $response = array( 'status' => 'error', 'data' => array(), 'messages' => array() ) ) {

        $valid = Validator::make( $request->all() , [
            'storeID'   => ['required'],
            'start_date'=> [],
            'end_date'  => [],
        ], [
            'storeID.required' => 'Please enter a store ID'
        ]);
        
        if( !$valid->fails() ) {

             $startDate  = Carbon::parse($request->start_date)->format('Y-m-d');
             $endDate    = Carbon::parse($request->end_date)->format('Y-m-d');

            $data = order_product::where('storeID',$request->storeID)
            ->whereBetween('created', [ $startDate, $endDate ])
            ->select('productID','title',\DB::raw('SUM(quantity) as total_val'),\DB::raw('SUM(amount) as total_amount'))
            ->groupBy('title','productID')
            ->get();


            if( isset( $data ) && !empty( $data ) ) {  
                $response['status'] = 'success';

                foreach( $data as $item ) {
                    $res['ID']        =  $item->productID;
                    $res['title']     =  $item->title;
                    $res['value']     =  $item->total_val;
                    $res['amount']     =  $item->total_amount;
                    // $res['timestamp'] =  date( 'c', strtotime( $item->created ) );

                    $response['data'][] = $res;
                }
            }
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }

        return $response;   
   }

    /** 
     * Add a Client 
     * 
     * @bodyParam title string required Unique title Example: xyz
     * @bodyParam slug string optional  slug Example: xyz
     * @bodyParam tagline string optional  optional Example: xyz
     * @bodyParam domain required domain Example: xyz.com
     * @bodyParam gstin required GST number Example: 1lkfb
     * @bodyParam description string optional description Example: xyz
     * @bodyParam keywords string optional keywords Example: xyz
     * @bodyParam userID  int required store owner userID name Example: 122
     *
     * @response {
     *      "data": {
     *   
     *
     *      },
     *      "messaegs": [],
     *      "status": "success"
     * }
     */
     public function post_client(Request $request)  {

        $valid=Validator::make($request->all(),[

            'title'          => ['required'],
            'slug'           => [],
            'tagline'        => [],
            'domain'         => ['required'],
            'gstin'          => ['required'],
            'description'    => [],
            'keywords'       => [],
            'payment_status' => [],
        

        ]);
        if( !$valid->fails() ) {
                $store_data=array(
                    'title'          => $request['title'],
                    'slug'           => $request['slug'],
                    'tagline'        => $request['tagline'],
                    'domain'         => $request['domain'],
                    'favicon'        => $request['favicon'],
                    'gstin'          => $request['gst'],
                    'description'    => $request['description'],
                    'keywords'       => $request['keyword'],
                    'status'         => 1,
                   
                );

            $team_data = array(
                'userID'  => $request['userID'],
                'role'    => 1,
            );

            $store = store::create( $store_data );
            $team_data['storeID'] = $store->storeID;
            team::create( $team_data);
            $response['data'] = $store;
            $response['messages'][] = array( 'message' => 'Client   has been created successfully', 'type' => 'success' );
        
        }

        if( $response['status'] == 'error' ) {
            $response['values'] = $request->all();
            $response['errors'] = $valid->errors();
        }
        return $response;
    }
}
?>