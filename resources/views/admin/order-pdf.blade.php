<!DOCTYPE html>
<html>
<head>
    <title>Order # {{ $data->orderID }}</title>
    <!-- <link rel="stylesheet" href="{{ asset( 'public/css/bootstrap.min.css' ) }}"> -->
    <!-- <link rel="stylesheet" href="{{ asset('public/css/all.min.css') }}"> -->
    <style type="text/css">
        .table {
            border-collapse: collapse;
            width:100%;
        }
    </style>
</head>
<body>
    <div class="table-responsive">
        <table class="table" border="1">
            <tr>
                <td style="text-align: center;">
                    <img src="{{ asset( 'public/media/icon-128.png' ) }}" style="width: 100px; height: 100px" alt="Supplyport" title="" height="64" width="auto">
                </td>
                <td>
                    <strong>{{ $store->title }}</strong> <br/>
                    <span>H 8/10 <br/>DLF phase 1 <br/>Gurgaon, 122002, Haryana-06, India</span>
                </td>
                <td>
                    <strong> GSTIN :</strong> {{ $store->gstin ?? '' }} <br/>
                    <strong> Telephone : </strong> {{ $store_add->phone ?? '' }}
                </td>
            </tr>
        </table>
        <table class="table" border="1">
            <thead>
                <tr>
                    <th>TAX INVOICE </th>
                    <th>SI{{ $data->orderID }} </th>
                    <th>Original </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                       <?php if($bill){ ?>
                       <p>{{ $bill->name }}<br>{{ $bill->email }}<br>{{ $bill->phone }}<br>{{ $bill->street }}<br>{{ $bill->locality }}<br>{{ $bill->region }}<br>{{ $bill->address }}<br>{{ $bill->city }}<br>{{ $bill->state }}<br>{{$bill->postal_code}}<br>{{$bill->country}}
                       </p>
                       <?php } ?></p>
                    </td>
                    <td>
                        <?php if($ship){ ?>
                          {{ $ship->name }}<br>{{$ship->email}}<br>{{$ship->phone}}<br>{{$ship->street}}<br>{{$ship->locality}}<br>{{$ship->region}}<br>{{$ship->address}}<br>{{$ship->city}}<br>{{$ship->state}}<br>{{$ship->postal_code}}<br>{{$ship->country}}
                          </p>
                        <?php } ?>
                    </td>
                    <td>
                        <strong> Sales Invoice No:  SI{{ $data->orderID }}<br/>
                        Sales Invoice Date : <?php echo  date('d M Y',strtotime($data->created)); ?><br/>
                        Status :<?php if($data->status == 2 ){ echo "Posted"; }else{ echo "Pending"; } ?><br/>
                        Cust Ref No : </strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table" border="1">
            <tr>
                <th>S.No </th>
                <th> ITEM </th>
                <th> HSN </th>
                <th> QTY </th>
                <th> PRICE </th>
                <th> Tax(%) </th>
                <th> Amount </th>
            </tr>
              <tbody>
                <?php $i = 1 ; ?>
                @foreach($order_product as $orders)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $orders['title'] }}</td>
                    <td>{{ $orders['hsn'] }}</td>
                    <td class="text-center">{{ $orders['quantity'] }}</td>
                    <td class="text-center"> {{ $orders['price'] }}</td>
                    <td class="text-center">{{ $orders['tax'] + $orders['cess'] }}</td>
                    <td class="text-right">{{ $orders['amount'] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <table class="table" border="1">
            <tr>
                <td>
                    <strong>GRAND TOTAL : {{ $data['amountInWords'] }} only </strong>
                </td>
                <td>
                    <strong>Grand Total :   {{ $data['sub_total'] }} <br/>
                     Net To Pay :  {{ $data['sub_total'] }}  </strong>
                </td>
            </tr>
        </table>
        <table class="table" border="1">
            <tr>
                <th>REMARKS :</th>
            </tr>
            <tr>
                <td>
                    NOTE - All disputes to be adjudicated by a sole Arbitrator appointed by {{ $store->title }}.
                </td>
                <td>
                    <strong>For : {{ $store->title }}<br/>( Authorized Signatory )</strong>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>