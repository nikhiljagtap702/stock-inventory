@extends('layouts/admin')

@section('title','Analytics')

@section('content')
<!-- main content start-->
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
	<h1 class="h2">Analytics</h1>
	<div class="btn-toolbar mb-2 mb-md-0">
		<div class="btn-group mr-2">
			
		</div>
		<?php   $segment1 =  Request::segment(3);
            $segment  =  date('d M Y',strtotime( $segment1));
		 ?>
		<select class="form-control"  id="compare">
			<option value="{{ url( 'admin/analytics/'.$today ) }}"> Today</option>
			<option value="{{ url( 'admin/analytics/'.$yesterday ) }}"  <?php if($segment == date('d M Y',strtotime( $yesterday))) echo ' selected="selected"'; ?>>Yesterday</option>
			<option value="{{ url( 'admin/analytics/'.$last_7_day ) }}" <?php if($segment == date('d M Y',strtotime( $last_7_day))) echo ' selected="selected"'; ?>>Last 7 days</option>
			<option value="{{ url( 'admin/analytics/'.$last_30_day ) }}" <?php if($segment == date('d M Y',strtotime( $last_30_day))) echo ' selected="selected"'; ?>>Last 30 days</option>
			<option value="{{ url( 'admin/analytics/'.$lastmonths ) }}" <?php if($segment == date('d M Y',strtotime( $lastmonths))) echo ' selected="selected"'; ?>>Last month</option>
			<option value="{{ url( 'admin/analytics/last_year' ) }}" <?php if($segment1 == 'last_year' ) echo ' selected="selected"'; ?>>Last year</option>
			<option value="{{ url( 'admin/analytics/secondOfQuarter' ) }}"  <?php if($segment1 == 'secondOfQuarter' ) echo ' selected="selected"'; ?> >2rd Quarter</option>
			<option value="{{ url( 'admin/analytics/firstOfQuarter' ) }}" <?php if($segment1 == 'firstOfQuarter' ) echo ' selected="selected"'; ?> >1st Quarter</option>
		</select>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		{!! $orderChart->container() !!}
	</div>
	<div class="col-lg-6">
		{!! $brandChart->container() !!}
	</div>
</div>
<div class="col-lg-12">
	{!! $orderproductChart->container() !!}
</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		$(function() {
			$('#datetimepicker1').datetimepicker();
		});
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
		{!! $orderChart->script() !!}
		{!! $orderproductChart->script() !!}
		{!! $brandChart->script() !!}
	<script type="text/javascript">
		$(document).ready( function () {
			$('#compare').on('change', function () {
				var url = $(this).val();
				if (url) {
					window.location = url;
				}
				return false;
			});
		});
	</script>
@endsection
@section('stylesheets')
<style type="text/css">
	.card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
  }

  .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .card-counter.primary{
    background-color: #007bff;
    color: #FFF;
  }

  .card-counter.danger{
    background-color: #ef5350;
    color: #FFF;
  }  

  .card-counter.success{
    background-color: #66bb6a;
    color: #FFF;
  }  

  .card-counter.info{
    background-color: #26c6da;
    color: #FFF;
  }  

  .card-counter i{
    font-size: 5em;
    opacity: 0.2;
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }
</style>
@endsection
