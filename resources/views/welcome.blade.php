@extends('layouts.app')

@section('content')
    <!-- <div class="container">
        <div class="d-flex align-items-center justify-content-center flex-column" style="min-height: 90vh;">        
            <h1 class="display-1 text-center text-primary font-weight-bold">Supply Port</h1>
            <h2 class="display-4 text-center">Coming Soon</h2>
        </div>
    </div> -->
    <section class="section" id="inventory">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5 ml-auto text-primary">
                    <h2 class="section-title">Inventory at your fingertips</h2>
                    <p class="section-description mt-3 mt-md-4">‘Supply Port’ or SupPort is an independent aggregator and supplier of all branded and packaged consumables servicing the restaurant sector. </p>
                </div>
                <div class="col-md-6 text-center">
                    <img class="img-fluid" src="./media/img-inventory.svg" alt="" height="" width="">
                </div>
            </div>
        </div>
    </section>

    <section class="section" id="about">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 ml-auto">
                    <h3 class="section-title text-md-right">Why supply port?</h3>
                </div>

                <div class="w-100"></div>

                <div class="col-11 col-md-6 mx-auto ml-md-0">
                    <ul class="text-white pl-0 mb-0">
                        <li>An ordering based interface to ease the process of ordering and tracking deliveries.</li>
                        <li>Through machine learning, create predictive modelling for better accuracy on regular and seasonal changes in re-order quantities relieving the operating manager from this stress as well.</li>
                        <li>Create an entire supply chain link to the ordering software so as to implement just-in-time inventory enabling us to manage supplies from our vendors in a timely manner as well, thereby making the entire system more efficient and economical.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="section" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h3 class="section-title text-center text-white">Wish to grab a chat?</h3>
                     @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <span>{{ $message }}</span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                    <div class="py-4"></div>

                    <form name="form" action="{{ url('/admin/feedbacks/store')}}" method="POST" accept="utf-8">
                        @csrf
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label text-white">Name<span class="text-danger">*</span> :</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputName" placeholder="" value="{{ old('name') ?? ''}}" name= 'name' required>
                                @error('name')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label text-white">Email<span class="text-danger">*</span> :</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail" placeholder="" value="{{ old('email') ?? ''}}" name="email" maxlength="255" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputMobile" class="col-sm-2 col-form-label text-white">Mobile <span class="text-danger">*</span> </label>
                            <div class="col-sm-10">
                                <input type="text" id="txtPhoneNo"  value="{{ old('mobile') ?? ''}}" class="form-control"  name="mobile" maxlength="10" required>
                                @error('mobile')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror 
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="inputMobile" class="col-sm-2 col-form-label text-primary">Company <span class="text-danger">*</span> :</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputMobile" name="company_name" value="{{ old('company_name') ?? ''}}" placeholder="" maxlength="45" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputMessage" class="col-sm-2 col-form-label text-primary">Message <span class="text-danger">*</span> :</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="inputMessage" placeholder="" name="message" required="required"  minlength="8" maxlength="1000" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-10 text-center mx-auto">
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="section" id="download">
        <div class="container">
            <div class="row align-items-center text-center text-primary">
                <div class="col-md-6 mb-5 mb-md-0">
                    <h3 class="section-title">Download the simplest management app</h3>
                    <p class="section-description">Available on</p>

                    <a href="https://play.google.com/store/apps/details?id=in.supplyport" target="_blank">
                        <img class="img-fluid" src="./media/google-play-badge.svg" alt="google-play-badge" title="">
                    </a>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    <img class="img-fluid" src="./media/mockup.png" alt="Mockups" title="">
                </div>
            </div>
        </div>
    </section>   
@endsection
@section('scripts')
<script type="text/javascript">
    function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            alert("Please enter only Numbers.");
            return false;
          }

          return true;
    }

 
</script>

@endsection 